package com.yalantis.ucrop;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.io.File;
import java.util.List;

import ci.objis.R;

import static android.support.v4.content.FileProvider.getUriForFile;

public class ImagePickerActivity extends AppCompatActivity {

    //les proprietes
    private final static String TAG = ImagePickerActivity.class.getSimpleName();
    public final static String INTENT_IMAGE_PICKER_OPTION = "image_picker_option";
    public final static String INTENT_ASPECT_RATIO_X = "aspect_ratio_x";
    public final static String INTENT_ASPECT_RATIO_Y = "aspect_ratio_y";
    public final static String INTENT_LOCK_ASPECT_RATIO = "lock_aspect_ratio";
    public final static String INTENT_IMAGE_COMPRESSION_QUALITY = "compression_quality";
    public final static String INTENT_BITMAP_MAX_WIDHT_HEIGHT = "set_bitmap_max_widht_height";
    public final static String INTENT_BITMAP_MAX_WIDHT = "max_widht";
    public final static String INTENT_BITMAP_MAX_HEIGHT = "max_height";

    public static final int REQUEST_IMAGE_CAPTURE = 0;
    public static final int REQUEST_GALLERY_IMAGE = 1;

    private Boolean lockAspectRatio = false;
    private Boolean setBitmapMaxHeightWidht = false;
    private int ASPECT_RATIO_X = 16;
    private int ASPECT_RATIO_Y = 9;
    private int bitmapMaxWidth = 1000;
    private int bitmapMaxHeigth = 1000;
    private int compressionImage = 80;

    public static String filename;

    public interface PickerOptionListener {
        void onTakeCameraSelected();

        void onChooseGallerySelected();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_picker);

        Intent intent = getIntent();

        if (intent == null) {
            Toast.makeText(this, "Aucune image selectionnée", Toast.LENGTH_SHORT).show();
            return;
        }

        ASPECT_RATIO_X = intent.getIntExtra(INTENT_ASPECT_RATIO_X, ASPECT_RATIO_X);
        ASPECT_RATIO_Y = intent.getIntExtra(INTENT_ASPECT_RATIO_Y, ASPECT_RATIO_Y);
        lockAspectRatio = intent.getBooleanExtra(INTENT_LOCK_ASPECT_RATIO, lockAspectRatio);
        setBitmapMaxHeightWidht = intent.getBooleanExtra(INTENT_BITMAP_MAX_WIDHT_HEIGHT, setBitmapMaxHeightWidht);
        bitmapMaxHeigth = intent.getIntExtra(INTENT_BITMAP_MAX_HEIGHT, bitmapMaxHeigth);
        bitmapMaxWidth = intent.getIntExtra(INTENT_BITMAP_MAX_WIDHT, bitmapMaxWidth);
        compressionImage = intent.getIntExtra(INTENT_IMAGE_COMPRESSION_QUALITY, compressionImage);

        int requestCode = intent.getIntExtra(INTENT_IMAGE_PICKER_OPTION, -1);
        if (requestCode == REQUEST_IMAGE_CAPTURE) {
            takeCameraImage();
        } else {
            chooseImageFromGallery();
        }
    }





    public static void showImagePickerOptions(Context context, PickerOptionListener listener) {
        //setup the alert builder
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //add the list
        String[] animals = {"Prendre la photo à partir de la gallerie", "Prendre la photo à partir de la camera"};
        builder.setItems(animals, (dialog, which) -> {
            switch (which) {
                case 0:
                    listener.onChooseGallerySelected();
                    break;
                case 1:
                    listener.onTakeCameraSelected();
                    break;
            }
        });

        //create et show the dialog
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void takeCameraImage() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            filename = System.currentTimeMillis() + ".jpg";
                            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, getCacheImagePath(filename));
                            if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                            }
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void chooseImageFromGallery(){
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if(report.areAllPermissionsGranted()){
                            Intent pickPhoto=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                            startActivityForResult(pickPhoto,REQUEST_GALLERY_IMAGE);
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case REQUEST_IMAGE_CAPTURE:
                if(resultCode==RESULT_OK){
                    cropImage(getCacheImagePath(filename));
                }else {
                    setResultCancelled();
                }
                break;
            case REQUEST_GALLERY_IMAGE:
                if(resultCode==RESULT_OK){
                    Uri imageUri=data.getData();
                    cropImage(imageUri);
                }else{
                    setResultCancelled();
                }
                break;
            case UCrop
                    .REQUEST_CROP:
                if(resultCode==RESULT_OK){
                    handleUCropResult(data);
                }else{
                    setResultCancelled();
                }
                break;
            case UCrop.RESULT_ERROR:
                final Throwable cropError=UCrop.getError(data);
                Log.e(TAG,"Crop error: "+cropError);
                setResultCancelled();
                break;
            default:
                setResultCancelled();
        }
    }

    private void cropImage(Uri sourceUri){
        Uri destination=Uri.fromFile(new File(getCacheDir(),queryName(getContentResolver(),sourceUri)));
        UCrop.Options options=new UCrop.Options();
        options.setCompressionQuality(compressionImage);
        options.setToolbarColor(ContextCompat.getColor(this,R.color.colorPrimary));
        options.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimary));
        options.setActiveWidgetColor(ContextCompat.getColor(this, R.color.colorPrimary));

        if (lockAspectRatio)
            options.withAspectRatio(ASPECT_RATIO_X, ASPECT_RATIO_Y);

        if (setBitmapMaxHeightWidht)
            options.withMaxResultSize(bitmapMaxWidth, bitmapMaxHeigth);

        UCrop.of(sourceUri, destination)
                .withOptions(options)
                .start(this);

    }

    private void handleUCropResult(Intent data) {
        if (data == null) {
            setResultCancelled();
            return;
        }
        final Uri resultUri = UCrop.getOutput(data);
        setResultOk(resultUri);
    }

    private void setResultOk(Uri imagePath) {
        Intent intent = new Intent();
        intent.putExtra("path", imagePath);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    private void setResultCancelled() {
        Intent intent = new Intent();
        setResult(Activity.RESULT_CANCELED, intent);
        finish();
    }

    private Uri getCacheImagePath(String fileName) {
        File path = new File(getExternalCacheDir(), "camera");
        if (!path.exists()) path.mkdirs();
        File image = new File(path, fileName);
        return getUriForFile(ImagePickerActivity.this, getPackageName() + ".provider", image);
    }

    private static String queryName(ContentResolver resolver, Uri uri) {
        Cursor returnCursor =
                resolver.query(uri, null, null, null, null);
        assert returnCursor != null;
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String name = returnCursor.getString(nameIndex);
        returnCursor.close();
        return name;
    }

    /**
     * Calling this will delete the images from cache directory
     * useful to clear some memory
     */
    public static void clearCache(Context context) {
        File path = new File(context.getExternalCacheDir(), "camera");
        if (path.exists() && path.isDirectory()) {
            for (File child : path.listFiles()) {
                child.delete();
            }
        }
    }
}
