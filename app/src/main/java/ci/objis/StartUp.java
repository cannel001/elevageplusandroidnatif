package ci.objis;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.app.Application;
import com.google.firebase.FirebaseApp;

public class StartUp extends Application {

    //les proprietes
    public final static String PREF = "PREF";
    public final static String ELEVAGESELECTIONNE = "ELEVAGE SELECTIONNE";
    public final static String OPTIONSELECT="OPTION SELECT";

    //les proprietes statiques du cycles en cours
    public final static String JOURCYCLESELECT="JOUR DU CYCLE SELECTIONNEE";
    public final static String CYCLESELECT="CYCLE SELECTIONNE";
    public final static String VETERINAIRESELECT="VETERINAIRE SELECTIONNE";

    @Override
    public void onCreate() {
        super.onCreate();

        ActiveAndroid.initialize(this);
    }
}
