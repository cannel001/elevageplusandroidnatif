package ci.objis.repository;

import java.util.List;

import ci.objis.model.Publication;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;


public interface PublicationRepository {

    @GET("all/")
    Call<List<Publication>> getAll(@Header("tel") String tel, @Header("password") String password);

}
