package ci.objis.repository;

import ci.objis.model.Utilisateur;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface UtilisateurRepository {

    @FormUrlEncoded
    @POST(value = "./register")
    Call<Utilisateur> registerUser(@Field("tel") String tel);

    @FormUrlEncoded
    @POST(value = "./activecompte")
    Call<Utilisateur> activeCompteUser(@Field("tel") String tel, @Field("codevalide") String codeValide);

    @FormUrlEncoded
    @POST(value = "./login")
    Call<Utilisateur> loginUser(@Field("tel") String tel, @Field("password") String password);

    @FormUrlEncoded
    @POST(value = "./recuperpassword")
    Call<Object> recupPassword(@Field("tel") String tel);

    @PUT(value = "./updatecompte")
    Call<Utilisateur> updateCompte(@Header("tel") String tel, @Header("password") String password, @Body Utilisateur utilisateur);

    @FormUrlEncoded
    @PUT(value = "./updatepassword")
    Call<Utilisateur> updatePassword(@Header("tel") String telUser, @Header("password") String password, @Field("ancienPassword") String ancienPassword, @Field("newPassword") String newPassword, @Field("tel") String tel);

}
