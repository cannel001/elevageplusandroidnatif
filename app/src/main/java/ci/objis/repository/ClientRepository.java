package ci.objis.repository;


import java.util.List;

import ci.objis.model.Client;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ClientRepository {

    @GET("./")
    Call<List<Client>> getAll(@Header("tel") String tel,@Header("password") String password);

    @GET("./{id}")
    Call<Client> getOne(@Header("tel") String tel,@Header("password") String password, @Path("id") long id);

}
