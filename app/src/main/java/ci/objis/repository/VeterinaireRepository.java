package ci.objis.repository;

import java.util.List;

import ci.objis.model.Veterinaire;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface VeterinaireRepository {

    @GET(value = "all/")
    Call<List<Veterinaire>> getAll();

    @FormUrlEncoded
    @POST(value = "./specialiste}")
    Call<List<Veterinaire>> getAllBySpecialite(@Header("tel") String tel, @Header("password") String password, @Field("libelle") String libelle);

}
