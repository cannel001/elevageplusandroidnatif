package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "Venteoiseau")
public class VenteOiseau extends AbstractTache implements Serializable {

    @Column(name = "nombre")
    private int nombre;

    @Column(name = "moyenpaiment")
    private String moyenPaiment;

    @Column(name = "montantttVente")
    private Double montantTtVente;

    @Column(name = "cycle")
    private Cycle cycle;

    //constructeur
    public VenteOiseau() {
        super();
    }

    public VenteOiseau(int nombre, String moyenPaiment, Double montantTtVente, Cycle cycle) {
        this.nombre = nombre;
        this.moyenPaiment = moyenPaiment;
        this.montantTtVente = montantTtVente;
        this.cycle = cycle;
    }
}
