package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "client")
@Getter
@Setter
@ToString
public class Client extends Model implements Serializable {

    @SerializedName("adrPostal")
    @Expose
    @Column(name = "adrPostal")
    private String adrPostal;

    @SerializedName("descript")
    @Expose
    @Column(name = "descript")
    private String descript;

    @SerializedName("latitude")
    @Expose
    @Column(name = "latitude")
    private String latitude;

    @SerializedName("longitude")
    @Expose
    @Column(name = "longitude")
    private String longitude;

    @SerializedName("lienImage")
    @Expose
    @Column(name = "lienImage")
    private String lienImage;

    @SerializedName("nom")
    @Expose
    @Column(name = "nom")
    private String nom;

    @SerializedName("numTel")
    @Expose
    @Column(name = "numTel")
    private String numTel;

    @SerializedName("pays")
    @Expose
    @Column(name = "pays")
    private String pays;

    @SerializedName("ville")
    @Expose
    @Column(name = "ville")
    private String ville;

    //constructeur avec tous les parametres
    public Client(String adrPostal, String descript, String latitude, String longitude, String lienImage, String nom, String numTel, String pays, String ville) {
        super();
        this.adrPostal = adrPostal;
        this.descript = descript;
        this.latitude = latitude;
        this.longitude = longitude;
        this.lienImage = lienImage;
        this.nom = nom;
        this.numTel = numTel;
        this.pays = pays;
        this.ville = ville;
    }

    //constructeur par defaut
    public Client() {
        super();
    }

}
