package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "temperature")
public class Temperature extends AbstractTache implements Serializable {

    @Column(name = "tempmatin")
    private Float tempMatin;

    @Column(name = "tempmidi")
    private Float tempMidi;

    @Column(name = "tempsoir")
    private Float tempSoir;

    @Column(name = "cycle")
    private Cycle cycle;

    //constructeur
    public Temperature() {
        super();
    }

    public Temperature(Float tempMatin, Float tempMidi, Float tempSoir, Cycle cycle) {
        super();
        this.tempMatin = tempMatin;
        this.tempMidi = tempMidi;
        this.tempSoir = tempSoir;
        this.cycle = cycle;
    }
}
