package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Table(name = "poussin")
public class Poussin extends Model implements Serializable {

    @Column(name = "identif",unique = true)
    private Long identif;

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "elevage")
    private Elevage elevage;


    //constructeur par defaut
    public Poussin() {
        super();
    }

    //constructeur avec parametres
    public Poussin(Long identif, String libelle,Elevage elevage) {
        super();
        this.identif = identif;
        this.libelle = libelle;
        this.elevage=elevage;
    }

    public List<Cycle> cycles(){
        return getMany(Cycle.class,"poussin");
    }


    public String toString(){
        return libelle;
    }


}
