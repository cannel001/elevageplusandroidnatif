package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "veterinaire")
@Getter
@Setter
@ToString
public class Veterinaire extends Model implements Serializable {

    @SerializedName(value = "dateCreation")
    @Expose
    @Column(name = "dateCreation")
    private Date dateCreation;

    @SerializedName(value = "dateUpdate")
    @Expose
    @Column(name = "dateUpdate")
    private Date dateUpdate;

    @SerializedName(value = "enabled")
    @Expose
    @Column(name = "enabled")
    private Boolean enabled;

    @SerializedName(value = "latitude")
    @Expose
    @Column(name = "latitude")
    private String latitude;

    @SerializedName(value = "lienPhoto")
    @Expose
    @Column(name = "lienPhoto")
    private String lienPhoto;

    @SerializedName(value = "longitude")
    @Expose
    @Column(name = "longitude")
    private String longitude;

    @SerializedName(value = "nationalite")
    @Expose
    @Column(name = "nationalite")
    private String nationalite;

    @SerializedName(value = "nom")
    @Expose
    @Column(name = "nom")
    private String nom;

    @SerializedName(value = "pays")
    @Expose
    @Column(name = "pays")
    private String pays;

    @SerializedName(value = "prenom")
    @Expose
    @Column(name = "prenom")
    private String prenom;

    @SerializedName(value = "specialite")
    @Expose
    @Column(name = "specialite")
    private String specialite;

    @SerializedName(value = "tel")
    @Expose
    @Column(name = "tel")
    private String tel;

    @SerializedName(value = "ville")
    @Expose
    @Column(name = "ville")
    private String ville;

    //constructeur par defaut
    public Veterinaire(){
        super();
    }

    //constructeur avec parametre
    public Veterinaire(Date dateCreation, Date dateUpdate, Boolean enabled, String latitude, String lienPhoto, String longitude, String nationalite, String nom, String pays, String prenom, String specialite, String tel, String ville) {
        super();
        this.dateCreation = dateCreation;
        this.dateUpdate = dateUpdate;
        this.enabled = enabled;
        this.latitude = latitude;
        this.lienPhoto = lienPhoto;
        this.longitude = longitude;
        this.nationalite = nationalite;
        this.nom = nom;
        this.pays = pays;
        this.prenom = prenom;
        this.specialite = specialite;
        this.tel = tel;
        this.ville = ville;
    }
}
