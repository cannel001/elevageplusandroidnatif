package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "publication")
public class Publication extends Model implements Serializable {

    @SerializedName("titre")
    @Expose
    @Column(name = "titre")
    private String titre;

    @SerializedName("intro")
    @Expose
    @Column(name = "intro")
    private String intro;

    @SerializedName("contenu")
    @Expose
    @Column(name = "contenu")
    private String contenu;

    @SerializedName("lienPhoto")
    @Expose
    @Column(name = "lienPhoto")
    private String lienPhoto;

    @SerializedName("resume")
    @Expose
    @Column(name = "resume")
    private String resume;

    @SerializedName("datePublication")
    @Expose
    @Column(name = "datePublication")
    private Date datePublication;

    @SerializedName("auteur")
    @Expose
    @Column(name = "auteur")
    private String auteur;

    //constructeur par defaut
    public Publication() {
        super();
    }

    //constructeur sans id
    public Publication(String titre, String intro, String contenu, String lienPhoto, String resume, Date datePublication, String auteur) {
        super();
        this.titre = titre;
        this.intro = intro;
        this.contenu = contenu;
        this.lienPhoto = lienPhoto;
        this.resume = resume;
        this.datePublication = datePublication;
        this.auteur = auteur;
    }
}
