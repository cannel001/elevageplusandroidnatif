package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "elevage")
@ToString
@Getter
@Setter
public class Elevage extends Model implements Serializable {

    @Column(name = "identif",unique = true)
    private Long identif;

    @Column(name ="libelle" )
    private String libelle;

    //constructeur par defaut
    public Elevage() {
        super();
    }

    //constructeur avec parametres
    public Elevage(Long identif, String libelle) {
        super();
        this.identif = identif;
        this.libelle = libelle;
    }

    public List<Poussin> poussins(){
        return getMany(Poussin.class,"elevage");
    }
}
