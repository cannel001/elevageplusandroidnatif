package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "observation")
public class Observation extends AbstractTache implements Serializable {

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "cycle")
    private Cycle cycle;

    //constructeur
    public Observation() {
        super();
    }

    public Observation(String libelle, Cycle cycle) {
        super();
        this.libelle = libelle;
        this.cycle = cycle;
    }
}
