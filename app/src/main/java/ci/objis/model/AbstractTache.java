package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public abstract class AbstractTache extends Model {

    @Column(name = "uid")
    private String uid;

    @Column(name = "oublie")
    private Boolean oublie;

    @Column(name = "lastModifieddate")
    private Date lastModifiedDate=new Date();

}
