package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "utilisateur")
@Getter
@Setter
@ToString
public class Utilisateur extends Model implements Serializable {

    @Column(name = "tel")
    @SerializedName(value = "tel")
    @Expose
    private String tel;

    @Column(name = "lienPhoto")
    @SerializedName(value = "lienPhoto")
    @Expose
    private String lienPhoto;

    @Column(name = "lienPhotoCouverture")
    @SerializedName(value = "lienPhotoCouverture")
    @Expose
    private String lienPhotoCouverture;

    @Column(name = "nom")
    @SerializedName(value = "nom")
    @Expose
    private String nom;

    @Column(name = "prenom")
    @SerializedName(value = "prenom")
    @Expose
    private String prenom;

    @Column(name = "description")
    @SerializedName(value = "description")
    @Expose
    private String description;

    @Column(name = "password")
    @SerializedName(value = "password")
    @Expose
    private String password;

    @Column(name = "pays")
    @SerializedName(value = "pays")
    @Expose
    private String pays;

    @Column(name = "ville")
    @SerializedName(value = "ville")
    @Expose
    private String ville;

    @Column(name = "commune")
    @SerializedName(value = "commune")
    @Expose
    private String commune;

    @Column(name = "dateCreation")
    @SerializedName(value = "dateCreation")
    @Expose
    private Date dateCreation;

    @Column(name = "dateUpdate")
    @SerializedName(value = "dateUpdate")
    @Expose
    private Date dateUpdate;

    @Column(name = "email")
    @SerializedName(value = "email")
    @Expose
    private String email;

    @Column(name = "isConfirmed")
    @SerializedName(value = "isConfirmed")
    @Expose
    private Boolean isConfirmed;

    @Column(name = "isBlocked")
    @SerializedName(value = "isBlocked")
    @Expose
    private Boolean isBlocked;

    @Column(name = "enabled")
    @SerializedName(value = "enabled")
    @Expose
    private Boolean enabled;

    //constructeur par defaut
    public Utilisateur() {
        super();
    }

    //constructeur avec tous les parametres
    public Utilisateur(String tel, String lienPhoto, String lienPhotoCouverture, String nom, String prenom, String description, String password, String pays, String ville, String commune, Date dateCreation, Date dateUpdate, String email, Boolean isConfirmed, Boolean isBlocked, Boolean enabled) {
        super();
        this.tel = tel;
        this.lienPhoto = lienPhoto;
        this.lienPhotoCouverture = lienPhotoCouverture;
        this.nom = nom;
        this.prenom = prenom;
        this.description = description;
        this.password = password;
        this.pays = pays;
        this.ville = ville;
        this.commune = commune;
        this.dateCreation = dateCreation;
        this.dateUpdate = dateUpdate;
        this.email = email;
        this.isConfirmed = isConfirmed;
        this.isBlocked = isBlocked;
        this.enabled = enabled;
    }

    public List<Cycle> cycles(){
        return getMany(Cycle.class,"utilisateur");
    }
}
