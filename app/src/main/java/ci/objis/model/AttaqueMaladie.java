package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Table(name = "AttaqueMaladie")
@Getter
@Setter
@ToString
public class AttaqueMaladie extends AbstractTache implements Serializable {

    @Column(name = "nommaladie")
    private String nomMaladie;

    @Column(name = "nbBete")
    private Long nbBete;

    @Column(name = "cycle")
    private Cycle cycle;

    //constructeur
    public AttaqueMaladie() {
        super();
    }

    public AttaqueMaladie(String nomMaladie, Long nbBete, Cycle cycle) {
        super();
        this.nomMaladie = nomMaladie;
        this.nbBete = nbBete;
        this.cycle = cycle;
    }
}
