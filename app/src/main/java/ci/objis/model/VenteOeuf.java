package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "venteoeuf")
public class VenteOeuf extends AbstractTache implements Serializable {

    @Column(name = "quantite")
    private int quantite;

    @Column(name = "moyenpaiement")
    private String moyenPaiement;

    @Column(name = "monttvente")
    private Double montTtVente;

    @Column(name = "cycle")
    private Cycle cycle;

    //constructeur
    public VenteOeuf() {
        super();
    }

    public VenteOeuf(int quantite,String moyenPaiement, Double montTtVente, Cycle cycle) {
        super();
        this.quantite = quantite;
        this.moyenPaiement = moyenPaiement;
        this.montTtVente = montTtVente;
        this.cycle = cycle;
    }
}
