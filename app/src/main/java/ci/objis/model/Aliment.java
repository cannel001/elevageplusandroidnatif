package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "aliment")
public class Aliment extends AbstractTache implements Serializable {

    @Column(name = "libelle")
    private String libelle;

    @Column(name = "quantite")
    private Float quantite;

    @Column(name = "unite")
    private String unite;

    @Column(name = "cycle")
    private Cycle cycle;

    //constructeur
    public Aliment() {
        super();
    }

    public Aliment(String libelle, Float quantite, String unite, Cycle cycle) {
        super();
        this.libelle = libelle;
        this.quantite = quantite;
        this.unite = unite;
        this.cycle = cycle;
    }
}
