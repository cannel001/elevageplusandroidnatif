package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Table(name = "medicament")
public class Medicament extends AbstractTache implements Serializable {

    @Column(name = "nommedoc")
    private String nomMedoc;

    @Column(name = "quantite")
    private int quantite;

    @Column(name = "cycle")
    private Cycle cycle;

    //constructeur
    public Medicament() {
        super();
    }

    public Medicament(int quantite, Cycle cycle) {
        super();
        this.quantite = quantite;
        this.cycle = cycle;
    }
}
