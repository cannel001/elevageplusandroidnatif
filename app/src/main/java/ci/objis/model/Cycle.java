package ci.objis.model;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Cette classe represente un cycle
 */
@Table(name = "cycle")
@ToString
@Getter
@Setter
public class Cycle extends Model implements Serializable {

    @Column(name = "uid")
    private String uid;

    @Column(name = "identif")
    private Long identif;

    @Column(name = "nom")
    private String nom;

    @Column(name = "nboisseau")
    private int nbOisseau;

    @Column(name = "datecycle")
    private Date datecycle;

    @Column(name = "dureecycle")
    private int dureeCycle;

    @Column(name = "poussin")
    private Poussin poussin;

    @Column(name = "utilisateur")
    private Utilisateur utilisateur;

    /**
     * constructeur par defaut
     */
    public Cycle() {
        super();
    }

    /**
     * constructeur avec parametres
     * @param identif
     * @param nom
     * @param nbOisseau
     * @param datecycle
     * @param dureeCycle
     */
    public Cycle(Long identif, String nom, int nbOisseau, Date datecycle, int dureeCycle) {
        super();
        this.identif = identif;
        this.nom = nom;
        this.nbOisseau = nbOisseau;
        this.datecycle = datecycle;
        this.dureeCycle = dureeCycle;
    }

    /**
     * methode permettant de recuperer la liste des aliments
     * @return
     */
    public List<Aliment> aliments() {
        return getMany(Aliment.class, "cycle");
    }

    /**
     * methode permettant de recuperer la liste des pertes
     * @return
     */
    public List<Perte> pertes() {
        return getMany(Perte.class, "cycle");
    }

    /**
     * methode permettant de recuperer la liste des ventes d'oiseaux
     * @return
     */
    public List<VenteOiseau> venteOiseaus() {
        return getMany(VenteOiseau.class, "cycle");
    }

}
