package ci.objis.config;

import ci.objis.R;

public final class VariableAnneeConfig {

    private VariableAnneeConfig(){}

    public static final String JANVIER="JANVIER";
    public static final String FEVRIER="FEVRIER";
    public static final String MARS="MARS";
    public static final String AVRIL="AVRIL";
    public static final String MAI="MAI";
    public static final String JUIN="JUIN";
    public static final String JUILLET="JUILLET";
    public static final String AOUT="AOUT";
    public static final String SEPTEMBRE="SEPTEMBRE";
    public static final String OCTOBRE="OCTOBRE";
    public static final String NOVEMBRE="NOVEMBRE";
    public static final String DECEMBRE="DECEMBRE";

}
