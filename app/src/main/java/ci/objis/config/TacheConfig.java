package ci.objis.config;

public final class TacheConfig {

    private TacheConfig(){}

    public final static String ALIMENT="aliment";
    public final static String ATTAQUEMALADIE="attaquemaladie";
    public final static String EAU="eau";
    public final static String MEDICAMENT="medicament";
    public final static String OBSERVATION="observation";
    public final static String PERTE="perte";
    public final static String POUSSIN="poussin";
    public final static String RECOLTEOEUF="recolteeouf";
    public final static String TEMPERATURE="temperature";
    public final static String VENTEOEUF="venteoeuf";
    public final static String VENTEOISEAU="venteoiseau";

}
