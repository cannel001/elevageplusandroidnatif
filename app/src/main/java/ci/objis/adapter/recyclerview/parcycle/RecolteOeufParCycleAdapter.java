package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.RecolteOeuf;

public class RecolteOeufParCycleAdapter extends RecyclerView.Adapter<RecolteOeufParCycleAdapter.RecolteoeufViewHolder> implements Filterable {
    //les proprietes
    private List<RecolteOeuf> recolteOeufList;
    private List<RecolteOeuf> listRecolteOeufFull;

    public RecolteOeufParCycleAdapter(List<RecolteOeuf> recolteOeufList) {
        this.recolteOeufList = recolteOeufList;
        listRecolteOeufFull = new ArrayList<>(recolteOeufList);
    }

    @NonNull
    @Override
    public RecolteOeufParCycleAdapter.RecolteoeufViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new RecolteoeufViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_recolteoeuf, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecolteOeufParCycleAdapter.RecolteoeufViewHolder recolteoeufViewHolder, int i) {
        RecolteOeuf recolteOeuf = recolteOeufList.get(i);

        if (recolteOeuf.getCycle() != null) {
            recolteoeufViewHolder.txtLibelle.setText((recolteOeuf.getQuantite() + " oeufs recoltés").toUpperCase());
        }
        recolteoeufViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(recolteOeuf.getLastModifiedDate()));
        recolteoeufViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(recolteOeuf.getLastModifiedDate()));
        recolteoeufViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(recolteOeuf.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return recolteOeufList.size();
    }

    @Override
    public Filter getFilter() {
        return recolteOeufFilter;
    }

    private Filter recolteOeufFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<RecolteOeuf> recolteOeufFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                recolteOeufFilterable.addAll(listRecolteOeufFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (RecolteOeuf recolteOeuf : listRecolteOeufFull) {
                    if (recolteOeuf.getCycle().getPoussin().getLibelle().toLowerCase().contains(pattern)) {
                        recolteOeufFilterable.add(recolteOeuf);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = recolteOeufFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            recolteOeufList.clear();
            recolteOeufList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class RecolteoeufViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public RecolteoeufViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcyclerecolteoeuf_libelle);
            txtJour = itemView.findViewById(R.id.itemcyclerecolteoeuf_jour);
            txtMois = itemView.findViewById(R.id.itemcyclerecolteoeuf_mois);
            txtAnnee = itemView.findViewById(R.id.itemcyclerecolteoeuf_annee);
        }
    }
}
