package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.VenteOiseau;

public class VenteOIseauParCycleAdapter extends RecyclerView.Adapter<VenteOIseauParCycleAdapter.VenteOiseauViewHolder> implements Filterable {
    //les proprietes
    private List<VenteOiseau> venteOiseauList;
    private List<VenteOiseau> listVenteOiseauFull;

    public VenteOIseauParCycleAdapter(List<VenteOiseau> venteOiseauList) {
        this.venteOiseauList = venteOiseauList;
        listVenteOiseauFull = new ArrayList<>(venteOiseauList);
    }

    @NonNull
    @Override
    public VenteOIseauParCycleAdapter.VenteOiseauViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VenteOiseauViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_venteoiseau, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VenteOIseauParCycleAdapter.VenteOiseauViewHolder venteOiseauViewHolder, int i) {
        VenteOiseau venteOiseau = venteOiseauList.get(i);

        if (venteOiseau.getCycle() != null) {
            venteOiseauViewHolder.txtLibelle.setText((venteOiseau.getNombre() + " " + venteOiseau.getCycle().getPoussin().getLibelle() + " vendus").toUpperCase());
        }
        venteOiseauViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(venteOiseau.getLastModifiedDate()));
        venteOiseauViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(venteOiseau.getLastModifiedDate()));
        venteOiseauViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return venteOiseauList.size();
    }

    @Override
    public Filter getFilter() {
        return venteOiseauFilter;
    }

    private Filter venteOiseauFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<VenteOiseau> venteOiseauFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                venteOiseauFilterable.addAll(listVenteOiseauFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (VenteOiseau venteOiseau : listVenteOiseauFull) {
                    if (venteOiseau.getMoyenPaiment().toLowerCase().contains(pattern)) {
                        venteOiseauFilterable.add(venteOiseau);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = venteOiseauFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            venteOiseauList.clear();
            venteOiseauList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class VenteOiseauViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public VenteOiseauViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcycleventeoiseau_libelle);
            txtJour = itemView.findViewById(R.id.itemcycleventeoiseau_jour);
            txtMois = itemView.findViewById(R.id.itemcycleventeoiseau_mois);
            txtAnnee = itemView.findViewById(R.id.itemcycleventeoiseau_annee);
        }
    }
}
