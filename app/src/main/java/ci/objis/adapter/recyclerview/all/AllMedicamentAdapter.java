package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Medicament;
import ci.objis.model.Perte;

public class AllMedicamentAdapter extends RecyclerView.Adapter<AllMedicamentAdapter.AllMedicamentViewHolder>  implements Filterable {
    //les proprietes
    private List<Medicament> medicaments;
    private List<Medicament> listMedicamentFull;

    //constructeur
    public AllMedicamentAdapter(List<Medicament> medicaments) {
        this.medicaments = medicaments;
        listMedicamentFull = new ArrayList<>(medicaments);
    }

    @NonNull
    @Override
    public AllMedicamentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AllMedicamentViewHolder allMedicamentViewHolder=new AllMedicamentViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_allmedicament,viewGroup,false));

        return allMedicamentViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AllMedicamentViewHolder allMedicamentViewHolder, int position) {
        Medicament medicament=medicaments.get(position);
        allMedicamentViewHolder.txtLibelle.setText((medicament.getQuantite()+" "+medicament.getNomMedoc()+" administré(s) aux "+medicament.getCycle().getPoussin().getLibelle()+" du cycle "+medicament.getCycle().getNom()).toUpperCase());
        LocalDate localDate=LocalDate.fromDateFields(medicament.getLastModifiedDate());
        allMedicamentViewHolder.txtJour.setText(String.valueOf(localDate.getDayOfMonth()));
        allMedicamentViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(medicament.getLastModifiedDate()));
        allMedicamentViewHolder.txtAnnee.setText(String.valueOf(localDate.getYear()));
    }

    @Override
    public int getItemCount() {
        return medicaments.size();
    }

    @Override
    public Filter getFilter() {
        return medicamentFilter;
    }

    private Filter medicamentFilter=new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Medicament> filteredList=new ArrayList<>();

            if(constraint==null || constraint.length()==0) {
                filteredList.addAll(listMedicamentFull);
            }else{
                String filtrePattern=constraint.toString().toLowerCase();

                for (Medicament medicament: listMedicamentFull) {
                    if(medicament.getNomMedoc().toLowerCase().contains(filtrePattern)){
                        filteredList.add(medicament);
                    }
                }
            }

            FilterResults results=new FilterResults();
            results.values=filteredList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            medicaments.clear();
            medicaments.addAll((List)results.values);
            notifyDataSetChanged();
        }
    };

    class AllMedicamentViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public AllMedicamentViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle=itemView.findViewById(R.id.itemallmedicament_libelle);
            txtJour=itemView.findViewById(R.id.itemallmedicament_jour);
            txtMois=itemView.findViewById(R.id.itemallmedicament_mois);
            txtAnnee=itemView.findViewById(R.id.itemallmedicament_annee);
        }
    }
}
