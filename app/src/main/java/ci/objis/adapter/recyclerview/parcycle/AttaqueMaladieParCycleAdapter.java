package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.AttaqueMaladie;

public class AttaqueMaladieParCycleAdapter extends RecyclerView.Adapter<AttaqueMaladieParCycleAdapter.AttaqueMaladieViewHolder> implements Filterable {
    //les proprietes
    private List<AttaqueMaladie> attaqueMaladieList;
    private List<AttaqueMaladie> listAttaqueMaladieFull;

    public AttaqueMaladieParCycleAdapter(List<AttaqueMaladie> attaqueMaladieList) {
        this.attaqueMaladieList = attaqueMaladieList;
        listAttaqueMaladieFull = new ArrayList<>(attaqueMaladieList);
    }

    @NonNull
    @Override
    public AttaqueMaladieParCycleAdapter.AttaqueMaladieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AttaqueMaladieViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_attaquemaladie, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AttaqueMaladieParCycleAdapter.AttaqueMaladieViewHolder attaqueMaladieViewHolder, int i) {
        AttaqueMaladie attaqueMaladie = attaqueMaladieList.get(i);

        if (attaqueMaladie.getCycle() != null) {
            attaqueMaladieViewHolder.txtLibelle.setText((attaqueMaladie.getNbBete() + " " + attaqueMaladie.getCycle().getPoussin().getLibelle() + " atteind(s)").toUpperCase());
        }
        attaqueMaladieViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(attaqueMaladie.getLastModifiedDate()));
        attaqueMaladieViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(attaqueMaladie.getLastModifiedDate()));
        attaqueMaladieViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(attaqueMaladie.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return attaqueMaladieList.size();
    }

    @Override
    public Filter getFilter() {
        return attaqueMaladieFilter;
    }

    private Filter attaqueMaladieFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<AttaqueMaladie> attaqueMaladiesFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                attaqueMaladiesFilterable.addAll(listAttaqueMaladieFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (AttaqueMaladie attaqueMaladie : listAttaqueMaladieFull) {
                    if (attaqueMaladie.getNomMaladie().toLowerCase().contains(pattern)) {
                        attaqueMaladiesFilterable.add(attaqueMaladie);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = attaqueMaladiesFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            attaqueMaladieList.clear();
            attaqueMaladieList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class AttaqueMaladieViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public AttaqueMaladieViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcycleattaquemaladie_libelle);
            txtJour = itemView.findViewById(R.id.itemcycleattaquemaladie_jour);
            txtMois = itemView.findViewById(R.id.itemcycleattaquemaladie_mois);
            txtAnnee = itemView.findViewById(R.id.itemcycleattaquemaladie_annee);
        }
    }
}
