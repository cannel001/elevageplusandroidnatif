package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Temperature;

public class TemperatureParCycleAdapter extends RecyclerView.Adapter<TemperatureParCycleAdapter.TemperatureViewHolder> implements Filterable {
    //les proprietes
    private List<Temperature> temperatureList;
    private List<Temperature> listTemperatureFull;

    public TemperatureParCycleAdapter(List<Temperature> temperatureList) {
        this.temperatureList = temperatureList;
        listTemperatureFull = new ArrayList<>(temperatureList);
    }

    @NonNull
    @Override
    public TemperatureParCycleAdapter.TemperatureViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new TemperatureViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_temperature, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull TemperatureParCycleAdapter.TemperatureViewHolder temperatureViewHolder, int i) {
        Temperature temperature = temperatureList.get(i);

        if (temperature.getCycle() != null) {
            temperatureViewHolder.txtLibelle.setText(("Jour:" + temperature.getTempMatin() + " - Midi:" + temperature.getTempMidi() + " - Soir:" + temperature.getTempSoir()).toUpperCase());
        }
        temperatureViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(temperature.getLastModifiedDate()));
        temperatureViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(temperature.getLastModifiedDate()));
        temperatureViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(temperature.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return temperatureList.size();
    }

    @Override
    public Filter getFilter() {
        return temperatureFilter;
    }

    private Filter temperatureFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Temperature> temperatureFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                temperatureFilterable.addAll(listTemperatureFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (Temperature temperature : listTemperatureFull) {
                    if (temperature.getCycle().getPoussin().getLibelle().toLowerCase().contains(pattern)) {
                        temperatureFilterable.add(temperature);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = temperatureFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            temperatureList.clear();
            temperatureList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class TemperatureViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public TemperatureViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcycletemperature_libelle);
            txtJour = itemView.findViewById(R.id.itemcycletemperature_jour);
            txtMois = itemView.findViewById(R.id.itemcycletemperature_mois);
            txtAnnee = itemView.findViewById(R.id.itemcycletemperature_annee);
        }
    }
}
