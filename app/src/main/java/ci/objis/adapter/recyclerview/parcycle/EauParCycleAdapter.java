package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Eau;

public class EauParCycleAdapter extends RecyclerView.Adapter<EauParCycleAdapter.EauViewHolder> implements Filterable {
    //les proprietes
    private List<Eau> eauList;
    private List<Eau> listEauFull;

    public EauParCycleAdapter(List<Eau> eauList) {
        this.eauList = eauList;
        listEauFull = new ArrayList<>(eauList);
    }

    @NonNull
    @Override
    public EauParCycleAdapter.EauViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new EauViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_eau, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull EauParCycleAdapter.EauViewHolder eauViewHolder, int i) {
        Eau eau = eauList.get(i);

        if (eau.getCycle() != null) {
            eauViewHolder.txtLibelle.setText((eau.getQuantite() + " litres donnés").toUpperCase());
        }

        eauViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(eau.getLastModifiedDate()));
        eauViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(eau.getLastModifiedDate()));
        eauViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(eau.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return eauList.size();
    }

    @Override
    public Filter getFilter() {
        return eauFilter;
    }

    private Filter eauFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Eau> eauFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                eauFilterable.addAll(listEauFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (Eau eau : listEauFull) {
                    if (eau.getCycle().getPoussin().getLibelle().toLowerCase().contains(pattern)) {
                        eauFilterable.add(eau);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = eauFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            eauList.clear();
            eauList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class EauViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public EauViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcycleeau_libelle);
            txtJour = itemView.findViewById(R.id.itemcycleeau_jour);
            txtMois = itemView.findViewById(R.id.itemcycleeau_mois);
            txtAnnee = itemView.findViewById(R.id.itemcycleeau_annee);
        }
    }
}
