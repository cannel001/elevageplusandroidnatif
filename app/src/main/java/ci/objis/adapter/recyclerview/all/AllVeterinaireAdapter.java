package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jackandphantom.circularimageview.RoundedImage;
import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.zip.Inflater;

import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.Veterinaire;
import ci.objis.ui.veterinaire.VeterinaireSelectActivity;

public class AllVeterinaireAdapter extends RecyclerView.Adapter<AllVeterinaireAdapter.AllVeterinaireViewHolder> {

    //les proprietes
    private Context context;
    private List<Veterinaire> veterinaireList;

    //constructeur
    public AllVeterinaireAdapter(Context context,List<Veterinaire> veterinaires){
        this.context=context;
        this.veterinaireList=veterinaires;
    }


    @NonNull
    @Override
    public AllVeterinaireViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        AllVeterinaireViewHolder allVeterinaireViewHolder=new AllVeterinaireViewHolder(LayoutInflater.from(context).inflate(R.layout.item_allveterinaire,viewGroup,false));

        return allVeterinaireViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AllVeterinaireViewHolder allVeterinaireViewHolder, int i) {
        Veterinaire veterinaire=veterinaireList.get(i);

        allVeterinaireViewHolder.txtSpecialite.setText(String.format("Spécialiste en %s", veterinaire.getSpecialite().toLowerCase()));
        allVeterinaireViewHolder.txtNomPrenom.setText(veterinaire.getNom());

        if(!TextUtils.isEmpty(veterinaire.getLienPhoto())){
            //ajout de l'image avec picasso
            Picasso.get()
                    .load(veterinaire.getLienPhoto())
                    .placeholder(R.drawable.avatardefault)
                    .error(R.drawable.avatardefault)
                    .fit()
                    .into(allVeterinaireViewHolder.imgProfil);
        }

        if(!"".equals(veterinaire.getPrenom())){
            allVeterinaireViewHolder.txtNomPrenom.setText(String.format("%s %s",allVeterinaireViewHolder.txtNomPrenom.getText(),veterinaire.getPrenom()));
        }

        allVeterinaireViewHolder.lnCall.setOnClickListener(v->{
            Uri tel=Uri.parse("tel:"+veterinaire.getTel());
            Intent intent=new Intent(Intent.ACTION_DIAL,tel);
            context.startActivity(intent);
        });

        allVeterinaireViewHolder.itemView.setOnClickListener(v->{
            Intent intent=new Intent(context, VeterinaireSelectActivity.class);
            intent.putExtra(StartUp.VETERINAIRESELECT,veterinaire);
            context.startActivity(intent);
        });


    }

    @Override
    public int getItemCount() {
        return veterinaireList.size();
    }

    public class AllVeterinaireViewHolder  extends RecyclerView.ViewHolder {

        //les proprietes
        TextView txtNomPrenom;
        TextView txtSpecialite;

        RoundedImage imgProfil;
        LinearLayout lnCall;

        public AllVeterinaireViewHolder(@NonNull View itemView) {
            super(itemView);

            txtNomPrenom=itemView.findViewById(R.id.itemallveterinaire_nomprenom);
            txtSpecialite=itemView.findViewById(R.id.itemallveterinaire_specialite);
            imgProfil=itemView.findViewById(R.id.itemallveterinaire_photo);
            lnCall=itemView.findViewById(R.id.itemallveterinaire_appel);
        }
    }
}
