package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.VenteOeuf;

public class VenteOeufParCycleAdapter extends RecyclerView.Adapter<VenteOeufParCycleAdapter.VenteOeufViewHolder> implements Filterable {
    //les proprietes
    private List<VenteOeuf> venteOeufList;
    private List<VenteOeuf> listVenteOeufFull;

    public VenteOeufParCycleAdapter(List<VenteOeuf> venteOeufList) {
        this.venteOeufList = venteOeufList;
        listVenteOeufFull = new ArrayList<>(venteOeufList);
    }

    @NonNull
    @Override
    public VenteOeufParCycleAdapter.VenteOeufViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new VenteOeufViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_venteoeuf, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VenteOeufParCycleAdapter.VenteOeufViewHolder venteOeufViewHolder, int i) {
        VenteOeuf venteOeuf = venteOeufList.get(i);

        if (venteOeuf.getCycle() != null) {
            venteOeufViewHolder.txtLibelle.setText((venteOeuf.getQuantite() + " oeufs vendus").toUpperCase());
        }
        venteOeufViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(venteOeuf.getLastModifiedDate()));
        venteOeufViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(venteOeuf.getLastModifiedDate()));
        venteOeufViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate()));
    }


    @Override
    public int getItemCount() {
        return venteOeufList.size();
    }

    @Override
    public Filter getFilter() {
        return venteOeufFilter;
    }

    private Filter venteOeufFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<VenteOeuf> venteOeufFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                venteOeufFilterable.addAll(listVenteOeufFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (VenteOeuf venteOeuf : venteOeufList) {
                    if (venteOeuf.getCycle().getPoussin().getLibelle().toLowerCase().contains(pattern)) {
                        venteOeufFilterable.add(venteOeuf);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = venteOeufFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            venteOeufList.clear();
            venteOeufList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class VenteOeufViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public VenteOeufViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcycleventeoeuf_libelle);
            txtJour = itemView.findViewById(R.id.itemcycleventeoeuf_jour);
            txtMois = itemView.findViewById(R.id.itemcycleventeoeuf_mois);
            txtAnnee = itemView.findViewById(R.id.itemcycleventeoeuf_annee);
        }
    }
}
