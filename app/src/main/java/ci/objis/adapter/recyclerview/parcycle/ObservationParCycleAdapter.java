package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Observation;

public class ObservationParCycleAdapter extends RecyclerView.Adapter<ObservationParCycleAdapter.ObservationViewHolder> implements Filterable {
    //les proprietes
    private List<Observation> observationList;
    private List<Observation> listObservationFull;

    public ObservationParCycleAdapter(List<Observation> observationList) {
        this.observationList = observationList;
        listObservationFull = new ArrayList<>(observationList);
    }

    @NonNull
    @Override
    public ObservationParCycleAdapter.ObservationViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ObservationViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_observation, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ObservationParCycleAdapter.ObservationViewHolder observationViewHolder, int i) {
        Observation observation = observationList.get(i);

        if (observation.getCycle() != null) {
            observationViewHolder.txtLibelle.setText((observation.getLibelle()).toUpperCase());
        }
        observationViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(observation.getLastModifiedDate()));
        observationViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(observation.getLastModifiedDate()));
        observationViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(observation.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return observationList.size();
    }

    @Override
    public Filter getFilter() {
        return observationFilter;
    }

    private Filter observationFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Observation> observationFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                observationFilterable.addAll(listObservationFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (Observation observation : listObservationFull) {
                    if (observation.getLibelle().toLowerCase().contains(pattern)) {
                        observationFilterable.add(observation);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = observationFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            observationList.clear();
            observationList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class ObservationViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public ObservationViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcycleobservation_libelle);
            txtJour = itemView.findViewById(R.id.itemcycleobservation_jour);
            txtMois = itemView.findViewById(R.id.itemcycleobservation_mois);
            txtAnnee = itemView.findViewById(R.id.itemcycleobservation_annee);
        }
    }
}
