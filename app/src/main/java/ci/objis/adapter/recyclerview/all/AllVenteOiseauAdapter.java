package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ci.objis.R;
import ci.objis.model.VenteOiseau;

public class AllVenteOiseauAdapter extends RecyclerView.Adapter<AllVenteOiseauAdapter.AllVenteOiseauViewHolder> implements Filterable {

    //les proprietes
    private final static String TAG = AllVenteOiseauAdapter.class.getSimpleName();
    private List<VenteOiseau> venteOiseauList;
    private List<VenteOiseau> listVenteOiseauFull;
    private LocalDate localDate;

    //constructeur
    public AllVenteOiseauAdapter(List<VenteOiseau> venteOiseauList) {
        this.venteOiseauList = venteOiseauList;
        listVenteOiseauFull = new ArrayList<>(venteOiseauList);
    }

    @NonNull
    @Override
    public AllVenteOiseauViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AllVenteOiseauViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_all_venteoiseau, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AllVenteOiseauViewHolder allVenteOiseauViewHolder, int position) {
        VenteOiseau venteOiseau = venteOiseauList.get(position);
        allVenteOiseauViewHolder.txtLibelle.setText((venteOiseau.getNombre() + " " + venteOiseau.getCycle().getPoussin().getLibelle() + " du cycle " + venteOiseau.getCycle().getNom() + " vendus").toUpperCase());
        LocalDate localDate = LocalDate.fromDateFields(venteOiseau.getLastModifiedDate());
        allVenteOiseauViewHolder.txtJour.setText(String.valueOf(localDate.getDayOfMonth()));
        allVenteOiseauViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(venteOiseau.getLastModifiedDate()));
        allVenteOiseauViewHolder.txtAnnee.setText(String.valueOf(localDate.getYear()));
    }

    @Override
    public int getItemCount() {
        return venteOiseauList.size();
    }

    @Override
    public Filter getFilter() {
        return venteOiseauFilter;
    }

    private Filter venteOiseauFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<VenteOiseau> filterableVenteOiseau = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filterableVenteOiseau.addAll(listVenteOiseauFull);
            } else {
                String venteOiseauPattern = constraint.toString().toLowerCase();

                for (VenteOiseau venteOiseau : listVenteOiseauFull) {
                    if (venteOiseau.getMoyenPaiment().toLowerCase().contains(venteOiseauPattern)) {
                        filterableVenteOiseau.add(venteOiseau);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filterableVenteOiseau;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            venteOiseauList.clear();
            venteOiseauList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class AllVenteOiseauViewHolder extends RecyclerView.ViewHolder {
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public AllVenteOiseauViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemallventeoiseau_libelle);
            txtJour = itemView.findViewById(R.id.itemallventeoiseau_jour);
            txtMois = itemView.findViewById(R.id.itemallventeoiseau_mois);
            txtAnnee = itemView.findViewById(R.id.itemallventeoiseau_annee);
        }
    }
}
