package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ci.objis.R;
import ci.objis.model.Publication;
import ci.objis.ui.conseil.ConseilSelectActivity;

public class AllConseilAdapter extends RecyclerView.Adapter<AllConseilAdapter.HolderPublication> implements Filterable {

    //les proprietes
    private static String TAG="AllConseilAdapter";
    private List<Publication> listPublication;
    private Context context;
    public static String ARTICLESELECT="ARTICLE SELECTIONNEE";

    //constructeur
    public AllConseilAdapter(List<Publication> listPublication, Context context) {
        this.listPublication = listPublication;
        this.context = context;
    }

    @NonNull
    @Override
    public HolderPublication onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        final View view;
        view = LayoutInflater.from(this.context).inflate(R.layout.item_publication_cardview, viewGroup, false);
        final HolderPublication holderPublication = new HolderPublication(view);

        holderPublication.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos=holderPublication.getAdapterPosition();
                Intent intent=new Intent(context, ConseilSelectActivity.class);
                intent.putExtra(ARTICLESELECT,listPublication.get(pos));
                context.startActivity(intent);
            }
        });

        return holderPublication;
    }

    @Override
    public void onBindViewHolder(@NonNull final HolderPublication holderPublication, int i) {

        Publication publication=listPublication.get(i);

        holderPublication.txtTitre.setText(publication.getTitre());
        holderPublication.txtDatePublication.setText(publication.getDatePublication().toString());

        //ajout de l'image avec picasso
        Picasso.get()
                .load(publication.getLienPhoto())
                .placeholder(R.drawable.ic_camera_front)
                .error(R.drawable.ic_camera_front)
                .fit()
                .into(holderPublication.imagePublication);
    }

    @Override
    public int getItemCount() {
        return listPublication.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    class HolderPublication extends RecyclerView.ViewHolder{

        //les proprietes
        TextView txtTitre;
        TextView txtDatePublication;
        ImageView imagePublication;

        public HolderPublication(@NonNull View itemView) {

            super(itemView);

            txtTitre=itemView.findViewById(R.id.item_titrepublication);
            txtDatePublication=itemView.findViewById(R.id.item_datepublication);
            imagePublication=itemView.findViewById(R.id.item_imagepublication);

        }
    }


}
