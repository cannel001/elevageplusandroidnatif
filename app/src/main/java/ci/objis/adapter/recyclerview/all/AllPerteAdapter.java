package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Perte;

public class AllPerteAdapter extends RecyclerView.Adapter<AllPerteAdapter.AllPerteViewHolder> implements Filterable {

    //les proprietes
    private List<Perte> pertes;
    private List<Perte> listPerteFull;
    private LocalDate localDate;

    //constructeur
    public AllPerteAdapter(List<Perte> pertes) {
        this.pertes = pertes;
        listPerteFull = new ArrayList<>(pertes);
    }

    @NonNull
    @Override
    public AllPerteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AllPerteViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_all_perte, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AllPerteViewHolder allPerteViewHolder, int position) {
        Perte perte = pertes.get(position);
        allPerteViewHolder.txtLibelle.setText((perte.getQuantite() + " " + perte.getCycle().getPoussin().getLibelle() + " du cycle " + perte.getCycle().getNom() + " perdus").toUpperCase());
        LocalDate localDate = LocalDate.fromDateFields(perte.getLastModifiedDate());
        allPerteViewHolder.txtJour.setText(String.valueOf(localDate.getDayOfMonth()));
        allPerteViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(perte.getLastModifiedDate()));
        allPerteViewHolder.txtAnnee.setText(String.valueOf(localDate.getYear()));
    }

    @Override
    public int getItemCount() {
        return pertes.size();
    }

    @Override
    public Filter getFilter() {
        return perteFilter;
    }

    private Filter perteFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Perte> filterablePerte = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filterablePerte.addAll(listPerteFull);
            } else {
                String patternPerte = constraint.toString().toLowerCase();

                for (Perte perte : listPerteFull) {
                    if (perte.getCycle().getNom().contains(patternPerte)) {
                        filterablePerte.add(perte);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filterablePerte;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            pertes.clear();
            pertes.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    class AllPerteViewHolder extends RecyclerView.ViewHolder {
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public AllPerteViewHolder(@NonNull View itemView) {
            super(itemView);

            txtLibelle = itemView.findViewById(R.id.itemallperte_libelle_allperte);
            txtJour = itemView.findViewById(R.id.itemallperte_jour);
            txtMois = itemView.findViewById(R.id.itemallperte_moisallperte);
            txtAnnee = itemView.findViewById(R.id.itemallperte_anneeallperte);
        }
    }
}
