package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ci.objis.R;
import ci.objis.model.Client;
import ci.objis.ui.client.ClientSelectActivity;

public class AllClientAdapter extends RecyclerView.Adapter<AllClientAdapter.HolderClient> {

    //les proprietes
    private Context context;
    private List<Client> listClients;
    public static String CLIENTSELECT="CLIENT SELECTIONNEE";

    //constructeur
    public AllClientAdapter(Context context, List<Client> listClients){
        this.context=context;
        this.listClients=listClients;
    }


    @NonNull
    @Override
    public HolderClient onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        final HolderClient holderClient=new HolderClient(LayoutInflater.from(context).inflate(R.layout.item_all_client_cardview,viewGroup,false));

        holderClient.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, ClientSelectActivity.class);
                Client client=listClients.get(holderClient.getAdapterPosition());
                intent.putExtra(CLIENTSELECT,client);
                context.startActivity(intent);
            }
        });

        return holderClient;

    }

    @Override
    public void onBindViewHolder(@NonNull HolderClient holderClient, int i) {

        Client client=listClients.get(i);
        holderClient.txtTitre.setText(client.getNom());
        holderClient.txtPaysVille.setText(client.getVille()+", "+client.getPays());

        //ajout de l'image avec picasso
        Picasso.get()
                .load(client.getLienImage())
                .placeholder(R.drawable.ic_camera_front)
                .error(R.drawable.ic_camera_front)
                .fit()
                .into(holderClient.imageClient);

        //holderClient.ratingBar.setRating();

    }

    @Override
    public int getItemCount() {
        return listClients.size();
    }

    class HolderClient extends RecyclerView.ViewHolder{

        //les proprietes
        TextView txtTitre;
        TextView txtPaysVille;
        ImageView imageClient;
        RatingBar ratingBar;

        public HolderClient(@NonNull View itemView) {
            super(itemView);
            txtTitre=itemView.findViewById(R.id.itemClientcarview_titre);
            txtPaysVille=itemView.findViewById(R.id.itemClientcarview_paysville);
            imageClient=itemView.findViewById(R.id.itemClientcarview_image);
            ratingBar=itemView.findViewById(R.id.itemClientcarview_rating);
        }
    }
}
