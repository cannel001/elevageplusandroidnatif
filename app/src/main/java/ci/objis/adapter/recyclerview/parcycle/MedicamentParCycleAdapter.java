package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import ci.objis.R;
import ci.objis.model.Medicament;

public class MedicamentParCycleAdapter extends RecyclerView.Adapter<MedicamentParCycleAdapter.MedicamentViewHolder> implements Filterable {
    //les proprietes
    private List<Medicament> medicamentList;
    private List<Medicament> listMedicamentFull;

    public MedicamentParCycleAdapter(List<Medicament> medicamentList) {
        this.medicamentList = medicamentList;
        listMedicamentFull = new ArrayList<>(medicamentList);
    }

    @NonNull
    @Override
    public MedicamentParCycleAdapter.MedicamentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MedicamentViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_medicament, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MedicamentParCycleAdapter.MedicamentViewHolder medicamentViewHolder, int i) {
        Medicament medicament = medicamentList.get(i);

        if (medicament.getCycle() != null) {
            medicamentViewHolder.txtLibelle.setText((medicament.getQuantite() + " " + medicament.getNomMedoc() + " administré(s) aux " + medicament.getCycle().getPoussin().getLibelle()).toUpperCase());
        }
        medicamentViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(medicament.getLastModifiedDate()));
        medicamentViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(medicament.getLastModifiedDate()));
        medicamentViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return medicamentList.size();
    }

    @Override
    public Filter getFilter() {
        return medicamentFilter;
    }

    private Filter medicamentFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Medicament> medicamentFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                medicamentFilterable.addAll(listMedicamentFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (Medicament medicament : listMedicamentFull) {
                    if (medicament.getNomMedoc().toLowerCase().contains(pattern)) {
                        medicamentFilterable.add(medicament);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = medicamentFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            medicamentList.clear();
            medicamentList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class MedicamentViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public MedicamentViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcyclemedicament_libelle);
            txtJour = itemView.findViewById(R.id.itemcyclemedicament_jour);
            txtMois = itemView.findViewById(R.id.itemcyclemedicament_mois);
            txtAnnee = itemView.findViewById(R.id.itemcyclemedicament_annee);
        }
    }
}
