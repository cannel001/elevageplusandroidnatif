package ci.objis.adapter.recyclerview.parcycle;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Perte;

public class PerteParCycleAdapter extends RecyclerView.Adapter<PerteParCycleAdapter.PerteViewHoler> implements Filterable {
    //les proprietes
    private List<Perte> perteList;
    private List<Perte> listPerteFull;

    public PerteParCycleAdapter(List<Perte> perteList) {
        this.perteList = perteList;
        listPerteFull = new ArrayList<>(perteList);
    }

    @NonNull
    @Override
    public PerteParCycleAdapter.PerteViewHoler onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new PerteViewHoler(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_perte, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull PerteParCycleAdapter.PerteViewHoler perteViewHoler, int i) {
        Perte perte = perteList.get(i);

        if (perte.getCycle() != null) {
            perteViewHoler.txtLibelle.setText((perte.getQuantite() + " " + perte.getCycle().getPoussin().getLibelle() + " perdus").toUpperCase());
        }
        perteViewHoler.txtJour.setText(new SimpleDateFormat("dd").format(perte.getLastModifiedDate()));
        perteViewHoler.txtMois.setText(new SimpleDateFormat("MMMM").format(perte.getLastModifiedDate()));
        perteViewHoler.txtAnnee.setText(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return perteList.size();
    }

    @Override
    public Filter getFilter() {
        return listPerteFilter;
    }

    private Filter listPerteFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Perte> perteFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                perteFilterable.addAll(listPerteFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (Perte perte : listPerteFull) {
                    if (perte.getCycle().getPoussin().getLibelle().toLowerCase().contains(pattern)) {
                        perteFilterable.add(perte);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = perteFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            perteList.clear();
            perteList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class PerteViewHoler extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public PerteViewHoler(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcycleperte_libelle);
            txtJour = itemView.findViewById(R.id.itemcycleperte_jour);
            txtMois = itemView.findViewById(R.id.itemcycleperte_mois);
            txtAnnee = itemView.findViewById(R.id.itemcycleperte_annee);
        }
    }
}
