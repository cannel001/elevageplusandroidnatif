package ci.objis.adapter.recyclerview;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.ui.cycle.SuiviCycleActivity;
import ci.objis.model.Cycle;

public class CycleEnCoursAdapter extends RecyclerView.Adapter<CycleEnCoursAdapter.HolderCycleEnCours> {

    //les proprietes
    private final static String TAG="CycleEnCoursAdapter";

    private Context context;
    private List<Map<String, Object>> listMap;

    private Map<String,Object> maMap;
    private int percent;
    private Cycle cycle;
    private int positionSelect;

    //constructeur par defaut
    public CycleEnCoursAdapter(Context context,List<Map<String, Object>> listMap) {
        this.context = context;
        this.listMap = listMap;

        Log.i(TAG,"passage dans allCyclAndPercentbyLibelleElevage "+listMap.toString());
    }

    @NonNull
    @Override
    public HolderCycleEnCours onCreateViewHolder(@NonNull ViewGroup viewGroup, final int i) {

        final HolderCycleEnCours holderCycleEnCours=new HolderCycleEnCours(LayoutInflater.from(context).inflate(R.layout.item_cycleencours,viewGroup,false));


        holderCycleEnCours.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cycle= (Cycle) listMap.get(holderCycleEnCours.getAdapterPosition()).get("cycle");
                Intent intent=new Intent(context, SuiviCycleActivity.class);
                intent.putExtra(StartUp.CYCLESELECT,cycle);

                Log.i(TAG,"passage dans cycle en cours adapter avec "+cycle.toString());

                context.startActivity(intent);
            }
        });

        return holderCycleEnCours;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderCycleEnCours holderCycleEnCours, int position) {

        //recuperation du map
        maMap=listMap.get(position);

        //recuperation
        percent= (int) maMap.get("percent");
        cycle= (Cycle) maMap.get("cycle");
        positionSelect=position;

        Log.i(TAG,"passage dans allCyclAndPercentbyLibelleElevage avec percent "+percent);
        Log.i(TAG,"passage dans allCyclAndPercentbyLibelleElevage avec cycle "+cycle.toString());

        holderCycleEnCours.txtPourcentageCycle.setText(percent+"%");
        holderCycleEnCours.txtInfoCycle.setText("Cycle de "+cycle.getDureeCycle()+" jours pour "+cycle.getPoussin().getLibelle());

    }

    @Override
    public int getItemCount() {
        return listMap.size();
    }

    public void deleteItem(int position) {

    }

    class HolderCycleEnCours extends RecyclerView.ViewHolder{

        //les proprietes
        TextView txtInfoCycle;
        TextView txtPourcentageCycle;
        ImageView imageOiseau;

        public HolderCycleEnCours(@NonNull View itemView) {
            super(itemView);
            txtInfoCycle=itemView.findViewById(R.id.itemcycleencours_infocyle);
            txtPourcentageCycle=itemView.findViewById(R.id.itemcycleencours_pourcentcycle);
            imageOiseau=itemView.findViewById(R.id.itemcycleencours_imageoiseau);
        }
    }

}
