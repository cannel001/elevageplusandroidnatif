package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Aliment;
import ci.objis.model.Perte;

public class AllAlimentAdapter extends RecyclerView.Adapter<AllAlimentAdapter.AllAlimentViewHolder> implements Filterable {

    //les proprietes
    private List<Aliment> aliments;
    private List<Aliment> listAlimentFull;

    //constructeur
    public AllAlimentAdapter(List<Aliment> aliments) {
        this.aliments = aliments;
        listAlimentFull = new ArrayList<>(aliments);
    }

    @NonNull
    @Override
    public AllAlimentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AllAlimentViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_allaliment, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AllAlimentViewHolder allAlimentViewHolder, int position) {
        Aliment aliment = aliments.get(position);

        if (aliment.getCycle() != null) {
            allAlimentViewHolder.txtLibelle.setText((aliment.getQuantite() + aliment.getUnite() + " de " + aliment.getLibelle() + " donné(s) aux " + aliment.getCycle().getPoussin().getLibelle() + " du cycle " + aliment.getCycle().getNom()).toUpperCase());
        }

        allAlimentViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(aliment.getLastModifiedDate()));
        allAlimentViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(aliment.getLastModifiedDate()));
        allAlimentViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate()));

    }

    @Override
    public int getItemCount() {
        return aliments.size();
    }

    @Override
    public Filter getFilter() {
        return alimentFilter;
    }

    private Filter alimentFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Aliment> filterableList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filterableList.addAll(listAlimentFull);
            } else {
                String filtrePattern = constraint.toString().toLowerCase();

                for (Aliment aliment : listAlimentFull) {
                    if (aliment.getLibelle().toLowerCase().contains(filtrePattern)) {
                        filterableList.add(aliment);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filterableList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            aliments.clear();
            aliments.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    class AllAlimentViewHolder extends RecyclerView.ViewHolder {

        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public AllAlimentViewHolder(@NonNull View itemView) {
            super(itemView);

            txtLibelle = itemView.findViewById(R.id.itemallaliment_libelle);
            txtJour = itemView.findViewById(R.id.itemallaliment_jour);
            txtMois = itemView.findViewById(R.id.itemallaliment_mois);
            txtAnnee = itemView.findViewById(R.id.itemallaliment_annee);
        }
    }
}
