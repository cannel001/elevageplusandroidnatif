package ci.objis.adapter.recyclerview.all;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import org.joda.time.LocalDate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Perte;
import ci.objis.model.VenteOeuf;

public class AllVenteOeufAdapter extends RecyclerView.Adapter<AllVenteOeufAdapter.AllVenteOeufViewHolder> implements Filterable {

    //les proprietes
    private List<VenteOeuf> venteOeufs;
    private List<VenteOeuf> listVenteOiseauFull;

    //les constructeurs
    public AllVenteOeufAdapter(List<VenteOeuf> venteOeufs) {
        this.venteOeufs = venteOeufs;
        listVenteOiseauFull = new ArrayList<>(venteOeufs);
    }

    @NonNull
    @Override
    public AllVenteOeufViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AllVenteOeufViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_allventeoeuf, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AllVenteOeufViewHolder allVenteOeufViewHolder, int position) {
        VenteOeuf venteOeuf = venteOeufs.get(position);
        allVenteOeufViewHolder.txtLibelle.setText((venteOeuf.getQuantite() + " oeufs du cycle " + venteOeuf.getCycle().getNom() + " vendus").toUpperCase());
        LocalDate localDate = LocalDate.fromDateFields(venteOeuf.getLastModifiedDate());
        allVenteOeufViewHolder.txtJour.setText(String.valueOf(localDate.getDayOfMonth()));
        allVenteOeufViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(venteOeuf.getLastModifiedDate()));
        allVenteOeufViewHolder.txtAnnee.setText(String.valueOf(localDate.getYear()));
    }

    @Override
    public int getItemCount() {
        return venteOeufs.size();
    }

    @Override
    public Filter getFilter() {
        return venteOeufFilter;
    }

    private Filter venteOeufFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<VenteOeuf> filterableList = new ArrayList<>();

            if (constraint == null || constraint.length() == 0) {
                filterableList.addAll(listVenteOiseauFull);
            } else {
                String pattern = constraint.toString().toLowerCase();

                for (VenteOeuf venteOeuf : venteOeufs) {
                    if (venteOeuf.getMoyenPaiement().toLowerCase().contains(pattern)) {
                        filterableList.add(venteOeuf);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = filterableList;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            venteOeufs.clear();
            venteOeufs.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };

    public class AllVenteOeufViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public AllVenteOeufViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemallventeoeuf_libelle);
            txtJour = itemView.findViewById(R.id.itemallventeoeuf_jour);
            txtMois = itemView.findViewById(R.id.itemallventeoeuf_mois);
            txtAnnee = itemView.findViewById(R.id.itemallventeoeuf_annee);
        }
    }
}
