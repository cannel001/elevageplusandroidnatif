package ci.objis.adapter.recyclerview.parcycle;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ci.objis.R;
import ci.objis.model.Aliment;

public class AlimentParCycleAdapter extends RecyclerView.Adapter<AlimentParCycleAdapter.AlimentViewHolder> implements Filterable {
    //les proprietes
    private List<Aliment> listAliments;
    private List<Aliment> alimentFull;

    public AlimentParCycleAdapter(List<Aliment> listAliments) {
        this.listAliments = listAliments;
        alimentFull = new ArrayList<>(listAliments);
    }

    @NonNull
    @Override
    public AlimentParCycleAdapter.AlimentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new AlimentViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cycle_aliment, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull AlimentParCycleAdapter.AlimentViewHolder alimentViewHolder, int i) {
        Aliment aliment = listAliments.get(i);

        if (aliment.getCycle() != null) {
            alimentViewHolder.txtLibelle.setText((aliment.getQuantite() + aliment.getUnite() + " de " + aliment.getLibelle() + " donné(s) aux " + aliment.getCycle().getPoussin().getLibelle()).toUpperCase());
        }
        alimentViewHolder.txtJour.setText(new SimpleDateFormat("dd").format(aliment.getLastModifiedDate()));
        alimentViewHolder.txtMois.setText(new SimpleDateFormat("MMMM").format(aliment.getLastModifiedDate()));
        alimentViewHolder.txtAnnee.setText(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate()));
    }

    @Override
    public int getItemCount() {
        return listAliments.size();
    }

    @Override
    public Filter getFilter() {
        return alimentFilter;
    }

    private Filter alimentFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Aliment> alimentFilterable = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                alimentFilterable.addAll(alimentFull);
            } else {
                String pattern = constraint.toString().toLowerCase();
                for (Aliment aliment : alimentFull) {
                    if (aliment.getLibelle().toLowerCase().contains(pattern)) {
                        alimentFilterable.add(aliment);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = alimentFilterable;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listAliments.clear();
            listAliments.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };


    class AlimentViewHolder extends RecyclerView.ViewHolder {
        //les proprietes
        private TextView txtLibelle;
        private TextView txtJour;
        private TextView txtMois;
        private TextView txtAnnee;

        public AlimentViewHolder(@NonNull View itemView) {
            super(itemView);
            txtLibelle = itemView.findViewById(R.id.itemcyclealiment_libelle);
            txtJour = itemView.findViewById(R.id.itemcyclealiment_jour);
            txtMois = itemView.findViewById(R.id.itemcyclealiment_mois);
            txtAnnee = itemView.findViewById(R.id.itemcyclealiment_annee);
        }
    }
}
