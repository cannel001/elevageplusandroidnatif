package ci.objis.adapter.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ci.objis.ui.venteoiseau.ListeVenteOiseauFragment;
import ci.objis.ui.venteoiseau.StatistiqueVenteOiseauFragment;

public class TabsVenteOiseauAdapter extends FragmentStatePagerAdapter {

    private int numTabs;
    private String typElevage;

    public TabsVenteOiseauAdapter(FragmentManager fm, int numTabs,String typElev) {
        super(fm);
        this.numTabs=numTabs;
        this.typElevage=typElev;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                ListeVenteOiseauFragment listeVenteOiseauFragment =new ListeVenteOiseauFragment();
                return listeVenteOiseauFragment;
            case 1:
                StatistiqueVenteOiseauFragment statistiqueVenteOiseauFragment =new StatistiqueVenteOiseauFragment();
                statistiqueVenteOiseauFragment.setTypElevage(typElevage);
                return statistiqueVenteOiseauFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numTabs;
    }
}
