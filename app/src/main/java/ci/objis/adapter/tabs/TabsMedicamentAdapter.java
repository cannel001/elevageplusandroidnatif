package ci.objis.adapter.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ci.objis.ui.medicament.ListeMedicamentFragment;
import ci.objis.ui.medicament.StatistiqueMedicamentFragment;

public class TabsMedicamentAdapter extends FragmentStatePagerAdapter {

    //les proprietes
    private int numTabl;
    private String typElevage;

    public TabsMedicamentAdapter(FragmentManager fm,int numTabl,String typElevage) {
        super(fm);
        this.numTabl=numTabl;
        this.typElevage=typElevage;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                ListeMedicamentFragment medicamentFragment=new ListeMedicamentFragment();
                return medicamentFragment;
            case 1:
                StatistiqueMedicamentFragment statistiqueMedicamentFragment=new StatistiqueMedicamentFragment();
                statistiqueMedicamentFragment.setTypElevage(typElevage);
                return statistiqueMedicamentFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numTabl;
    }
}
