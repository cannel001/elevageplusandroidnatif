package ci.objis.adapter.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.widget.SearchView;

import ci.objis.ui.aliment.ListeAlimentFragment;
import ci.objis.ui.aliment.StatistiqueAlimentFragment;

public class TabsAlimentAdapter extends FragmentStatePagerAdapter {

    private int numTabl;
    private String typElevage;
    private SearchView searchView;

    public TabsAlimentAdapter(FragmentManager fm, int numTabl, String typeElevage) {
        super(fm);
        this.numTabl=numTabl;
        this.typElevage=typeElevage;
        this.searchView=searchView;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                ListeAlimentFragment listeAlimentFragment=new ListeAlimentFragment();
                return listeAlimentFragment;
            case 1:
                StatistiqueAlimentFragment statistiqueAlimentFragment=new StatistiqueAlimentFragment();
                statistiqueAlimentFragment.newInstance(typElevage);
                return statistiqueAlimentFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numTabl;
    }
}
