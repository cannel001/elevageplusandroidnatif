package ci.objis.adapter.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ci.objis.ui.perte.ListePerteFragment;
import ci.objis.ui.perte.StatistiquePerteFragment;

public class TabsPerteAdapter extends FragmentStatePagerAdapter {

    private int numOfTab;
    private String typElevage;

    public TabsPerteAdapter(FragmentManager fm, int numOfTab,String typElev) {
        super(fm);
        this.numOfTab = numOfTab;
        this.typElevage=typElev;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                ListePerteFragment listePerteFragment = new ListePerteFragment();
                return listePerteFragment;
            case 1:
                StatistiquePerteFragment statistiquePerteFragment = new StatistiquePerteFragment();
                statistiquePerteFragment.setTypElevage(typElevage);
                return statistiquePerteFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numOfTab;
    }
}
