package ci.objis.adapter.tabs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import ci.objis.ui.venteoeuf.ListeVenteOeufFragment;
import ci.objis.ui.venteoeuf.StatistiqueVenteOeufFragment;

public class TabsVenteOeufAdapter extends FragmentStatePagerAdapter {

    //les proprietes
    private int numTabs;
    private String typElevage;

    public TabsVenteOeufAdapter(FragmentManager fm, int numTabs,String typElev) {
        super(fm);
        this.numTabs = numTabs;
        this.typElevage=typElev;
    }

    @Override
    public Fragment getItem(int position) {
        switch(position) {
            case 0:
                ListeVenteOeufFragment listeVenteOeufFragment=new ListeVenteOeufFragment();
                return listeVenteOeufFragment;
            case 1:
                StatistiqueVenteOeufFragment statistiqueVenteOeufFragment =new StatistiqueVenteOeufFragment();
                statistiqueVenteOeufFragment.setTypElevage(typElevage);
                return statistiqueVenteOeufFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return numTabs;
    }
}
