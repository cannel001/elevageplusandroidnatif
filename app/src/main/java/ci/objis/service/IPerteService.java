package ci.objis.service;

import java.util.List;
import java.util.Map;

import ci.objis.model.Cycle;
import ci.objis.model.Perte;

public interface IPerteService {

    public Perte create(Perte perte);

    public Perte createOublie(Perte perte);

    public Perte readOne(Long id);

    public List<Perte> readAll();

    public Boolean delete(Long id);

    public Map<String,Object> getStatForYearByElevage(String typElevage);

    public int getAllNbRegisterByThisMonth(String typElevage);

    public int getAllNbRegisterByThisYear(String typElevage);

    public List<Perte> getAllByCycle(Cycle cycle);

}
