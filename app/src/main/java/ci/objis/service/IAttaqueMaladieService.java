package ci.objis.service;

import java.util.List;

import ci.objis.model.AttaqueMaladie;
import ci.objis.model.Utilisateur;

public interface IAttaqueMaladieService {

    public AttaqueMaladie save(AttaqueMaladie attaqueMaladie);

    public AttaqueMaladie createOublie(AttaqueMaladie attaqueMaladie);

    public List<AttaqueMaladie> getAll();

    public AttaqueMaladie update(AttaqueMaladie attaqueMaladie);

    public Boolean delete(Long id);

    public AttaqueMaladie getOne(Long id);
}
