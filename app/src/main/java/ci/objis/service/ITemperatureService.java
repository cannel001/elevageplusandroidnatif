package ci.objis.service;

import java.util.List;

import ci.objis.model.Temperature;

public interface ITemperatureService {

    public Temperature create(Temperature temperature);

    public Temperature createOublie(Temperature temperature);

    public Temperature readOne(Long id);

    public List<Temperature> readAll();

    public Boolean delete(Long id);

}
