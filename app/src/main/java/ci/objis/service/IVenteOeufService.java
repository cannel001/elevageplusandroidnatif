package ci.objis.service;

import java.util.List;
import java.util.Map;

import ci.objis.model.VenteOeuf;
import ci.objis.model.VenteOiseau;

public interface IVenteOeufService {

    public VenteOeuf create(VenteOeuf venteOeuf);

    public VenteOeuf createOublie(VenteOeuf venteOeuf);

    public VenteOeuf readOne(Long id);

    public List<VenteOeuf> readAll();

    public Boolean delete(Long id);

    public Map<String,Object> getStatForYearByElevage(String typElevage);

    public int getAllNbRegisterByThisMonth(String typElevage);

    public int getAllNbRegisterByThisYear(String typElevage);
}
