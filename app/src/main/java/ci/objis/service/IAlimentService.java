package ci.objis.service;

import java.util.List;
import java.util.Map;

import ci.objis.model.Aliment;
import ci.objis.model.Cycle;

public interface IAlimentService {

    public Aliment create(Aliment aliment);

    public Aliment createOublie(Aliment aliment);

    public Aliment readOne(Long id);

    public List<Aliment> readAll();

    public Boolean delete(Long id);

    public Map<String,Object> getStatForYearByCycl(Cycle cycle);

    public Map<String,Object> getStatForYearByElevage(String typElevage);

    public List<Aliment> readAllByCycle(Cycle cycle);

    public int getAllNbRegisterByThisMonth(String typElevage);

    public int getAllNbRegisterByThisYear(String typElevage);


}
