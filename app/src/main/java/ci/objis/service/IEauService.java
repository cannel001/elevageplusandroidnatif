package ci.objis.service;

import java.util.List;

import ci.objis.model.Eau;

public interface IEauService {

    public Eau create(Eau eau);

    public Eau createOublie(Eau eau);

    public Eau readOne(Long id);

    public List<Eau> readAll();

    public Boolean delete(Long id);
}
