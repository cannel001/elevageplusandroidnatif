package ci.objis.service;

import java.util.List;
import java.util.Map;

import ci.objis.model.Medicament;

public interface IMedicamentService {

    public Medicament create(Medicament medicament);

    public Medicament createOublie(Medicament medicament);

    public Medicament readOne(Long id);

    public List<Medicament> readAll();

    public Boolean delete(Long id);

    public Map<String,Object> getStatForYearByElevage(String typElevage);

    public int getAllNbRegisterByThisMonth(String typElevage);

    public int getAllNbRegisterByThisYear(String typElevage);
}
