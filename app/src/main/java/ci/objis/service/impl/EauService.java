package ci.objis.service.impl;

import java.util.List;

import ci.objis.dao.impl.EauDao;
import ci.objis.model.Eau;
import ci.objis.service.IEauService;

public class EauService implements IEauService {

    //les proprietes
    private EauDao eauDao;

    public EauService() {
        this.eauDao = new EauDao();
    }

    @Override
    public Eau create(Eau eau) {
        if (eau != null) {
            eau.setOublie(false);
            return eauDao.create(eau);
        }
        return null;
    }

    @Override
    public Eau createOublie(Eau eau) {
        if(eau!=null){
            eau.setOublie(true);
            return eauDao.create(eau);
        }
        return null;
    }

    @Override
    public Eau readOne(Long id) {
        if (id > 0) {
            return eauDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<Eau> readAll() {
        return eauDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return eauDao.delete(id);
        }
        return null;
    }
}
