package ci.objis.service.impl;

import android.text.TextUtils;

import java.util.List;

import ci.objis.dao.IUtilisateurDao;
import ci.objis.dao.impl.UtilisateurDao;
import ci.objis.model.Utilisateur;
import ci.objis.service.IUtilisateurService;

public class UtilisateurService implements IUtilisateurService {

    //les proprietes
    private IUtilisateurDao utilisateurDao;
    private Utilisateur utilisateur;

    //constructeur
    public UtilisateurService() {
        this.utilisateurDao = new UtilisateurDao();
    }

    @Override
    public Utilisateur save(Utilisateur utilisateur) {
        if (utilisateur != null) {
            return utilisateurDao.create(utilisateur);
        }
        return null;
    }

    @Override
    public List<Utilisateur> getAll() {
        return utilisateurDao.readAll();
    }

    @Override
    public Utilisateur getOne(Long id) {
        if (id > 0) {
            utilisateurDao.readOne(id);
        }
        return null;
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur) {
        this.utilisateur=getOneByTel(utilisateur.getTel());

        if (this.utilisateur != null) {
            this.utilisateur.setLienPhoto(utilisateur.getLienPhoto());
            this.utilisateur.setLienPhotoCouverture(utilisateur.getLienPhotoCouverture());
            this.utilisateur.setNom(utilisateur.getNom());
            this.utilisateur.setPrenom(utilisateur.getPrenom());
            this.utilisateur.setDescription(utilisateur.getDescription());
            this.utilisateur.setPassword(utilisateur.getPassword());
            this.utilisateur.setPays(utilisateur.getPays());
            this.utilisateur.setVille(utilisateur.getVille());
            this.utilisateur.setCommune(utilisateur.getCommune());
            this.utilisateur.setEmail(utilisateur.getEmail());

            return utilisateurDao.update(this.utilisateur);
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return utilisateurDao.delete(id);
        }
        return false;
    }

    @Override
    public Utilisateur getOneByTel(String tel) {
        if(!TextUtils.isEmpty(tel)){
            return utilisateurDao.readOneByTel(tel);
        }
        return null;
    }

    @Override
    public void deleteAll() {
        List<Utilisateur> utilisateurs=getAll();
        for (Utilisateur utilisateur :
                utilisateurs) {
            delete(utilisateur.getId());
        }
    }
}
