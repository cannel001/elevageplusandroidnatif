package ci.objis.service.impl;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ci.objis.config.VariableAnneeConfig;
import ci.objis.dao.impl.PerteDao;
import ci.objis.model.Cycle;
import ci.objis.model.Perte;
import ci.objis.service.IPerteService;

public class PerteService implements IPerteService {

    //les proprietes
    private PerteDao perteDao;

    private final static String TAG = "PerteService";

    int cptJan = 0;
    int cptFev = 0;
    int cptMar = 0;
    int cptAvr = 0;
    int cptMai = 0;
    int cptJuin = 0;
    int cptJuil = 0;
    int cptAout = 0;
    int cptSept = 0;
    int cptOct = 0;
    int cptNov = 0;
    int cptDec = 0;

    public PerteService() {
        this.perteDao = new PerteDao();
    }

    @Override
    public Perte create(Perte perte) {
        if (perte != null) {
            perte.setOublie(false);
            return perteDao.create(perte);
        }
        return null;
    }

    @Override
    public Perte createOublie(Perte perte) {
        if(perte!=null){
            perte.setOublie(true);
            return perteDao.create(perte);
        }
        return null;
    }

    @Override
    public Perte readOne(Long id) {
        if (id > 0) {
            return perteDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<Perte> readAll() {
        return perteDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return perteDao.delete(id);
        }
        return null;
    }

    /**
     * Methode permettant de retourner les pertes enregistrés durant l'annee en cours par le type d'elevage
     *
     * @param typElevage
     * @return
     */
    @Override
    public Map<String, Object> getStatForYearByElevage(String typElevage) {

        Map<String, Object> maMap = new HashMap<>();

        List<Perte> maList = perteDao.readAll();

        for (Perte perte : maList) {

            if (perte.getCycle() != null) {
                if (perte.getCycle().getPoussin() != null) {
                    if (perte.getCycle().getPoussin().getElevage() != null) {

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 1 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJan += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 2 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptFev += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 3 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMar += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 4 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAvr += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 5 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMai += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 6 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuin += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 7 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuil += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 8 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAout += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 9 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptSept += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 10 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptOct += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 11 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptNov += perte.getQuantite();
                        }

                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())) == 12 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptDec += perte.getQuantite();
                        }

                    }
                }
            }
        }

        //affectation des valeurs à la map
        maMap.put(VariableAnneeConfig.JANVIER, cptJan);
        maMap.put(VariableAnneeConfig.FEVRIER, cptFev);
        maMap.put(VariableAnneeConfig.MARS, cptMar);
        maMap.put(VariableAnneeConfig.AVRIL, cptAvr);
        maMap.put(VariableAnneeConfig.MAI, cptMai);
        maMap.put(VariableAnneeConfig.JUIN, cptJuin);
        maMap.put(VariableAnneeConfig.JUILLET, cptJuil);
        maMap.put(VariableAnneeConfig.AOUT, cptAout);
        maMap.put(VariableAnneeConfig.SEPTEMBRE, cptSept);
        maMap.put(VariableAnneeConfig.OCTOBRE, cptOct);
        maMap.put(VariableAnneeConfig.NOVEMBRE, cptNov);
        maMap.put(VariableAnneeConfig.DECEMBRE, cptDec);

        Log.i(TAG, "la liste des enregistrement par année " + maMap.toString());

        return maMap;
    }

    /**
     * Methode permettant de retourner la quantité totale
     *
     * @param typElevage
     * @return
     */
    @Override
    public int getAllNbRegisterByThisMonth(String typElevage) {

        int monCpt = 0;

        List<Perte> maList = perteDao.readAll();

        for (Perte perte : maList) {
            if (perte.getCycle() != null) {
                if (perte.getCycle().getPoussin() != null) {
                    if (perte.getCycle().getPoussin().getElevage() != null) {
                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("MM").format(new Date()))) &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

    @Override
    public int getAllNbRegisterByThisYear(String typElevage) {

        int monCpt = 0;

        List<Perte> maList = perteDao.readAll();

        for (Perte perte : maList) {
            if (perte.getCycle() != null) {
                if (perte.getCycle().getPoussin() != null) {
                    if (perte.getCycle().getPoussin().getElevage() != null) {
                        if (perte.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("yyyy").format(perte.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

    @Override
    public List<Perte> getAllByCycle(Cycle cycle) {
        List<Perte> pertes = new LinkedList<>();
        if (cycle != null) {
            for (Perte perte : readAll()) {
                if (perte.getCycle().getIdentif().equals(cycle.getIdentif())) {
                    pertes.add(perte);
                }
            }
            if(!pertes.isEmpty()){
                return pertes;
            }
        }
        return null;
    }
}
