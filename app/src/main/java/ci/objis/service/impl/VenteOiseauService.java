package ci.objis.service.impl;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ci.objis.config.VariableAnneeConfig;
import ci.objis.dao.impl.VenteOiseauDao;
import ci.objis.model.Cycle;
import ci.objis.model.VenteOiseau;
import ci.objis.service.IVenteOiseauService;

public class VenteOiseauService implements IVenteOiseauService {

    //les proprietes
    private VenteOiseauDao venteOiseauDao;

    private final static String TAG="VenteOiseauService";

    int cptJan = 0;
    int cptFev = 0;
    int cptMar = 0;
    int cptAvr = 0;
    int cptMai = 0;
    int cptJuin = 0;
    int cptJuil = 0;
    int cptAout = 0;
    int cptSept = 0;
    int cptOct = 0;
    int cptNov = 0;
    int cptDec = 0;

    public VenteOiseauService() {
        this.venteOiseauDao = new VenteOiseauDao();
    }

    @Override
    public VenteOiseau create(VenteOiseau venteOiseau) {
        if (venteOiseau != null) {
            venteOiseau.setOublie(false);
            return venteOiseauDao.create(venteOiseau);
        }
        return null;
    }

    @Override
    public VenteOiseau createOublie(VenteOiseau venteOiseau) {
        if(venteOiseau!=null){
            venteOiseau.setOublie(true);
            return venteOiseauDao.create(venteOiseau);
        }
        return null;
    }

    @Override
    public VenteOiseau readOne(Long id) {
        if (id > 0) {
            return venteOiseauDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<VenteOiseau> readAll() {
        return venteOiseauDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return venteOiseauDao.delete(id);
        }
        return null;
    }

    /**
     * Methode permettant de retourner les ventes oiseaux enregistrés durant l'annee en cours par le type d'elevage
     *
     * @param typElevage
     * @return
     */
    @Override
    public Map<String, Object> getStatForYearByElevage(String typElevage) {

        Map<String, Object> maMap = new HashMap<>();

        List<VenteOiseau> maList = venteOiseauDao.readAll();

        for (VenteOiseau venteOiseau : maList) {

            if (venteOiseau.getCycle() != null) {
                if (venteOiseau.getCycle().getPoussin() != null) {
                    if (venteOiseau.getCycle().getPoussin().getElevage() != null) {

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 1 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJan += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 2 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptFev += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 3 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMar += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 4 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAvr += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 5 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMai += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 6 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuin += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 7 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuil += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 8 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAout += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 9 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptSept += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 10 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptOct += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 11 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptNov += venteOiseau.getNombre();
                        }

                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())) == 12 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptDec += venteOiseau.getNombre();
                        }

                    }
                }
            }


        }

        //affectation des valeurs à la map
        maMap.put(VariableAnneeConfig.JANVIER, cptJan);
        maMap.put(VariableAnneeConfig.FEVRIER, cptFev);
        maMap.put(VariableAnneeConfig.MARS, cptMar);
        maMap.put(VariableAnneeConfig.AVRIL, cptAvr);
        maMap.put(VariableAnneeConfig.MAI, cptMai);
        maMap.put(VariableAnneeConfig.JUIN, cptJuin);
        maMap.put(VariableAnneeConfig.JUILLET, cptJuil);
        maMap.put(VariableAnneeConfig.AOUT, cptAout);
        maMap.put(VariableAnneeConfig.SEPTEMBRE, cptSept);
        maMap.put(VariableAnneeConfig.OCTOBRE, cptOct);
        maMap.put(VariableAnneeConfig.NOVEMBRE, cptNov);
        maMap.put(VariableAnneeConfig.DECEMBRE, cptDec);

        Log.i(TAG,"la liste des enregistrement par année "+maMap.toString());

        return maMap;
    }

    /**
     * Methode permettant de retourner la quantité totale
     * @param typElevage
     * @return
     */
    @Override
    public int getAllNbRegisterByThisMonth(String typElevage) {

        int monCpt = 0;

        List<VenteOiseau> maList = venteOiseauDao.readAll();

        for (VenteOiseau venteOiseau : maList) {
            if (venteOiseau.getCycle() != null) {
                if (venteOiseau.getCycle().getPoussin() != null) {
                    if (venteOiseau.getCycle().getPoussin().getElevage() != null) {
                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("MM").format(new Date()))) &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

    @Override
    public int getAllNbRegisterByThisYear(String typElevage) {

        int monCpt = 0;

        List<VenteOiseau> maList = venteOiseauDao.readAll();

        for (VenteOiseau venteOiseau : maList) {
            if (venteOiseau.getCycle() != null) {
                if (venteOiseau.getCycle().getPoussin() != null) {
                    if (venteOiseau.getCycle().getPoussin().getElevage() != null) {
                        if (venteOiseau.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOiseau.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

    @Override
    public List<VenteOiseau> getAllByCycle(Cycle cycle) {
        List<VenteOiseau> venteOiseaus = new LinkedList<>();
        if (cycle != null) {
            for (VenteOiseau venteOiseau : readAll()) {
                if (venteOiseau.getCycle().getIdentif().equals(cycle.getIdentif())) {
                    venteOiseaus.add(venteOiseau);
                }
            }
            if(!venteOiseaus.isEmpty()){
                return venteOiseaus;
            }
        }
        return null;
    }
}
