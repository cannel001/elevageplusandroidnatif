package ci.objis.service.impl;

import java.util.List;

import ci.objis.dao.impl.TemperatureDao;
import ci.objis.model.Temperature;
import ci.objis.service.ITemperatureService;

public class TemperatureService implements ITemperatureService {

    //les proprietes
    private TemperatureDao temperatureDao;

    public TemperatureService() {
        this.temperatureDao = new TemperatureDao();
    }

    @Override
    public Temperature create(Temperature temperature) {
        if (temperature != null) {
            temperature.setOublie(false);
            return temperatureDao.create(temperature);
        }
        return null;
    }

    @Override
    public Temperature createOublie(Temperature temperature) {
        if(temperature!=null){
            temperature.setOublie(true);
            return temperatureDao.create(temperature);
        }
        return null;
    }

    @Override
    public Temperature readOne(Long id) {
        if (id > 0) {
            return temperatureDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<Temperature> readAll() {
        return temperatureDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return temperatureDao.delete(id);
        }
        return null;
    }
}
