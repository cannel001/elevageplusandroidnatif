package ci.objis.service.impl;

import java.util.List;

import ci.objis.dao.impl.ObservationDao;
import ci.objis.model.Observation;
import ci.objis.service.IObservationService;

public class ObservationService implements IObservationService {

    //les proprietes
    private ObservationDao observationDao;

    public ObservationService() {
        this.observationDao = new ObservationDao();
    }

    @Override
    public Observation create(Observation observation) {
        if (observation != null) {
            observation.setOublie(false);
            return observationDao.create(observation);
        }
        return null;
    }

    @Override
    public Observation createOublie(Observation observation) {
        if(observation!=null){
            observation.setOublie(true);
            return observationDao.create(observation);
        }
        return null;
    }

    @Override
    public Observation readOne(Long id) {
        if (id > 0) {
            return observationDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<Observation> readAll() {
        return observationDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return observationDao.delete(id);
        }
        return null;
    }
}
