package ci.objis.service.impl;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ci.objis.config.VariableAnneeConfig;
import ci.objis.dao.impl.AlimentDao;
import ci.objis.model.Aliment;
import ci.objis.model.Cycle;
import ci.objis.service.IAlimentService;

public class AlimentService implements IAlimentService {

    //les proprietes
    private AlimentDao alimentDao;
    private final static String TAG = "AlimentService";

    int cptJan = 0;
    int cptFev = 0;
    int cptMar = 0;
    int cptAvr = 0;
    int cptMai = 0;
    int cptJuin = 0;
    int cptJuil = 0;
    int cptAout = 0;
    int cptSept = 0;
    int cptOct = 0;
    int cptNov = 0;
    int cptDec = 0;

    public AlimentService() {
        this.alimentDao = new AlimentDao();
    }

    @Override
    public Aliment create(Aliment aliment) {
        if (aliment != null) {
            aliment.setOublie(false);
            return alimentDao.create(aliment);
        }
        return null;
    }

    @Override
    public Aliment createOublie(Aliment aliment) {
        if(aliment!=null){
            aliment.setOublie(true);
            return alimentDao.create(aliment);
        }
        return null;
    }

    @Override
    public Aliment readOne(Long id) {
        if (id > 0) {
            return alimentDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<Aliment> readAll() {
        return alimentDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return alimentDao.delete(id);
        }
        return null;
    }

    /**
     * Methode permettant de retourner les aliments enregistres durant l'annee en cours par le cycle
     *
     * @param cycle
     * @return
     */
    @Override
    public Map<String, Object> getStatForYearByCycl(Cycle cycle) {

        Map<String, Object> maMap = new HashMap<>();

        List<Aliment> maList = alimentDao.readAllByCycle(cycle);

        for (Aliment aliment : maList) {

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 1 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptJan += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 2 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptFev += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 3 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptMar += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 4 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptAvr += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 5 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptMai += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 6 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptJuin += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 7 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptJuil += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 8 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptAout += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 9 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptSept += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 10 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptOct += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 11 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptNov += aliment.getQuantite();
            }

            if (Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 12 &&
                    Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                cptDec += aliment.getQuantite();
            }

        }

        //affectation des valeurs à la map
        maMap.put(VariableAnneeConfig.JANVIER, cptJan);
        maMap.put(VariableAnneeConfig.FEVRIER, cptFev);
        maMap.put(VariableAnneeConfig.MARS, cptMar);
        maMap.put(VariableAnneeConfig.AVRIL, cptAvr);
        maMap.put(VariableAnneeConfig.MAI, cptMai);
        maMap.put(VariableAnneeConfig.JUIN, cptJuin);
        maMap.put(VariableAnneeConfig.JUILLET, cptJuil);
        maMap.put(VariableAnneeConfig.AOUT, cptAout);
        maMap.put(VariableAnneeConfig.SEPTEMBRE, cptSept);
        maMap.put(VariableAnneeConfig.OCTOBRE, cptOct);
        maMap.put(VariableAnneeConfig.NOVEMBRE, cptNov);
        maMap.put(VariableAnneeConfig.DECEMBRE, cptDec);

        return maMap;
    }

    /**
     * Methode permettant de retourner les aliments enregistres durant l'annee en cours par le type d'elevage
     *
     * @param typElevage
     * @return
     */
    @Override
    public Map<String, Object> getStatForYearByElevage(String typElevage) {

        Map<String, Object> maMap = new HashMap<>();

        List<Aliment> maList = alimentDao.readAll();

        for (Aliment aliment : maList) {

            if (aliment.getCycle() != null) {
                if (aliment.getCycle().getPoussin() != null) {
                    if (aliment.getCycle().getPoussin().getElevage() != null) {

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 1 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJan += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 2 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptFev += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 3 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMar += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 4 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAvr += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 5 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMai += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 6 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuin += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 7 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuil += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 8 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAout += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 9 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptSept += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 10 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptOct += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 11 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptNov += aliment.getQuantite();
                        }

                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())) == 12 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptDec += aliment.getQuantite();
                        }

                    }
                }
            }


        }

        //affectation des valeurs à la map
        maMap.put(VariableAnneeConfig.JANVIER, cptJan);
        maMap.put(VariableAnneeConfig.FEVRIER, cptFev);
        maMap.put(VariableAnneeConfig.MARS, cptMar);
        maMap.put(VariableAnneeConfig.AVRIL, cptAvr);
        maMap.put(VariableAnneeConfig.MAI, cptMai);
        maMap.put(VariableAnneeConfig.JUIN, cptJuin);
        maMap.put(VariableAnneeConfig.JUILLET, cptJuil);
        maMap.put(VariableAnneeConfig.AOUT, cptAout);
        maMap.put(VariableAnneeConfig.SEPTEMBRE, cptSept);
        maMap.put(VariableAnneeConfig.OCTOBRE, cptOct);
        maMap.put(VariableAnneeConfig.NOVEMBRE, cptNov);
        maMap.put(VariableAnneeConfig.DECEMBRE, cptDec);

        Log.i(TAG, "la liste des enregistrement par année " + maMap.toString());

        return maMap;
    }

    @Override
    public List<Aliment> readAllByCycle(Cycle cycle) {
        if (cycle != null) {
            return alimentDao.readAllByCycle(cycle);
        }
        return null;
    }

    /**
     * Methode permettant de retourner la quantité totale
     * @param typElevage
     * @return
     */
    @Override
    public int getAllNbRegisterByThisMonth(String typElevage) {

        int monCpt = 0;

        List<Aliment> maList = alimentDao.readAll();

        for (Aliment aliment : maList) {
            if (aliment.getCycle() != null) {
                if (aliment.getCycle().getPoussin() != null) {
                    if (aliment.getCycle().getPoussin().getElevage() != null) {
                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("MM").format(new Date()))) &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

    @Override
    public int getAllNbRegisterByThisYear(String typElevage) {

        int monCpt = 0;

        List<Aliment> maList = alimentDao.readAll();

        for (Aliment aliment : maList) {
            if (aliment.getCycle() != null) {
                if (aliment.getCycle().getPoussin() != null) {
                    if (aliment.getCycle().getPoussin().getElevage() != null) {
                        if (aliment.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("yyyy").format(aliment.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }
}
