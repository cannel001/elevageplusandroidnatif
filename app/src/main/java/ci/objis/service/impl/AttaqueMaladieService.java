package ci.objis.service.impl;


import java.util.List;

import ci.objis.dao.IAttaqueMaladieDao;
import ci.objis.dao.impl.AttaqueMaladieDao;
import ci.objis.model.AttaqueMaladie;
import ci.objis.service.IAttaqueMaladieService;

public class AttaqueMaladieService implements IAttaqueMaladieService {

    //les proprietes
    private IAttaqueMaladieDao attaqueMaladieDao;

    //constructeur
    public AttaqueMaladieService(){
        this.attaqueMaladieDao=new AttaqueMaladieDao();
    }


    @Override
    public AttaqueMaladie save(AttaqueMaladie attaqueMaladie) {
        if (attaqueMaladie != null) {
            attaqueMaladie.setOublie(false);
            return attaqueMaladieDao.create(attaqueMaladie);
        }
        return null;
    }

    @Override
    public AttaqueMaladie createOublie(AttaqueMaladie attaqueMaladie) {
        if(attaqueMaladie!=null){
            attaqueMaladie.setOublie(true);
            return attaqueMaladieDao.create(attaqueMaladie);
        }
        return null;
    }

    @Override
    public List<AttaqueMaladie> getAll() {
        return attaqueMaladieDao.readAll();
    }

    @Override
    public AttaqueMaladie getOne(Long id) {
        if (id > 0) {
            attaqueMaladieDao.readOne(id);
        }
        return null;
    }

    @Override
    public AttaqueMaladie update(AttaqueMaladie attaqueMaladie) {
        if (attaqueMaladie != null) {
            return attaqueMaladieDao.update(attaqueMaladie);
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return attaqueMaladieDao.delete(id);
        }
        return false;
    }


}
