package ci.objis.service.impl;

import android.util.Log;

import org.joda.time.Days;
import org.joda.time.LocalDate;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ci.objis.dao.impl.CycleDao;
import ci.objis.model.Cycle;
import ci.objis.model.Perte;
import ci.objis.model.VenteOiseau;
import ci.objis.service.ICycleService;
import ci.objis.service.IPerteService;
import ci.objis.service.IVenteOiseauService;

public class CycleService implements ICycleService {

    //les proprietes
    private CycleDao cycleDao;
    private final static String TAG = "CycleService";
    private Cycle cycle;
    private IPerteService perteService;
    private IVenteOiseauService venteOiseauService;

    //constructeur
    public CycleService() {
        this.cycleDao = new CycleDao();
        perteService = new PerteService();
        venteOiseauService = new VenteOiseauService();
    }

    @Override
    public Cycle create(Cycle cycle) {

        cycle.setIdentif((new Date()).getTime());
        switch (cycle.getPoussin().getLibelle()) {
            case "POULET CHAIR":
                cycle.setDureeCycle(55);
                break;
            case "PONTE":
                cycle.setDureeCycle(21);
                break;
        }
        cycle.setDatecycle(new Date());
        return cycleDao.create(cycle);
    }

    @Override
    public Cycle readOne(Long id) {
        if (id > 0) {
            return cycleDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<Cycle> readAll() {
        return cycleDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            cycleDao.delete(id);
        }
        return null;
    }

    @Override
    public List<Map<String, Object>> allCyclAndPercentbyLibelleElevage(String libelleElevage) {

        //les proprietes
        int percent = 0;
        int nbJour = 0;
        LocalDate dateDebut;
        LocalDate dateFin;
        Map<String, Object> maMap;
        List<Map<String, Object>> maListemap = new LinkedList<>();

        //recuperation de la liste des cycles
        List<Cycle> cycleList = cycleDao.readAllByLibelleElevage(libelleElevage);

        if (cycleList != null) {
            for (Cycle cycle : cycleList) {

                //initialiser les variables
                percent = 0;
                nbJour = 0;
                dateDebut = null;
                dateFin = null;
                maMap = new HashMap<>();

                //calcul de la periode
                dateDebut = LocalDate.fromDateFields(cycle.getDatecycle());
                dateFin = LocalDate.fromDateFields(new Date());
                nbJour = Days.daysBetween(dateDebut, dateFin).getDays();

                //calcul du pourcentage
                percent = (100 * nbJour) / cycle.getDureeCycle();

                maMap.put("percent", percent);
                maMap.put("cycle", cycle);

                Log.i(TAG, "passage dans allCyclAndPercentbyLibelleElevage avec percent " + percent);
                Log.i(TAG, "passage dans allCyclAndPercentbyLibelleElevage avec cycle " + cycle.toString());

                maListemap.add(maMap);

            }
        }

        return maListemap;
    }

    @Override
    public int getNombreActivityByLibelleElevage(String libelleElevage) {

        List<Map<String, Object>> listMaMap = allCyclAndPercentbyLibelleElevage(libelleElevage);

        int cpt = 0;

        for (Map<String, Object> map : listMaMap) {
            cycle = (Cycle) map.get("cycle");
            cpt += cycle.getDureeCycle();
        }

        return cpt;
    }

    @Override
    public Cycle readOneByIdentif(Long identif) {
        if (identif > 0) {
            return cycleDao.readOneByIdentif(identif);
        }
        return null;
    }

    /**
     * Methode permettant de recuperer le jour encours
     *
     * @return
     */
    @Override
    public int getJourEnCours(Cycle cycle) {
        LocalDate dateDebutCycle = LocalDate.fromDateFields(cycle.getDatecycle());
        LocalDate dateEnCours = LocalDate.fromDateFields(new Date());
        return Days.daysBetween(dateDebutCycle, dateEnCours).getDays();
    }

    /**
     * Methode permettant de recuperer le nombre de jours restants
     *
     * @param numJrEnCours
     * @param cycle
     * @return
     */
    @Override
    public int getNbJrRestant(int numJrEnCours, Cycle cycle) {
        return cycle.getDureeCycle() - numJrEnCours;
    }

    /**
     * Methode permettant de retourner le pourcentage actuel
     *
     * @param numJrEnCours
     * @param cycle
     * @return
     */
    @Override
    public Float getPourcentagActuel(int numJrEnCours, Cycle cycle) {
        return (float) ((numJrEnCours * 100) / cycle.getDureeCycle());
    }

    @Override
    public int getTtNbJrCyclRestantParAnnee(String typElevage) {

        //recuperer la liste des cycles par elevage
        List<Cycle> maList = cycleDao.readAllByLibelleElevage(typElevage);

        int cptRecup = 0;
        int cpt = 0;

        if (!maList.isEmpty()) {
            for (Cycle cycle : maList) {
                cptRecup = getNbJrRestant(getJourEnCours(cycle), cycle);
                if (cptRecup >= 0) {
                    cpt += cptRecup;
                }
            }
        }

        return cpt;
    }

    @Override
    public int getNbBeterestant(Cycle cycle) {

        if (cycle != null) {

            int nbPerte = 0;
            int nbVenteOiseau = 0;
            int nbBete = 0;

            List<Perte> listePerte = perteService.getAllByCycle(cycle);
            List<VenteOiseau> listeVenteOiseau = venteOiseauService.getAllByCycle(cycle);

            Log.i(TAG,"cycle "+cycle.toString());

            //compter le nombre de perte
            if (listePerte!=null) {
                Log.i(TAG,"liste pertes "+listePerte.toString());
                for (Perte perte : listePerte) {
                    nbPerte += perte.getQuantite();
                }
            }

            //compter le nombre de vente
            if (listeVenteOiseau!=null) {
                Log.i(TAG,"liste vente oiseau "+listeVenteOiseau.toString());
                for (VenteOiseau venteOiseau : listeVenteOiseau) {
                    nbVenteOiseau += venteOiseau.getNombre();
                }
            }

            //calcul du nombre restant
            nbBete = cycle.getNbOisseau() - (nbPerte + nbVenteOiseau);

            return nbBete;
        }
        return 0;
    }
}
