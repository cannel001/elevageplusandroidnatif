package ci.objis.service.impl;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ci.objis.config.VariableAnneeConfig;
import ci.objis.dao.impl.VenteOeufDao;
import ci.objis.model.VenteOeuf;
import ci.objis.service.IVenteOeufService;

public class VenteOeufService implements IVenteOeufService {

    //les proprietes
    private VenteOeufDao venteOeufDao;

    private final static String TAG="VenteOeufService";

    int cptJan = 0;
    int cptFev = 0;
    int cptMar = 0;
    int cptAvr = 0;
    int cptMai = 0;
    int cptJuin = 0;
    int cptJuil = 0;
    int cptAout = 0;
    int cptSept = 0;
    int cptOct = 0;
    int cptNov = 0;
    int cptDec = 0;

    public VenteOeufService() {
        this.venteOeufDao = new VenteOeufDao();
    }

    @Override
    public VenteOeuf create(VenteOeuf venteOeuf) {
        if (venteOeuf != null) {
            venteOeuf.setOublie(false);
            return venteOeufDao.create(venteOeuf);
        }
        return null;
    }

    @Override
    public VenteOeuf createOublie(VenteOeuf venteOeuf) {
        if(venteOeuf!=null){
            venteOeuf.setOublie(true);
            return venteOeufDao.create(venteOeuf);
        }
        return null;
    }

    @Override
    public VenteOeuf readOne(Long id) {
        if (id > 0) {
            return venteOeufDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<VenteOeuf> readAll() {
        return venteOeufDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return venteOeufDao.delete(id);
        }
        return null;
    }


    /**
     * Methode permettant de retourner les ventes oeufs enregistrés durant l'annee en cours par le type d'elevage
     *
     * @param typElevage
     * @return
     */
    @Override
    public Map<String, Object> getStatForYearByElevage(String typElevage) {

        Map<String, Object> maMap = new HashMap<>();

        List<VenteOeuf> maList = venteOeufDao.readAll();

        for (VenteOeuf venteOeuf : maList) {

            if (venteOeuf.getCycle() != null) {
                if (venteOeuf.getCycle().getPoussin() != null) {
                    if (venteOeuf.getCycle().getPoussin().getElevage() != null) {

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 1 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJan += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 2 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptFev += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 3 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMar += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 4 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAvr += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 5 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMai += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 6 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuin += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 7 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuil += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 8 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAout += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 9 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptSept += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 10 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptOct += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 11 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptNov += venteOeuf.getQuantite();
                        }

                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())) == 12 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptDec += venteOeuf.getQuantite();
                        }

                    }
                }
            }


        }

        //affectation des valeurs à la map
        maMap.put(VariableAnneeConfig.JANVIER, cptJan);
        maMap.put(VariableAnneeConfig.FEVRIER, cptFev);
        maMap.put(VariableAnneeConfig.MARS, cptMar);
        maMap.put(VariableAnneeConfig.AVRIL, cptAvr);
        maMap.put(VariableAnneeConfig.MAI, cptMai);
        maMap.put(VariableAnneeConfig.JUIN, cptJuin);
        maMap.put(VariableAnneeConfig.JUILLET, cptJuil);
        maMap.put(VariableAnneeConfig.AOUT, cptAout);
        maMap.put(VariableAnneeConfig.SEPTEMBRE, cptSept);
        maMap.put(VariableAnneeConfig.OCTOBRE, cptOct);
        maMap.put(VariableAnneeConfig.NOVEMBRE, cptNov);
        maMap.put(VariableAnneeConfig.DECEMBRE, cptDec);

        Log.i(TAG,"la liste des enregistrement par année "+maMap.toString());

        return maMap;
    }

    /**
     * Methode permettant de retourner la quantité totale
     * @param typElevage
     * @return
     */
    @Override
    public int getAllNbRegisterByThisMonth(String typElevage) {

        int monCpt = 0;

        List<VenteOeuf> maList = venteOeufDao.readAll();

        for (VenteOeuf venteOeuf : maList) {
            if (venteOeuf.getCycle() != null) {
                if (venteOeuf.getCycle().getPoussin() != null) {
                    if (venteOeuf.getCycle().getPoussin().getElevage() != null) {
                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("MM").format(new Date()))) &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

    @Override
    public int getAllNbRegisterByThisYear(String typElevage) {

        int monCpt = 0;

        List<VenteOeuf> maList = venteOeufDao.readAll();

        for (VenteOeuf venteOeuf : maList) {
            if (venteOeuf.getCycle() != null) {
                if (venteOeuf.getCycle().getPoussin() != null) {
                    if (venteOeuf.getCycle().getPoussin().getElevage() != null) {
                        if (venteOeuf.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("yyyy").format(venteOeuf.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

}
