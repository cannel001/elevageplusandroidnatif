package ci.objis.service.impl;

import android.app.Activity;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;

import ci.objis.R;

public class MenuActionService {

    public static SearchView mySearchView;

    public static Boolean standart(Menu menu, Activity activity){
        MenuInflater inflater = activity.getMenuInflater();
        inflater.inflate(R.menu.menu_action, menu);
        //cacher l'item message du menu action
        MenuItem itemMessage=menu.findItem(R.id.item_message);
        itemMessage.setVisible(false);
        //cacher l'item message du menu action
        MenuItem itemSearch=menu.findItem(R.id.item_recherche);
        itemSearch.setVisible(false);
        return true;
    }

    public static Boolean message(Menu menu, Activity activity){
        MenuInflater inflater = activity.getMenuInflater();
        inflater.inflate(R.menu.menu_action, menu);
        //cacher l'item message du menu action
        MenuItem itemMessage=menu.findItem(R.id.item_message);
        itemMessage.setVisible(true);
        //cacher l'item message du menu action
        MenuItem itemSearch=menu.findItem(R.id.item_recherche);
        itemSearch.setVisible(false);

        return true;
    }

    public static Boolean recherche(Menu menu, Activity activity){
        MenuInflater inflater = activity.getMenuInflater();
        inflater.inflate(R.menu.menu_action, menu);
        //cacher l'item message du menu action
        final MenuItem itemMessage=menu.findItem(R.id.item_message);
        itemMessage.setVisible(false);
        //cacher l'item message du menu action
        final MenuItem itemSearch=menu.findItem(R.id.item_recherche);
        itemSearch.setVisible(true);
        //Associer seach View
        final SearchView searchView = (SearchView) itemSearch.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);

        mySearchView=searchView;


        return true;
    }
}
