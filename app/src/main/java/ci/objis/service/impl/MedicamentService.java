package ci.objis.service.impl;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ci.objis.config.VariableAnneeConfig;
import ci.objis.dao.impl.MedicamentDao;
import ci.objis.model.Medicament;
import ci.objis.service.IMedicamentService;

public class MedicamentService  implements IMedicamentService {

    //les proprietes
    private MedicamentDao medicamentDao;

    private final static String TAG="MedicamentService";

    int cptJan = 0;
    int cptFev = 0;
    int cptMar = 0;
    int cptAvr = 0;
    int cptMai = 0;
    int cptJuin = 0;
    int cptJuil = 0;
    int cptAout = 0;
    int cptSept = 0;
    int cptOct = 0;
    int cptNov = 0;
    int cptDec = 0;

    public MedicamentService() {
        this.medicamentDao = new MedicamentDao();
    }

    @Override
    public Medicament create(Medicament medicament) {
        if (medicament != null) {
            medicament.setOublie(false);
            return medicamentDao.create(medicament);
        }
        return null;
    }

    @Override
    public Medicament createOublie(Medicament medicament) {
        if(medicament!=null){
            medicament.setOublie(true);
            return medicamentDao.create(medicament);
        }
        return null;
    }

    @Override
    public Medicament readOne(Long id) {
        if (id > 0) {
            return medicamentDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<Medicament> readAll() {
        return medicamentDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return medicamentDao.delete(id);
        }
        return null;
    }

    /**
     * Methode permettant de retourner les medicaments enregistrés durant l'annee en cours par le type d'elevage
     *
     * @param typElevage
     * @return
     */
    @Override
    public Map<String, Object> getStatForYearByElevage(String typElevage) {

        Map<String, Object> maMap = new HashMap<>();

        List<Medicament> maList = medicamentDao.readAll();

        for (Medicament medicament : maList) {

            if (medicament.getCycle() != null) {
                if (medicament.getCycle().getPoussin() != null) {
                    if (medicament.getCycle().getPoussin().getElevage() != null) {

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 1 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJan += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 2 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptFev += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 3 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMar += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 4 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAvr += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 5 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptMai += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 6 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuin += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 7 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptJuil += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 8 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptAout += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 9 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptSept += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 10 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptOct += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 11 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptNov += medicament.getQuantite();
                        }

                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())) == 12 &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            cptDec += medicament.getQuantite();
                        }

                    }
                }
            }


        }

        //affectation des valeurs à la map
        maMap.put(VariableAnneeConfig.JANVIER, cptJan);
        maMap.put(VariableAnneeConfig.FEVRIER, cptFev);
        maMap.put(VariableAnneeConfig.MARS, cptMar);
        maMap.put(VariableAnneeConfig.AVRIL, cptAvr);
        maMap.put(VariableAnneeConfig.MAI, cptMai);
        maMap.put(VariableAnneeConfig.JUIN, cptJuin);
        maMap.put(VariableAnneeConfig.JUILLET, cptJuil);
        maMap.put(VariableAnneeConfig.AOUT, cptAout);
        maMap.put(VariableAnneeConfig.SEPTEMBRE, cptSept);
        maMap.put(VariableAnneeConfig.OCTOBRE, cptOct);
        maMap.put(VariableAnneeConfig.NOVEMBRE, cptNov);
        maMap.put(VariableAnneeConfig.DECEMBRE, cptDec);

        Log.i(TAG,"la liste des enregistrement par année "+maMap.toString());

        return maMap;
    }

    /**
     * Methode permettant de retourner la quantité totale
     * @param typElevage
     * @return
     */
    @Override
    public int getAllNbRegisterByThisMonth(String typElevage) {

        int monCpt = 0;

        List<Medicament> maList = medicamentDao.readAll();

        for (Medicament medicament : maList) {
            if (medicament.getCycle() != null) {
                if (medicament.getCycle().getPoussin() != null) {
                    if (medicament.getCycle().getPoussin().getElevage() != null) {
                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("MM").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("MM").format(new Date()))) &&
                                Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }

    @Override
    public int getAllNbRegisterByThisYear(String typElevage) {

        int monCpt = 0;

        List<Medicament> maList = medicamentDao.readAll();

        for (Medicament medicament : maList) {
            if (medicament.getCycle() != null) {
                if (medicament.getCycle().getPoussin() != null) {
                    if (medicament.getCycle().getPoussin().getElevage() != null) {
                        if (medicament.getCycle().getPoussin().getElevage().getLibelle().equals(typElevage) && Integer.valueOf(new SimpleDateFormat("yyyy").format(medicament.getLastModifiedDate())).equals(Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date())))) {
                            monCpt ++;
                        }
                    }
                }
            }
        }

        return monCpt;
    }
}
