package ci.objis.service.impl;

import java.util.List;

import ci.objis.dao.impl.RecolteOeufDao;
import ci.objis.model.RecolteOeuf;
import ci.objis.service.IRecolteOeufService;

public class RecolteOeufService implements IRecolteOeufService {

    //les proprietes
    private RecolteOeufDao recolteOeufDao;

    public RecolteOeufService() {
        this.recolteOeufDao = new RecolteOeufDao();
    }

    @Override
    public RecolteOeuf create(RecolteOeuf recolteOeuf) {
        if (recolteOeuf != null) {
            recolteOeuf.setOublie(false);
            return recolteOeufDao.create(recolteOeuf);
        }
        return null;
    }

    @Override
    public RecolteOeuf createOublie(RecolteOeuf recolteOeuf) {
        if(recolteOeuf!=null){
            recolteOeuf.setOublie(true);
            return recolteOeufDao.create(recolteOeuf);
        }
        return null;
    }

    @Override
    public RecolteOeuf readOne(Long id) {
        if (id > 0) {
            return recolteOeufDao.readOne(id);
        }
        return null;
    }

    @Override
    public List<RecolteOeuf> readAll() {
        return recolteOeufDao.readAll();
    }

    @Override
    public Boolean delete(Long id) {
        if (id > 0) {
            return recolteOeufDao.delete(id);
        }
        return null;
    }

}
