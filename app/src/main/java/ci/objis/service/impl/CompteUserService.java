package ci.objis.service.impl;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.bestsoft32.tt_fancy_gif_dialog_lib.TTFancyGifDialog;
import com.bestsoft32.tt_fancy_gif_dialog_lib.TTFancyGifDialogListener;

import ci.objis.R;
import ci.objis.service.ICompteUserService;
import ci.objis.service.IUtilisateurService;
import es.dmoral.toasty.Toasty;

public class CompteUserService implements ICompteUserService {

    private Context context;
    private Activity activity;
    private IUtilisateurService utilisateurService;

    //contructeur
    public CompteUserService(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        utilisateurService = new UtilisateurService();
    }

    @Override
    public void logoutUser() {
        new TTFancyGifDialog.Builder(activity)
                .setTitle("Fermer")
                .setMessage("Voulez-vous quitter l'application Elevage plus")
                .setPositiveBtnText("Quitter")
                .setPositiveBtnBackground("#22b573")
                .setNegativeBtnText("Annuler")
                .setNegativeBtnBackground("#c1272d")
                .setGifResource(R.drawable.cloudbyebye)      //pass your gif, png or jpg
                .isCancellable(true)
                .OnPositiveClicked(new TTFancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        utilisateurService.deleteAll();
                        activity.finish();
                    }
                })
                .OnNegativeClicked(new TTFancyGifDialogListener() {
                    @Override
                    public void OnClick() {
                        Toasty.warning(context, "Fermeture annulée", Toast.LENGTH_SHORT).show();
                    }
                })
                .build();
    }
}
