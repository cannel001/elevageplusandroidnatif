package ci.objis.service;

import java.util.List;

import ci.objis.model.Utilisateur;

public interface IUtilisateurService {

    public Utilisateur save(Utilisateur utilisateur);

    public List<Utilisateur> getAll();

    public Utilisateur getOne(Long id);

    public Utilisateur update(Utilisateur utilisateur);

    public Boolean delete(Long id);

    public Utilisateur getOneByTel(String tel);

    public void deleteAll();

}
