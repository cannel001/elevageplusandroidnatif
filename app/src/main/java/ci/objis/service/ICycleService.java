package ci.objis.service;

import java.util.List;
import java.util.Map;

import ci.objis.model.Cycle;

public interface ICycleService {

    public Cycle create(Cycle cycle);

    public Cycle readOne(Long id);

    public List<Cycle> readAll();

    public Boolean delete(Long id);

    public List<Map<String,Object>> allCyclAndPercentbyLibelleElevage(String libelleElevage);

    public int getNombreActivityByLibelleElevage(String libelleElevage);

    public Cycle readOneByIdentif(Long identif);

    public int getJourEnCours(Cycle cycle);

    public int getNbJrRestant(int numJrEnCours, Cycle cycle);

    public Float getPourcentagActuel(int numJrEnCours, Cycle cycle);

    public int getTtNbJrCyclRestantParAnnee(String typElevage);

    public int getNbBeterestant(Cycle cycle);

}
