package ci.objis.service;

import java.util.List;

import ci.objis.model.RecolteOeuf;

public interface IRecolteOeufService {

    public RecolteOeuf create(RecolteOeuf recolteOeuf);

    public RecolteOeuf createOublie(RecolteOeuf recolteOeuf);

    public RecolteOeuf readOne(Long id);

    public List<RecolteOeuf> readAll();

    public Boolean delete(Long id);

}
