package ci.objis.service;

import java.util.List;
import java.util.Map;

import ci.objis.model.Cycle;
import ci.objis.model.VenteOiseau;

public interface IVenteOiseauService {

    public VenteOiseau create(VenteOiseau venteOiseau);

    public VenteOiseau createOublie(VenteOiseau venteOiseau);

    public VenteOiseau readOne(Long id);

    public List<VenteOiseau> readAll();

    public Boolean delete(Long id);

    public Map<String,Object> getStatForYearByElevage(String typElevage);

    public int getAllNbRegisterByThisMonth(String typElevage);

    public int getAllNbRegisterByThisYear(String typElevage);

    public List<VenteOiseau> getAllByCycle(Cycle cycle);
}
