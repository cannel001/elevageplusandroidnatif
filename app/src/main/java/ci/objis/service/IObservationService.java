package ci.objis.service;

import java.util.List;

import ci.objis.model.Observation;

public interface IObservationService {

    public Observation create(Observation observation);

    public Observation createOublie(Observation observation);

    public Observation readOne(Long id);

    public List<Observation> readAll();

    public Boolean delete(Long id);
}
