package ci.objis.utility;

import ci.objis.repository.PublicationRepository;
import ci.objis.shared.RetrofitClient;

public class PublicationUtility {

    //les proprietes
    public  static String API_PUBLICATION_URL="https://elplus.herokuapp.com/api/mespublications/publications/";

    private PublicationUtility(){}

    public static PublicationRepository getPublicationService(){
        return RetrofitClient
                .getClient(API_PUBLICATION_URL)
                .create(PublicationRepository.class);
    }


}
