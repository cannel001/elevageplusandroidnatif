package ci.objis.utility;

import ci.objis.repository.UtilisateurRepository;
import ci.objis.shared.RetrofitClient;

public class UtilisateurUtility {

    //les proprietes
    public  static String API_UTILISATEUR_URL="https://elplus.herokuapp.com/api/compte/utlisateurs/";

    private UtilisateurUtility(){}

    public static UtilisateurRepository getUtilisateurService(){
        return RetrofitClient
                .getClient(API_UTILISATEUR_URL)
                .create(UtilisateurRepository.class);
    }

}
