package ci.objis.utility;

import ci.objis.repository.UtilisateurRepository;
import ci.objis.repository.VeterinaireRepository;
import ci.objis.shared.RetrofitClient;

public class VeterinaireUtility {

    //les proprietes
    public  static String API_VETERINAIRE_URL="https://elplus.herokuapp.com/api/veterinaires/";

    private VeterinaireUtility(){}

    public static VeterinaireRepository getVeterinaireService(){
        return RetrofitClient
                .getClient(API_VETERINAIRE_URL)
                .create(VeterinaireRepository.class);
    }

}
