package ci.objis.utility;

import ci.objis.repository.ClientRepository;
import ci.objis.shared.RetrofitClient;

public class ClientUtility {

    private static final String API_URL_CLIENT="https://elplus.herokuapp.com/api/mesclients/clients/";

    //constructeur
    private ClientUtility(){}

    //methode permettant de retourner un nouveau Client repository
    public static ClientRepository getClientService(){
        return RetrofitClient
                .getClient(API_URL_CLIENT)
                .create(ClientRepository.class);
    }
}
