package ci.objis.api;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;

import ci.objis.ui.MainActivity;
import ci.objis.ui.SelectElevageActivity;
import ci.objis.model.Utilisateur;
import ci.objis.repository.UtilisateurRepository;
import ci.objis.service.IUtilisateurService;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.utility.UtilisateurUtility;
import ci.objis.ui.compteuser.ConfirmCreatCptActivity;
import ci.objis.ui.compteuser.LoginActivity;
import es.dmoral.toasty.Toasty;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UtilisateurRestDao {

    private static final String TAG = "UtilisateurRestDao";
    private UtilisateurRepository utilisateurRepository;
    private Utilisateur utilisateur;
    private Context context;
    private Intent intent;
    private Boolean rep;
    private NSidedProgressBar nSidedProgressBar;
    private IUtilisateurService utilisateurService;
    private LinearLayout lnFormulaire;
    private LinearLayout lnProgressBar;
    private Activity activity;

    //constructeur par defaut
    public UtilisateurRestDao(Context context) {
        utilisateurRepository = UtilisateurUtility.getUtilisateurService();
        this.context = context;
        this.utilisateurService = new UtilisateurService();
        Log.i(TAG, "passage dans le constructeur ");
    }

    public UtilisateurRestDao(Context context, NSidedProgressBar nSidedProgressBar) {
        utilisateurRepository = UtilisateurUtility.getUtilisateurService();
        this.context = context;
        this.nSidedProgressBar = nSidedProgressBar;
        this.utilisateurService = new UtilisateurService();
        Log.i(TAG, "passage dans le constructeur ");
    }

    public UtilisateurRestDao(Context context, LinearLayout lnFormulaire, LinearLayout lnProgressBar, Activity activity) {
        utilisateurRepository = UtilisateurUtility.getUtilisateurService();
        this.context = context;
        this.lnFormulaire = lnFormulaire;
        this.lnProgressBar = lnProgressBar;
        this.utilisateurService = new UtilisateurService();
        this.activity = activity;
        Log.i(TAG, "passage dans le constructeur ");
    }


    public void registerUser(String tel) {

        Call<Utilisateur> call = utilisateurRepository.registerUser(tel);

        nSidedProgressBar.setVisibility(View.VISIBLE);

        call.enqueue(new Callback<Utilisateur>() {
            @Override
            public void onResponse(Call<Utilisateur> call, Response<Utilisateur> response) {
                Log.i(TAG, "passage dans onresponse");
                if (response.isSuccessful()) {
                    utilisateur = response.body();
                    Log.i(TAG, "Inscription effectué avec succes avec " + utilisateur.toString());
                    Log.i(TAG, "Inscription effectué " + response.toString());

                    //sauvegarder des parametres de l'utilisateur dans les fichiers preferences
                    utilisateurService.save(utilisateur);

                    //demarer l'activité de confirmation compte
                    intent = new Intent(context, ConfirmCreatCptActivity.class);
                    context.startActivity(intent);
                } else {
                    Log.i(TAG, "passage dans onresponse failure avec " + response.code() + " " + response.toString());
                    nSidedProgressBar.setVisibility(View.INVISIBLE);
                    Toasty.warning(context, "Erreur survenue pendant la connexion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Utilisateur> call, Throwable t) {
                Log.i(TAG, "Inscription echoué ");
                nSidedProgressBar.setVisibility(View.INVISIBLE);
                Toasty.warning(context, "Erreur survenue pendant la connexion", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void activeCompteUser(final String tel, String codeValide) {

        Log.i(TAG, "passage dans actice compte");
        nSidedProgressBar.setVisibility(View.VISIBLE);
        Call<Utilisateur> call = utilisateurRepository.activeCompteUser(tel, codeValide);
        call.enqueue(new Callback<Utilisateur>() {
            @Override
            public void onResponse(Call<Utilisateur> call, Response<Utilisateur> response) {

                Log.i(TAG, "passage dans reponse active compte " + response.toString());

                if (response.isSuccessful()) {

                    Log.i(TAG, "activation effectué avec succes");

                    //affectation de la valeur de l'utilisateur
                    utilisateur = utilisateurService.getAll().get(0);
                    utilisateur.setIsConfirmed(true);
                    utilisateur.setPassword(codeValide);
                    //mise a jour des infos
                    utilisateurService.save(utilisateur);
                    //demarer l'activité de l'acceuil
                    nSidedProgressBar.setVisibility(View.GONE);
                    intent = new Intent(context, SelectElevageActivity.class);
                    context.startActivity(intent);

                } else {
                    nSidedProgressBar.setVisibility(View.GONE);
                    Toast.makeText(context, "Erreur survenue pendant l'activation", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Utilisateur> call, Throwable t) {
                nSidedProgressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(context, "Erreur survenue pendant l'activation", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void loginUser(String tel, String password) {

        Call<Utilisateur> call = utilisateurRepository.loginUser(tel, password);
        nSidedProgressBar.setVisibility(View.VISIBLE);

        call.enqueue(new Callback<Utilisateur>() {
            @Override
            public void onResponse(Call<Utilisateur> call, Response<Utilisateur> response) {

                Log.i(TAG, response.toString());

                if (response.isSuccessful()) {

                    utilisateur = response.body();
                    utilisateur.setPassword(password);

                    Log.i(TAG, utilisateur.toString());

                    utilisateurService.save(utilisateur);

                    if (utilisateur.getIsConfirmed()) {
                        intent = new Intent(context, SelectElevageActivity.class);
                        context.startActivity(intent);
                    } else {
                        intent = new Intent(context, ConfirmCreatCptActivity.class);
                        context.startActivity(intent);
                    }

                } else {
                    nSidedProgressBar.setVisibility(View.INVISIBLE);
                    Toasty.warning(context, "Erreur survenue pendant la connexion", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Utilisateur> call, Throwable t) {
                nSidedProgressBar.setVisibility(View.INVISIBLE);
                Toasty.warning(context, "Erreur survenue pendant la connexion", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void recupPassword(String tel) {
        Call<Object> call = utilisateurRepository.recupPassword(tel);
        nSidedProgressBar.setVisibility(View.VISIBLE);
        rep = false;

        Log.i(TAG, "passage dans recup password ");

        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                Log.i(TAG, "passage dans on response");
                if (response.isSuccessful()) {

                    Log.i(TAG, "passage dans on response successuful");

                    nSidedProgressBar.setVisibility(View.INVISIBLE);

                    // Create Alert using Builder
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                            .setTitle("Felicitations !")
                            .setMessage("La recuperation de votre mot de passe s'est effectué avec succès")
                            .addButton("CONTINUER", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                                //Toast.makeText(context, "Upgrade tapped", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(context, LoginActivity.class);
                                context.startActivity(intent);
                                //
                                dialog.dismiss();
                            });

                    // Show the alert
                    CFAlertDialog alertDialog = builder.show();
                    alertDialog.setOnDismissListener(dialog -> {
                        intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    });

                    rep = true;
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                Log.i(TAG, "passage dans le on failure ");
                nSidedProgressBar.setVisibility(View.INVISIBLE);
                Toasty.warning(context, "Erreur survenue pendant la recuperation", Toasty.LENGTH_LONG).show();
                rep = false;
            }
        });
    }


    public void updateCompte(Utilisateur utilisateur) {

        Log.i(TAG, "passage update 1 avec utilisateur " + utilisateur.toString());

        utilisateur.setDateCreation(null);
        utilisateur.setDateUpdate(null);

        Call<Utilisateur> call = utilisateurRepository.updateCompte(utilisateur.getTel(), utilisateur.getPassword(), utilisateur);

        Log.i(TAG, "passage update 2");

        lnFormulaire.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);

        call.enqueue(new Callback<Utilisateur>() {
            @Override
            public void onResponse(Call<Utilisateur> call, Response<Utilisateur> response) {
                Log.i(TAG, "passage update 3 avec code response" + response.toString());
                if (response.isSuccessful()) {
                    Log.i(TAG, "passage update 4");
                    // Create Alert using Builder
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                            .setTitle("Felicitations !")
                            .setMessage("La mise à jour de votre compte s'est effectuée avec succès")
                            .addButton("CONTINUER", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                                //
                                dialog.dismiss();
                            });

                    // Show the alert
                    CFAlertDialog alertDialog = builder.show();
                    alertDialog.setOnDismissListener(dialog -> {
                        utilisateurService.update(response.body());
                        Intent intent = new Intent(context, MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        context.startActivity(intent);
                        activity.finish();
                    });
                } else {
                    lnFormulaire.setVisibility(View.VISIBLE);
                    lnProgressBar.setVisibility(View.GONE);
                    Log.i(TAG, "passage update 5");
                    // Create Alert using Builder
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                            .setTitle("Oups !")
                            .setMessage("Erreur inattendue survenue pendant la mise à jour de vos informations")
                            .addButton("CONTINUER", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                                //
                                dialog.dismiss();
                            });

                    // Show the alert
                    builder.show();
                }
            }

            @Override
            public void onFailure(Call<Utilisateur> call, Throwable t) {
                // Create Alert using Builder
                lnFormulaire.setVisibility(View.VISIBLE);
                lnProgressBar.setVisibility(View.GONE);

                Log.i(TAG, "passage update 6");
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setTitle("Oups !")
                        .setMessage("Erreur inattendue survenue pendant la mise à jour de vos informations")
                        .addButton("CONTINUER", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });

                // Show the alert
                builder.show();
            }
        });
    }


    public void updatePassword(String ancienPassword, String newPassword, String tel) {

        Log.i(TAG, "passage update 1 avec " + ancienPassword + " " + newPassword + " " + tel);
        utilisateur = utilisateurService.getAll().get(0);

        Call<Utilisateur> call = utilisateurRepository.updatePassword(utilisateur.getTel(), utilisateur.getPassword(), ancienPassword, newPassword, tel);

        lnFormulaire.setVisibility(View.GONE);
        lnProgressBar.setVisibility(View.VISIBLE);

        call.enqueue(new Callback<Utilisateur>() {
            @Override
            public void onResponse(Call<Utilisateur> call, Response<Utilisateur> response) {
                Log.i(TAG, "passage update 2");
                if (response.isSuccessful()) {
                    Log.i(TAG, "passage update 3");
                    // Create Alert using Builder
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                            .setTitle("Felicitations !")
                            .setMessage("La mise à jour de votre mot de passe s'est effectuée avec succès")
                            .addButton("CONTINUER", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                                //
                                dialog.dismiss();
                            });

                    // Show the alert
                    CFAlertDialog alertDialog = builder.show();
                    alertDialog.setOnDismissListener(dialog -> {
                        utilisateur = response.body();
                        utilisateur.setPassword(newPassword);
                        utilisateurService.update(utilisateur);
                        activity.finish();
                    });
                } else {
                    lnFormulaire.setVisibility(View.VISIBLE);
                    lnProgressBar.setVisibility(View.GONE);
                    Log.i(TAG, "passage update 5");
                    // Create Alert using Builder
                    CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                            .setTitle("Oups !")
                            .setMessage("Erreur inattendue survenue pendant la mise à jour de votre mot de passe")
                            .addButton("CONTINUER", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                                //
                                dialog.dismiss();
                            });

                    // Show the alert
                    builder.show();
                }
            }

            @Override
            public void onFailure(Call<Utilisateur> call, Throwable t) {
                // Create Alert using Builder
                lnFormulaire.setVisibility(View.VISIBLE);
                lnProgressBar.setVisibility(View.GONE);

                Log.i(TAG, "passage update 6");
                CFAlertDialog.Builder builder = new CFAlertDialog.Builder(context)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setTitle("Oups !")
                        .setMessage("Erreur inattendue survenue pendant la mise à jour de votre mot de passe")
                        .addButton("CONTINUER", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });

                // Show the alert
                builder.show();
            }
        });
    }

}
