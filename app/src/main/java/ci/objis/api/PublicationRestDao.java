package ci.objis.api;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;

import java.util.LinkedList;
import java.util.List;

import ci.objis.adapter.recyclerview.all.AllConseilAdapter;
import ci.objis.model.Publication;
import ci.objis.model.Utilisateur;
import ci.objis.repository.PublicationRepository;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.utility.PublicationUtility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PublicationRestDao {

    //les proprietes
    private final static String TAG = "PublicationRestDao";
    private List<Publication> listpublication = null;
    private Publication publication;
    private PublicationRepository publicationRepository;
    private Boolean bool;
    private Utilisateur utilisateur;

    //contructeur
    public PublicationRestDao() {
        this.publicationRepository = PublicationUtility.getPublicationService();
        utilisateur=new UtilisateurService().getAll().get(0);
        Log.i(TAG, "Passage dans publication dao");
    }


    public List<Publication> getAll(
            final NSidedProgressBar progressBar,
            final RecyclerView recyclerView,
            final Context context,
            final LinearLayout linearLayout,
            final TextView textView,
            final ImageView imgReload,
            final LinearLayout linearConseilVide) {

        //rendre recycler view invisible
        imgReload.setVisibility(View.INVISIBLE);
        recyclerView.setVisibility(View.GONE);
        linearConseilVide.setVisibility(View.GONE);

        //rendre les champs visibles
        progressBar.setVisibility(View.VISIBLE);
        textView.setText("Chargement en cours");
        textView.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);

        Call<List<Publication>> call = publicationRepository.getAll(utilisateur.getTel(),utilisateur.getPassword());

        Log.i(TAG, "Passage dans getAll dao");

        call.enqueue(new Callback<List<Publication>>() {
            @Override
            public void onResponse(Call<List<Publication>> call, Response<List<Publication>> response) {

                if (response.isSuccessful()) {
                    listpublication = new LinkedList<>();
                    listpublication = response.body();

                    //rendre les champs invisible
                    if (listpublication.isEmpty()) {
                        linearConseilVide.setVisibility(View.VISIBLE);
                        linearLayout.setVisibility(View.GONE);
                    } else {
                        progressBar.setVisibility(View.INVISIBLE);
                        textView.setVisibility(View.INVISIBLE);
                        linearLayout.setVisibility(View.INVISIBLE);

                        //rendre les champs visibles
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.setAdapter(new AllConseilAdapter(listpublication, context));
                        recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    }

                } else {
                    Log.i(TAG, response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Publication>> call, Throwable t) {
                Log.i(TAG, "Failure, retry please");
                textView.setText("Failure, retry please");
                progressBar.setVisibility(View.INVISIBLE);
                imgReload.setVisibility(View.VISIBLE);
                textView.setVisibility(View.VISIBLE);
            }
        });

        Log.i(TAG, "Passage dans fin getAll dao");

        return listpublication;

    }

}
