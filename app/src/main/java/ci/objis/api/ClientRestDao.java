package ci.objis.api;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;

import java.util.List;

import ci.objis.adapter.recyclerview.all.AllClientAdapter;
import ci.objis.model.Client;
import ci.objis.model.Utilisateur;
import ci.objis.repository.ClientRepository;
import ci.objis.service.IUtilisateurService;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.utility.ClientUtility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ClientRestDao {

    //les proprietes
    private static final String TAG = "ClientRestDao";
    private List<Client> listClients = null;
    private Client client = null;
    private ClientRepository clientRepository;
    private Utilisateur utilisateur;

    //constructeur par defaut
    public ClientRestDao() {
        this.clientRepository = ClientUtility.getClientService();
        this.utilisateur=new UtilisateurService().getAll().get(0);
    }


    public List<Client> getAll(
            final NSidedProgressBar progressBar,
            final RecyclerView recyclerView,
            final Context context,
            final LinearLayout linearLayout,
            final TextView textView,
            final ImageView imgReload,
            final LinearLayout linearVide
    ) {

        //rendre recycler view invisible
        imgReload.setVisibility(View.INVISIBLE);
        linearVide.setVisibility(View.GONE);
        recyclerView.setVisibility(View.GONE);

        //rendre les champs visibles
        progressBar.setVisibility(View.VISIBLE);
        textView.setText("Chargement en cours");
        textView.setVisibility(View.VISIBLE);
        linearLayout.setVisibility(View.VISIBLE);

        Call<List<Client>> call = clientRepository.getAll(utilisateur.getTel(),utilisateur.getPassword());

        call.enqueue(new Callback<List<Client>>() {
            @Override
            public void onResponse(Call<List<Client>> call, Response<List<Client>> response) {
                if (response.isSuccessful()) {
                    listClients = response.body();

                    //rendre les champs invisible
                    progressBar.setVisibility(View.INVISIBLE);
                    textView.setVisibility(View.INVISIBLE);
                    linearLayout.setVisibility(View.INVISIBLE);

                    if(listClients.isEmpty()){
                        linearLayout.setVisibility(View.GONE);
                        linearVide.setVisibility(View.VISIBLE);
                    }else{
                        linearLayout.setVisibility(View.VISIBLE);
                        linearVide.setVisibility(View.GONE);
                        //rendre les champs visibles
                        recyclerView.setVisibility(View.VISIBLE);
                        recyclerView.setAdapter(new AllClientAdapter(context,listClients));
                        recyclerView.setLayoutManager(new LinearLayoutManager(context));
                    }
                }else{
                    Log.i(TAG,response.message());
                }
            }

            @Override
            public void onFailure(Call<List<Client>> call, Throwable t) {
                Log.i(TAG, "Failure, retry please");
                textView.setText("Failure, retry please");
                progressBar.setVisibility(View.INVISIBLE);
                imgReload.setVisibility(View.VISIBLE);
                textView.setVisibility(View.VISIBLE);
            }
        });

        return listClients;
    }


    public Client getOne(long id) {

        Call<Client> call = clientRepository.getOne(utilisateur.getTel(),utilisateur.getPassword(),id);

        call.enqueue(new Callback<Client>() {
            @Override
            public void onResponse(Call<Client> call, Response<Client> response) {
                if (response.isSuccessful()) {
                    client = response.body();
                    Log.i(TAG, client.toString());
                }
            }

            @Override
            public void onFailure(Call<Client> call, Throwable t) {
                Log.i(TAG, "Failure, retry please");
            }
        });

        return client;
    }
}
