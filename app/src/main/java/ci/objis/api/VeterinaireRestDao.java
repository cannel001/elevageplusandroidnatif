package ci.objis.api;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import ci.objis.adapter.recyclerview.all.AllVeterinaireAdapter;
import ci.objis.model.Utilisateur;
import ci.objis.model.Veterinaire;
import ci.objis.repository.VeterinaireRepository;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.utility.VeterinaireUtility;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VeterinaireRestDao {

    private VeterinaireRepository veterinaireRepository;
    private List<Veterinaire> veterinaireList;
    private RecyclerView recyclerView;
    private Context context;
    private LinearLayout lnLayoutProgres;

    private ProgressBar progressBar;
    private TextView txtMessage;
    private ImageView imageViewReload;
    private LinearLayout lnmessage;

    private LinearLayout lnLayoutListeVide;
    private Utilisateur utilisateur;

    private final static String TAG = "VeterinaireRestDao";

    public VeterinaireRestDao(RecyclerView recyclerView, Context context, LinearLayout lnLayoutProgres, ImageView imageViewReload, TextView txtMessage, ProgressBar progressBar, LinearLayout lnLayoutListeVide, LinearLayout lnmessage) {
        this.veterinaireRepository = VeterinaireUtility.getVeterinaireService();
        this.recyclerView = recyclerView;
        this.context = context;
        this.lnLayoutProgres = lnLayoutProgres;
        this.lnmessage = lnmessage;
        this.progressBar = progressBar;
        this.txtMessage = txtMessage;
        this.imageViewReload = imageViewReload;
        this.lnLayoutListeVide = lnLayoutListeVide;
        this.utilisateur = new UtilisateurService().getAll().get(0);
    }

    //methode permettant de retourner la liste de tous les veterinaires
    public void getAll() {
        Log.i(TAG, "Passage dans getAll avec ");
        Call<List<Veterinaire>> call = veterinaireRepository.getAll();
        call.enqueue(new Callback<List<Veterinaire>>() {
            @Override
            public void onResponse(Call<List<Veterinaire>> call, Response<List<Veterinaire>> response) {
                if (response.isSuccessful()) {
                    veterinaireList = response.body();
                    Log.i(TAG, "Passage dans on response avec code" + response.code() + " et " + veterinaireList.toString());
                } else {
                    Log.i(TAG, "Passage dans on response else avec " + response.code());
                }
            }

            @Override
            public void onFailure(Call<List<Veterinaire>> call, Throwable t) {
                Log.i(TAG, "Passage dans on failure");
            }
        });
    }

    public void getAllBySpecialiste(String specialite) {

        Log.i(TAG, "passage dans getALlBySpecialiste");

        //masquer le recylerView
        recyclerView.setVisibility(View.GONE);
        lnLayoutListeVide.setVisibility(View.GONE);

        //initailiser la liste
        veterinaireList = null;

        //afficher les objets d'attente
        lnmessage.setVisibility(View.VISIBLE);
        lnLayoutProgres.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        imageViewReload.setVisibility(View.GONE);
        txtMessage.setText("Chargement en cours");

        Call<List<Veterinaire>> call = veterinaireRepository.getAllBySpecialite(utilisateur.getTel(), utilisateur.getPassword(), specialite);

        call.enqueue(new Callback<List<Veterinaire>>() {
            @Override
            public void onResponse(Call<List<Veterinaire>> call, Response<List<Veterinaire>> response) {
                if (response.isSuccessful()) {
                    Log.i(TAG, "passage dans on response avec le code " + response.code() + " et " + response.body());

                    veterinaireList = response.body();
                    lnLayoutProgres.setVisibility(View.GONE);

                    if (!veterinaireList.isEmpty()) {
                        lnmessage.setVisibility(View.GONE);
                        recyclerView.setAdapter(new AllVeterinaireAdapter(context, veterinaireList));
                        recyclerView.setLayoutManager(new LinearLayoutManager(context));
                        recyclerView.setVisibility(View.VISIBLE);
                        Log.i(TAG, "passage dans on response 2 avec le code " + response.code() + " et " + response.body());
                    } else {
                        lnLayoutListeVide.setVisibility(View.VISIBLE);
                    }
                } else {
                    lnLayoutProgres.setVisibility(View.GONE);
                    lnLayoutListeVide.setVisibility(View.VISIBLE);
                    Log.i(TAG, "passage dans on response avec le code " + response.code() + " et " + response.toString());
                }
            }

            @Override
            public void onFailure(Call<List<Veterinaire>> call, Throwable t) {
                Log.i(TAG, "Passage dans on failure");

                progressBar.setVisibility(View.GONE);
                txtMessage.setText("Failure, retry please!");
                imageViewReload.setVisibility(View.VISIBLE);
            }
        });

    }

}
