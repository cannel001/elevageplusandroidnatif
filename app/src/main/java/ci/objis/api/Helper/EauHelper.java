package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Eau;

public class EauHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "eau";

    //COLLECTION REFERENCE
    private static CollectionReference getEauCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createEau(Eau eau) {
        return EauHelper
                .getEauCollection()
                .add(eau);
    }

    //READ ONE
    private Task<DocumentSnapshot> getAliment(String uidEau) {
        return EauHelper
                .getEauCollection()
                .document(uidEau)
                .get();
    }

    //UPDATE
    private Task<Void> updateAliment(Eau eau) {
        return EauHelper
                .getEauCollection()
                .document(eau.getUid())
                .set(eau);
    }

    //DELETE
    private Task<Void> deleteAliment(String uidEau) {
        return EauHelper
                .getEauCollection()
                .document(uidEau)
                .delete();
    }

}
