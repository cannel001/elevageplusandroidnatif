package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Observation;

public class ObservationHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "observation";

    //COLLECTION REFERENCE
    private static CollectionReference getObservationCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createAttaqueMaladie(Observation observation) {
        return ObservationHelper
                .getObservationCollection()
                .add(observation);
    }

    //READ ONE
    private Task<DocumentSnapshot> getObservation(String uidObservation) {
        return ObservationHelper
                .getObservationCollection()
                .document(uidObservation)
                .get();
    }

    //UPDATE
    private Task<Void> updateObservation(Observation observation) {
        return ObservationHelper
                .getObservationCollection()
                .document(observation.getUid())
                .set(observation);
    }

    //DELETE
    private Task<Void> deleteObservation(String uidObservation) {
        return ObservationHelper
                .getObservationCollection()
                .document(uidObservation)
                .delete();
    }

}
