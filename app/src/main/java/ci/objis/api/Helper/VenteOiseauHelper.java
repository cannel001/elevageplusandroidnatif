package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.VenteOiseau;

public class VenteOiseauHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "venteoiseau";

    //COLLECTION REFERENCE
    private static CollectionReference getVenteOiseauCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createVenteOiseau(VenteOiseau venteOiseau) {
        return VenteOiseauHelper
                .getVenteOiseauCollection()
                .add(venteOiseau);
    }

    //READ ONE
    private Task<DocumentSnapshot> getVenteOiseau(String uidVenteOiseau) {
        return VenteOiseauHelper
                .getVenteOiseauCollection()
                .document(uidVenteOiseau)
                .get();
    }

    //UPDATE
    private Task<Void> updateVenteOiseau(VenteOiseau venteOiseau) {
        return VenteOiseauHelper
                .getVenteOiseauCollection()
                .document(venteOiseau.getUid())
                .set(venteOiseau);
    }

    //DELETE
    private Task<Void> deleteVenteOiseau(String uidVenteOiseau) {
        return VenteOiseauHelper
                .getVenteOiseauCollection()
                .document(uidVenteOiseau)
                .delete();
    }

}
