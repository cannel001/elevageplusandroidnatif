package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.AttaqueMaladie;

public class AttaqueMaladieHelper {

    //les proprietes
    private static final String COLLECTION_NAME="attaquemaladie";

    //COLLECTION REFERENCE
    private static CollectionReference getAttaqueMaladieCollection(){
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createAttaqueMaladie(AttaqueMaladie attaqueMaladie){
        return AttaqueMaladieHelper
                .getAttaqueMaladieCollection()
                .add(attaqueMaladie);
    }

    //READ ONE
    private Task<DocumentSnapshot> getAliment(String uidAttaqueMaladie){
        return AttaqueMaladieHelper
                .getAttaqueMaladieCollection()
                .document(uidAttaqueMaladie)
                .get();
    }

    //UPDATE
    private Task<Void> updateAliment(AttaqueMaladie attaqueMaladie){
        return AttaqueMaladieHelper
                .getAttaqueMaladieCollection()
                .document(attaqueMaladie.getUid())
                .set(attaqueMaladie);
    }

    //DELETE
    private Task<Void> deleteAliment(String uidAttaqueMaladie){
        return AttaqueMaladieHelper
                .getAttaqueMaladieCollection()
                .document(uidAttaqueMaladie)
                .delete();
    }

}
