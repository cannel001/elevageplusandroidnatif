package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.VenteOeuf;

public class VenteOeufHelper {

    //les proprietes
    private static final String COLLECTION_NAME="venteoeuf";

    //COLLECTION REFERENCE
    private static CollectionReference getVenteOeufCollection(){
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createVenteOeuf(VenteOeuf venteOeuf){
        return VenteOeufHelper
                .getVenteOeufCollection()
                .add(venteOeuf);
    }

    //READ ONE
    private Task<DocumentSnapshot> getVenteOeuf(String uidVenteOeuf){
        return VenteOeufHelper
                .getVenteOeufCollection()
                .document(uidVenteOeuf)
                .get();
    }

    //UPDATE
    private Task<Void> updateVenteOeuf(VenteOeuf venteOeuf){
        return VenteOeufHelper
                .getVenteOeufCollection()
                .document(venteOeuf.getUid())
                .set(venteOeuf);
    }

    //DELETE
    private Task<Void> deleteVenteOeuf(String uidVenteOeuf){
        return VenteOeufHelper
                .getVenteOeufCollection()
                .document(uidVenteOeuf)
                .delete();
    }

}
