package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Perte;

public class PerteHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "perte";

    //COLLECTION REFERENCE
    private static CollectionReference getPerteCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createPerte(Perte perte) {
        return PerteHelper
                .getPerteCollection()
                .add(perte);
    }

    //READ ONE
    private Task<DocumentSnapshot> getPerte(String uidPerte) {
        return PerteHelper
                .getPerteCollection()
                .document(uidPerte)
                .get();
    }

    //UPDATE
    private Task<Void> updatePerte(Perte perte) {
        return PerteHelper
                .getPerteCollection()
                .document(perte.getUid())
                .set(perte);
    }

    //DELETE
    private Task<Void> deletePerte(String uidPerte) {
        return PerteHelper
                .getPerteCollection()
                .document(uidPerte)
                .delete();
    }

}
