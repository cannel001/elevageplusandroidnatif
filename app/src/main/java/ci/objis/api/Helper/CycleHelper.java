package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Cycle;

public class CycleHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "cycle";

    //COLLECTION REFERENCE
    private static CollectionReference getCycleCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle").getParent();
    }

    //CREATE
    private Task<DocumentReference> createUser(Cycle cycle) {
        return CycleHelper.getCycleCollection()
                .add(cycle);
    }

    //READ ONE
    private Task<DocumentSnapshot> getCycle(String uidCycle) {
        return CycleHelper.getCycleCollection().document(uidCycle).get();
    }

    //UPDATE
    private Task<Void> updateCycle(Cycle cycle) {
        return CycleHelper.getCycleCollection()
                .document(cycle.getUid())
                .set(cycle);
    }

    //DELETE
    private Task<Void> deleteCycle(String uidCycle) {
        return CycleHelper.getCycleCollection()
                .document(uidCycle)
                .delete();
    }

}
