package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.RecolteOeuf;

public class RecolteOeufHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "recolteoeuf";

    //COLLECTION REFERENCE
    private static CollectionReference getRecolteOeufCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createRecolteOeuf(RecolteOeuf recolteOeuf) {
        return RecolteOeufHelper
                .getRecolteOeufCollection()
                .add(recolteOeuf);
    }

    //READ ONE
    private Task<DocumentSnapshot> getRecolteOeuf(String uidRecolteOeuf) {
        return RecolteOeufHelper
                .getRecolteOeufCollection()
                .document(uidRecolteOeuf)
                .get();
    }

    //UPDATE
    private Task<Void> updateRecolteOeuf(RecolteOeuf recolteOeuf) {
        return RecolteOeufHelper
                .getRecolteOeufCollection()
                .document(recolteOeuf.getUid())
                .set(recolteOeuf);
    }

    //DELETE
    private Task<Void> deleteRecolteOeuf(String uidRecolteOeuf) {
        return RecolteOeufHelper
                .getRecolteOeufCollection()
                .document(uidRecolteOeuf)
                .delete();
    }

}
