package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Temperature;

public class TemperatureHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "temperature";

    //COLLECTION REFERENCE
    private static CollectionReference getTemperatureCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createTemperature(Temperature temperature) {
        return TemperatureHelper
                .getTemperatureCollection()
                .add(temperature);
    }

    //READ ONE
    private Task<DocumentSnapshot> getAliment(String uidTemperature) {
        return TemperatureHelper
                .getTemperatureCollection()
                .document(uidTemperature)
                .get();
    }

    //UPDATE
    private Task<Void> updateAliment(Temperature temperature) {
        return TemperatureHelper
                .getTemperatureCollection()
                .document(temperature.getUid())
                .set(temperature);
    }

    //DELETE
    private Task<Void> deleteAliment(String uidTemperature) {
        return TemperatureHelper
                .getTemperatureCollection()
                .document(uidTemperature)
                .delete();
    }
}
