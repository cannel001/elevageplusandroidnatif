package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Medicament;

public class MedicamentHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "medicament";

    //COLLECTION REFERENCE
    private static CollectionReference getMedicamentCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createMedicament(Medicament medicament) {
        return MedicamentHelper
                .getMedicamentCollection()
                .add(medicament);
    }

    //READ ONE
    private Task<DocumentSnapshot> getMedicament(String uidMedicament) {
        return MedicamentHelper
                .getMedicamentCollection()
                .document(uidMedicament)
                .get();
    }

    //UPDATE
    private Task<Void> updateMedicament(Medicament medicament) {
        return MedicamentHelper
                .getMedicamentCollection()
                .document(medicament.getUid())
                .set(medicament);
    }

    //DELETE
    private Task<Void> deleteMedicament(String uidMedicament) {
        return MedicamentHelper
                .getMedicamentCollection()
                .document(uidMedicament)
                .delete();
    }

}
