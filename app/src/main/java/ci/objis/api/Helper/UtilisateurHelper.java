package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Utilisateur;

public class UtilisateurHelper {

    private static final String COLLECTION_NAME = "users";

    // --- COLLECTION REFERENCE ---

    public static CollectionReference getUsersCollection(){
        return FirebaseFirestore.getInstance().collection(COLLECTION_NAME);
    }

    // CREATE
    public static Task<DocumentReference> createUser(Utilisateur utilisateur){
        return UtilisateurHelper.getUsersCollection().add(utilisateur);
    }

    //READ ONE
    public static Task<DocumentSnapshot> getUtilisateur(String tel){
        return UtilisateurHelper.getUsersCollection().document(tel).get();
    }

    //UPDATE
    public static Task<Void> updateUtilisateur(Utilisateur utilisateur){
        return UtilisateurHelper.getUsersCollection().document(utilisateur.getTel()).set(utilisateur);
    }

    //DELETE
    public static Task<Void> deleteUtilisateur(String tel){
        return UtilisateurHelper.getUsersCollection().document(tel).delete();
    }

}
