package ci.objis.api.Helper;

import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import ci.objis.model.Aliment;

public class AlimentHelper {

    //les proprietes
    private static final String COLLECTION_NAME = "aliment";

    //COLLECTION REFERENCE
    private static CollectionReference getAlimentCollection() {
        return FirebaseFirestore
                .getInstance()
                .collection("users")
                .document("cycle")
                .collection(COLLECTION_NAME);
    }

    //CREATE
    private Task<DocumentReference> createAliment(Aliment aliment) {
        return AlimentHelper
                .getAlimentCollection()
                .add(aliment);
    }

    //READ ONE
    private Task<DocumentSnapshot> getAliment(String uidAliment) {
        return AlimentHelper
                .getAlimentCollection()
                .document(uidAliment)
                .get();
    }

    //UPDATE
    private Task<Void> updateAliment(Aliment aliment) {
        return AlimentHelper
                .getAlimentCollection()
                .document(aliment.getUid())
                .set(aliment);
    }

    //DELETE
    private Task<Void> deleteAliment(String uidAliment) {
        return AlimentHelper
                .getAlimentCollection()
                .document(uidAliment)
                .delete();
    }

}
