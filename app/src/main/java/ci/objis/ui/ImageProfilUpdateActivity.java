package ci.objis.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.steelkiwi.cropiwa.CropIwaView;
import com.steelkiwi.cropiwa.config.CropIwaSaveConfig;

import butterknife.BindView;
import butterknife.ButterKnife;
import ci.objis.R;
import ci.objis.ui.base.BaseActivity;

public class ImageProfilUpdateActivity extends BaseActivity {

    @BindView(R.id.crop_view)
    CropIwaView cropView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_image_profil_update;
    }
}
