package ci.objis.ui;

import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ci.objis.R;
import ci.objis.api.Helper.UtilisateurHelper;
import ci.objis.service.IUtilisateurService;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.compteuser.ConfirmCreatCptActivity;

import com.crashlytics.android.Crashlytics;
import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import lombok.NonNull;

public class SplashScreenActivity extends BaseActivity {

    private final static String TAG = SplashScreenActivity.class.getSimpleName();
    //les proprietes
    private static int SPLASH_TIME_OUT = 6000;
    @BindView(R.id.videoView)
    VideoView videoView;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());

        FirebaseAuth auth = FirebaseAuth.getInstance();

        //instanciation du service utilisateur
        IUtilisateurService utilisateurService = new UtilisateurService();

        //lancer la video en background
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.chickenmovie);
        videoView.setVideoURI(uri);
        videoView.start();

        //verification de l'existance d'un compte
        if (!utilisateurService.getAll().isEmpty()) {
            //verifier si le compte est active
            if (utilisateurService.getAll().get(0).getIsConfirmed().equals(true)) {
                intent = new Intent(this, SelectElevageActivity.class);
            } else {
                intent = new Intent(this, ConfirmCreatCptActivity.class);
            }
        } else {
            //afficher l'activité connexion
            intent = new Intent(this, FirstScreenActivity.class);
        }


        new Handler().postDelayed(() -> {
            startActivity(intent);
            finish();
        }, SPLASH_TIME_OUT);

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_splashscreen;
    }

}
