package ci.objis.ui.client;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.api.ClientRestDao;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseActivity;

public class AllClientActivity extends BaseActivity {

    //les proprietes
    private ClientRestDao clientRestDao;

    @BindView(R.id.allclient_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.attentallclient_progre)
    NSidedProgressBar progressBar;

    @BindView(R.id.attentallclient_text)
    TextView txtAttente;

    @BindView(R.id.clientall_attenteallclient_linea)
    LinearLayout linerAttente;

    @BindView(R.id.attentallclient_reload)
    ImageView imgReload;

    @BindView(R.id.clientall_listevide)
    LinearLayout linearVide;

    CarouselView carouselView;

    int[] sampleImages = {R.mipmap.image, R.mipmap.imagehaut_mesactivites, R.mipmap.imagehaut_newactivity};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        clientRestDao = new ClientRestDao();
        clientRestDao.getAll(progressBar, recyclerView, this, linerAttente, txtAttente, imgReload, linearVide);

        carouselView = findViewById(R.id.allclient_carouselView);
        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);
        imgReload.setOnClickListener(v -> {
            clientRestDao.getAll(progressBar, recyclerView, AllClientActivity.this, linerAttente, txtAttente, imgReload, linearVide);
        });
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
        txtSdeTitleToolbar.setText(getString(R.string.clients));

        toolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MenuActionService.recherche(menu, this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_client_all;
    }

    ImageListener imageListener = (int position, ImageView imageView) -> {
        imageView.setImageResource(sampleImages[position]);
    };
}
