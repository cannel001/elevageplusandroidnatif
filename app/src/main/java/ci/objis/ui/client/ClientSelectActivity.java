package ci.objis.ui.client;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.adapter.recyclerview.all.AllClientAdapter;
import ci.objis.model.Client;
import ci.objis.service.ICompteUserService;
import ci.objis.service.impl.CompteUserService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseActivity;

public class ClientSelectActivity extends BaseActivity {

    //les proprietes
    @BindView(R.id.Clientselect_image)
    ImageView imageClient;

    @BindView(R.id.Clientselect_nom)
    TextView txtNomClient;

    @BindView(R.id.Clientselect_paysville)
    TextView txtVillePays;

    @BindView(R.id.Clientselect_adresse)
    TextView txtAdresse;

    @BindView(R.id.Clientselect_description)
    TextView txtDescript;

    Client client = null;

    @BindView(R.id.mapView)
    MapView mapView;

    @BindView(R.id.clientselect_floatingActionButton)
    FloatingActionButton floatingActionButton;

    @BindView(R.id.clientselect_cardinfoclient)
    CardView cardInfoClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, getString(R.string.acces_token));

        mapView.onCreate(savedInstanceState);

        //recuperation du client
        client = (Client) getIntent().getSerializableExtra(AllClientAdapter.CLIENTSELECT);

        final Double longitude = Double.valueOf(client.getLongitude());
        final Double latitude = Double.valueOf(client.getLatitude());
        final LatLng latLng = new LatLng(latitude, longitude);
        final LatLngBounds latLngBounds = new LatLngBounds.Builder()
                .include(latLng)
                .include(latLng)
                .build();

        mapView.getMapAsync(mapboxMap -> {

            mapboxMap.setLatLngBoundsForCameraTarget(latLngBounds);

            mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {

                style.addImage("marker-icon-id",
                        BitmapFactory.decodeResource(ClientSelectActivity.this.getResources(), R.drawable.mapbox_marker_icon_default));

                GeoJsonSource geoJsonSource = new GeoJsonSource("source-id",
                        Feature.fromGeometry(
                                Point.fromLngLat(longitude, latitude)
                        ));

                style.addSource(geoJsonSource);

                SymbolLayer symbolLayer = new SymbolLayer("layer-id", "source-id");
                symbolLayer.withProperties(
                        PropertyFactory.iconImage("marker-icon-id"));

                style.addLayer(symbolLayer);
            });
        });

        //affectation des proprietes du client aux variables
        txtNomClient.setText(client.getNom());
        txtAdresse.setText(client.getAdrPostal());
        txtVillePays.setText(String.format("%s, %s",client.getVille(),client.getPays()));
        txtDescript.setText(client.getDescript());

        //ajout de l'image avec picasso
        Picasso.get()
                .load(client.getLienImage())
                .placeholder(R.drawable.default_image)
                .error(R.drawable.default_image)
                .fit()
                .into(imageClient);

        afficherCardInfo();
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mon)));
        txtSdeTitleToolbar.setText(getString(R.string.client));

        toolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v-> {
                onBackPressed();
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_client_select;
    }

    //ajouter mapBox aux differentes lethodes du cycle de vie
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mapView.onSaveInstanceState(outState);
    }

    //methode permettant d'afficher le card info client
    void afficherCardInfo() {
        cardInfoClient.setVisibility(View.VISIBLE);
        floatingActionButton.hide();
    }

    //methode permettant d'afficher floating button
    void afficherFloatingBtn() {
        floatingActionButton.show();
        cardInfoClient.setVisibility(View.INVISIBLE);
    }

    //methode permettant d'afficher floating lorsqu'on clique sur imageView de card info
    @OnClick(R.id.clientselect_closecardinfo)
    void clikerSurImagClose() {
        afficherFloatingBtn();
    }

    //methode permettant d'afficher cardView info lorsqu'on click sur floating button
    @OnClick(R.id.clientselect_floatingActionButton)
    void clickerSurFloatinButton() {
        afficherCardInfo();
    }

    //methode permettant de lancer l'appel du client
    @OnClick(R.id.clientselect_cardappelclient)
    void clickerSurAppelMaintenant() {
        Uri telNumer = Uri.parse("tel:" + client.getNumTel());
        Intent intent = new Intent(Intent.ACTION_DIAL, telNumer);
        startActivity(intent);
    }

}
