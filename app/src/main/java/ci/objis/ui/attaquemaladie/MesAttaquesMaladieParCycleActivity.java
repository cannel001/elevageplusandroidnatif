package ci.objis.ui.attaquemaladie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.AttaqueMaladieParCycleAdapter;
import ci.objis.service.IAttaqueMaladieService;
import ci.objis.service.impl.AttaqueMaladieService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesAttaquesMaladieParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mesattaquemaladie_recyclerview)
    RecyclerView recyclerView;
    private IAttaqueMaladieService attaqueMaladieService;
    private AttaqueMaladieParCycleAdapter attaqueMaladieParCycleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.attaqmaladieimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_attaques_maladie_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        attaqueMaladieService=new AttaqueMaladieService();
        attaqueMaladieParCycleAdapter=new AttaqueMaladieParCycleAdapter(attaqueMaladieService.getAll());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager=new GridLayoutManager(this,3);

        recyclerView.setAdapter(attaqueMaladieParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String s) {
        attaqueMaladieParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
