package ci.objis.ui.attaquemaladie;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.libizo.CustomEditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.AttaqueMaladie;
import ci.objis.model.Cycle;
import ci.objis.service.IAttaqueMaladieService;
import ci.objis.service.ICycleService;
import ci.objis.service.impl.AttaqueMaladieService;
import ci.objis.service.impl.CycleService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.cycle.SuiviCycleActivity;

public class AddAttaqMaladieCyclActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = AddAttaqMaladieCyclActivity.class.getSimpleName();

    //les proprietes de l'entete
    @BindView(R.id.addattaquemaladie_edt_nommaladie)
    CustomEditText edtNomMaladie;

    @BindView(R.id.addattaquemaladie_edt_nbrmalade)
    CustomEditText edtNbrMaladie;

    @BindView(R.id.formulaire_dateoublie)
    DatePicker dateOubliePicker;

    @BindView(R.id.lnformulaire_dateoublie)
    LinearLayout lnDateOublie;

    private IAttaqueMaladieService attaqueMaladieService;

    private int jourSelect;
    private Cycle cycleSelect;
    private AttaqueMaladie attaqueMaladie;

    private ICycleService cycleService;

    private int requestCode;

    private Date dateOublie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);

        attaqueMaladieService = new AttaqueMaladieService();
        cycleService = new CycleService();

        cycleSelect = (Cycle) getIntent().getSerializableExtra(StartUp.CYCLESELECT);

        requestCode=getIntent().getIntExtra(StartUp.OPTIONSELECT,0);

        switch (requestCode) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);
                updateUIForFormul(SuiviCycleActivity.OPTION_NOUVEAU);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                updateUIForFormul(SuiviCycleActivity.OPTION_OUBLIE);
                break;
            default:
                break;
        }
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_attaqmaladie_add_cycl;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        //traitement lié à la toolbar
        setSupportActionBar(toolbar);
        txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
        txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.attaque)));

        //bouton retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    //----------------------------------
    // UI
    //-------------------------------------

    private void updateUIForFormul(int option) {
        switch (option) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvel)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.attaque)));
                lnDateOublie.setVisibility(View.GONE);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvel)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.attaque_oublie)));
                lnDateOublie.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    /**
     * methode permettant d'annuler l'enregistrement et revenir à l'activité precedente
     */
    @OnClick(R.id.btngroup_btnannuler)
    void clickerSurBtnAnnuler() {
        finish();
    }

    /**
     * methode permettant d'enregistrer une attaque
     */
    @OnClick(R.id.btngroup_btnvalider)
    void clickerSurBtnValider() {

        if (contrainteEnregis()) {

            attaqueMaladie = new AttaqueMaladie();

            attaqueMaladie.setNomMaladie(edtNomMaladie.getText().toString());
            attaqueMaladie.setNbBete(Long.valueOf(edtNbrMaladie.getText().toString()));
            attaqueMaladie.setCycle(cycleService.readOneByIdentif(cycleSelect.getIdentif()));

            AttaqueMaladie attaqueACreer=null;

            if(SuiviCycleActivity.OPTION_NOUVEAU==requestCode){
                attaqueACreer=attaqueMaladieService.save(attaqueMaladie);
            }else{
                int day=dateOubliePicker.getDayOfMonth();
                int month=dateOubliePicker.getMonth();
                int year=dateOubliePicker.getYear();
                try {
                    dateOublie=new SimpleDateFormat("dd-MM-yyyy").parse(String.format("%s-%s-%s",day,month,year));
                    attaqueMaladie.setLastModifiedDate(dateOublie);
                    attaqueACreer=attaqueMaladieService.createOublie(attaqueMaladie);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (attaqueACreer != null) {
                // Create Alert using Builder
                builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                        .setTitle(R.string.Felicitations)
                        .setMessage(R.string.Nouvelle_attaque_enregistree)
                        .addButton(getString(R.string.CONTINUER), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });

                // Show the alert
                CFAlertDialog alertDialog = builder.show();
                alertDialog.setOnDismissListener(dialog -> {
                    finish();
                });
            } else {
                //appel le dialog pour notifier une erreur pendant le traitement
                dialogErrorProgress();
            }
        }
    }

    /**
     * methode permettant de gerer les contraintes d'enregistrements
     *
     * @return
     */
    private Boolean contrainteEnregis() {
        if (TextUtils.isEmpty(edtNomMaladie.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_nom_de_la_maladie_sil_vous_plait)
                    .addButton("OK", Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });

            builder.show();
            return false;
        }

        if (TextUtils.isEmpty(edtNbrMaladie.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_nombre_de_betes_malades)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });

            builder.show();

            return false;
        }
        return true;
    }

}
