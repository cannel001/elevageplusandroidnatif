package ci.objis.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.circulardialog.CDialog;
import com.example.circulardialog.extras.CDConstants;
import com.luseen.spacenavigation.SpaceItem;
import com.luseen.spacenavigation.SpaceNavigationView;
import com.luseen.spacenavigation.SpaceOnClickListener;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.service.ICycleService;
import ci.objis.service.impl.CycleService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.main.AccueilFragment;
import ci.objis.ui.compteuser.ProfilUserFragment;
import ci.objis.ui.client.AllClientActivity;
import ci.objis.ui.cycle.MesCyclesActivity;
import ci.objis.ui.cycle.NewCycleActivity;
import ci.objis.ui.conseil.ConseilAllActivity;
import ci.objis.ui.veterinaire.AllVeterinaireFragment;

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ICycleService cycleService;

    @BindView(R.id.space)
    SpaceNavigationView spaceNavigationView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, AccueilFragment.newInstance())
                    .commitNow();
        }

        //instanciation
        cycleService = new CycleService();

        //call configuerDrawerLayout
        configuerDrawerLayout();

        //call configuerSpaceNavigationView
        configuerSpaceNavigationView(savedInstanceState);

        //call dialog en cours de dev
        dialogDevEnCours();
    }

    private void dialogDevEnCours() {
        dialog=new Dialog(this);
        dialog.setContentView(R.layout.dialog_developencours);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        TextView txtOkDialog=dialog.findViewById(R.id.dialogdevencours_txtok);
        txtOkDialog.setOnClickListener(v->{
            dialog.dismiss();
        });
    }

    private void configuerSpaceNavigationView(Bundle savedInstanceState) {
        spaceNavigationView.initWithSaveInstanceState(savedInstanceState);

        spaceNavigationView.addSpaceItem(new SpaceItem(null, R.mipmap.house));
        spaceNavigationView.addSpaceItem(new SpaceItem(null, R.mipmap.doctor01white));
        spaceNavigationView.addSpaceItem(new SpaceItem(null, R.mipmap.callwhite));
        spaceNavigationView.addSpaceItem(new SpaceItem(null, R.mipmap.profile));
        spaceNavigationView.setSoundEffectsEnabled(true);
        clickerSurMenuBas();
    }

    private void configuerDrawerLayout() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_monaccueil;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_mon_profil) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container, ProfilUserFragment.newInstance()).commitNow();
        } else if (id == R.id.nav_mes_activites) {
            if (
                    cycleService.readAll().isEmpty()) {
                new CDialog(this).createAlert(getString(R.string.Vous_navez_pas_dactivites_en_cours),
                        CDConstants.WARNING,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                        .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                        .setDuration(3000)   // in milliseconds
                        .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                        .show();
            } else {
                Intent intent = new Intent(this, MesCyclesActivity.class);
                startActivity(intent);
            }
        } else if (id == R.id.nav_mes_mesclients) {
            Intent intent = new Intent(MainActivity.this, AllClientActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_mon_conseiller) {
            Intent intent = new Intent(MainActivity.this, ConseilAllActivity.class);
            startActivity(intent);
        }


        DrawerLayout drawer = MainActivity.this.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /**
     * Methode permettant d'ouvrir un fragment lorsqu'on clique sur un item du menu bas
     */
    void clickerSurMenuBas() {

        spaceNavigationView.setSpaceOnClickListener(new SpaceOnClickListener() {
            @Override
            public void onCentreButtonClick() {
                getSupportFragmentManager().beginTransaction().replace(R.id.container, NewCycleActivity.newInstance()).commitNow();
            }

            @Override
            public void onItemClick(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, AccueilFragment.newInstance()).commitNow();
                        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Elevage)));
                        txtSdeTitleToolbar.setText(getString(R.string.plus));
                        break;
                    case 1:
                        dialog.show();
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, AllVeterinaireFragment.newInstance()).commitNow();
                        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
                        txtSdeTitleToolbar.setText(getString(R.string.veterinaires));
                        break;
                    case 3:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, ProfilUserFragment.newInstance()).commitNow();
                        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mon)));
                        txtSdeTitleToolbar.setText(getString(R.string.profil));
                        break;
                }
            }

            @Override
            public void onItemReselected(int itemIndex, String itemName) {
                switch (itemIndex) {
                    case 0:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, AccueilFragment.newInstance()).commitNow();
                        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Elevage)));
                        txtSdeTitleToolbar.setText(getString(R.string.plus));
                        break;
                    case 1:
                        dialog.show();
                        break;
                    case 2:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, AllVeterinaireFragment.newInstance()).commitNow();
                        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
                        txtSdeTitleToolbar.setText(R.string.veterinaires);
                        break;
                    case 3:
                        getSupportFragmentManager().beginTransaction().replace(R.id.container, ProfilUserFragment.newInstance()).commitNow();
                        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mon)));
                        txtSdeTitleToolbar.setText(getString(R.string.profil));
                        break;
                }
            }
        });
    }
}
