package ci.objis.ui.compteuser;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;
import com.libizo.CustomEditText;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.api.UtilisateurRestDao;
import ci.objis.ui.FirstScreenActivity;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.ui.base.BaseActivity;

public class RegisterActivity extends BaseActivity {

    //les proprietes
    @BindView(R.id.edt_inscript_tel)
    CustomEditText edtTel;

    @BindView(R.id.txt_inscript_login)
    TextView txtLogin;

    @BindView(R.id.register_NSidedProgressBar)
    NSidedProgressBar nSidedProgressBar;

    private UtilisateurRestDao utilisateurDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(StartUp.PREF, MODE_PRIVATE);

        nSidedProgressBar.setVisibility(View.INVISIBLE);
        utilisateurDao = new UtilisateurRestDao(this, nSidedProgressBar);

        clickerSurTxtLogin();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_register;
    }

    //methode permettant de s'inscrire
    @OnClick(R.id.btn_inscript)
    void clickerSurBtnInscript() {
        if (contrainte(edtTel.getText().toString())) {
            //tentative d'inscription
            utilisateurDao.registerUser(edtTel.getText().toString());
        }
    }

    //methode permettant de se connecter
    void clickerSurTxtLogin() {
        txtLogin.setOnClickListener(v -> {
            Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
            startActivity(intent);
        });

    }

    //contrainte de selection
    private Boolean contrainte(String tel) {
        if (TextUtils.isEmpty(tel)) {
            edtTel.setError(getString(R.string.Veuillez_entrer_un_numero_sil_vous_plait));
            edtTel.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(RegisterActivity.this, FirstScreenActivity.class);
        startActivity(intent);
    }
}
