package ci.objis.ui.compteuser;

import android.os.Bundle;
import android.text.TextUtils;
import android.widget.EditText;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.api.UtilisateurRestDao;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.ui.base.BaseActivity;

public class ConfirmCreatCptActivity extends BaseActivity {

    //les proprietes
    private static final String TAG = ConfirmCreatCptActivity.class.getSimpleName();

    private UtilisateurRestDao utilisateurDao;

    @BindView(R.id.edt_confir_compte)
    EditText edtConfirCompte;

    @BindView(R.id.confirmcompte_NSidedProgressBar)
    NSidedProgressBar nSidedProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_confirm_creat_cpt;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        utilisateurDao=new UtilisateurRestDao(this,nSidedProgressBar);
    }

    @OnClick(R.id.btn_confirm_compt)
    void clickerSurBtnConfirm() {
        if (contrainte()) {
            utilisateurDao.activeCompteUser(new UtilisateurService().getAll().get(0).getTel(), edtConfirCompte.getText().toString());
        }
    }

    boolean contrainte() {
        if (TextUtils.isEmpty(edtConfirCompte.getText().toString())) {
            edtConfirCompte.setError(getString(R.string.Veuillez_entrer_le_code_de_confirmation));
            edtConfirCompte.requestFocus();
            return false;
        }
        return true;
    }


}
