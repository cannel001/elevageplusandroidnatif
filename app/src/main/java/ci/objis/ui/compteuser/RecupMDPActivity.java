package ci.objis.ui.compteuser;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;
import com.libizo.CustomEditText;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.api.UtilisateurRestDao;
import ci.objis.ui.base.BaseActivity;

public class RecupMDPActivity extends BaseActivity {

    //les proprietes
    @BindView(R.id.recupmdp_edt_tel)
    CustomEditText edtTel;

    @BindView(R.id.recupmdp_NSidedProgressBar)
    NSidedProgressBar nSidedProgressBar;

    private UtilisateurRestDao utilisateurRestDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nSidedProgressBar.setVisibility(View.INVISIBLE);
        utilisateurRestDao=new UtilisateurRestDao(this,nSidedProgressBar);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_recup_mdp;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent=new Intent(RecupMDPActivity.this,LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.recupmdp_btnrecup)
    void clickerSurBtnRecup(){
        utilisateurRestDao.recupPassword(edtTel.getText().toString());
    }
}
