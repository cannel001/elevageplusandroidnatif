package ci.objis.ui.compteuser;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.libizo.CustomEditText;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.api.UtilisateurRestDao;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.ui.base.BaseActivity;

public class ChangerMdpActivity extends BaseActivity {

    @BindView(R.id.changemdp_ancienmdp)
    CustomEditText edtAncienMdp;

    @BindView(R.id.changemdp_newmdp)
    CustomEditText edtNewMdp;

    @BindView(R.id.changemdp_repmdp)
    CustomEditText edtRepMdp;

    @BindView(R.id.changemdp_lnformulaire)
    LinearLayout lnFormulaire;

    @BindView(R.id.changemdp_lnprogessbar)
    LinearLayout lnProgressBar;

    private CFAlertDialog.Builder builder;

    private UtilisateurRestDao utilisateurRestDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        lnProgressBar.setVisibility(View.GONE);
        utilisateurRestDao = new UtilisateurRestDao(this, lnFormulaire, lnProgressBar, this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_compteuser_changermdp;
    }

    //methode permettant d'annuler la mise a jour
    @OnClick(R.id.changemdp_btnannuler)
    void clickerSurBtnAnnuler() {
        finish();
    }

    //methode permettant de faire la mise a jour du mot de passe
    @OnClick(R.id.changemdp_btnvalider)
    void clickerSurBtnValider() {
        if (contrainteMdp()) {
            utilisateurRestDao.updatePassword(edtAncienMdp.getText().toString(), edtNewMdp.getText().toString(), new UtilisateurService().getAll().get(0).getTel());
        }
    }

    //contrainte de validation du mot de passe
    Boolean contrainteMdp() {
        if (!TextUtils.isEmpty(edtNewMdp.getText().toString()) || !TextUtils.isEmpty(edtNewMdp.getText().toString())) {
            if (!edtNewMdp.getText().toString().equals(edtRepMdp.getText().toString())) {
                // Create Alert using Builder
                builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setTitle(getString(R.string.Oups))
                        .setMessage(R.string.Veuillez_entrer_un_mot_de_passe_valide_sil_vous_plait)
                        .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });
                builder.show();
                return false;
            }
        }
        return true;
    }
}
