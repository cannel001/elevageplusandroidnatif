package ci.objis.ui.compteuser;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;
import com.libizo.CustomEditText;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.api.UtilisateurRestDao;
import ci.objis.ui.FirstScreenActivity;
import ci.objis.R;
import ci.objis.ui.base.BaseActivity;

public class LoginActivity extends BaseActivity {

    //les proprietes
    private final static String TAG = LoginActivity.class.getSimpleName();

    @BindView(R.id.edt_login_tel)
    CustomEditText edtTel;

    @BindView(R.id.edt_login_mdp)
    CustomEditText edtMdp;

    @BindView(R.id.txt_login_inscript)
    TextView txtInscrip;

    private Intent intent;

    private UtilisateurRestDao utilisateurDao;

    @BindView(R.id.login_NSidedProgressBar)
    NSidedProgressBar nSidedProgressBar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nSidedProgressBar.setVisibility(View.INVISIBLE);

        utilisateurDao = new UtilisateurRestDao(this, nSidedProgressBar);

        clickerSurTxtInscript();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_login;
    }

    /**
     * methode permettant de se connecter
     */
    @OnClick(R.id.btn_login_connex)
    void clickerSurBtnConnect() {
        if (contraintDeConexion()) {
            String telUser = edtTel.getText().toString();
            String pass = TextUtils.isEmpty(edtMdp.getText().toString()) ? null : edtMdp.getText().toString();

            //tentative de connexion
            utilisateurDao.loginUser(telUser, pass);
        }
    }

    /**
     * Methode permettant d'afficher l'activity de recuperation du mot de passe
     */
    @OnClick(R.id.login_mdpoublie)
    void clickerSurBtnMdpOublie() {
        intent = new Intent(LoginActivity.this, RecupMDPActivity.class);
        startActivity(intent);
    }

    /**
     * methode permettant d'ouvir l'activity register
     */
    void clickerSurTxtInscript() {
        txtInscrip.setOnClickListener(v -> {
            intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        });
    }

    /**
     * Methode permettant de gerer les contraintes de saisies
     *
     * @return
     */
    boolean contraintDeConexion() {
        if (TextUtils.isEmpty(edtTel.getText().toString())) {
            edtTel.setError(getString(R.string.Veuillez_entrer_le_numero_de_telephone));
            edtTel.requestFocus();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        intent = new Intent(LoginActivity.this, FirstScreenActivity.class);
        startActivity(intent);
    }


}
