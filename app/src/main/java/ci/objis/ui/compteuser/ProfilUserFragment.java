package ci.objis.ui.compteuser;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.nikartm.button.FitButton;

import java.text.SimpleDateFormat;

import ci.objis.model.Utilisateur;
import ci.objis.service.ICompteUserService;
import ci.objis.service.IUtilisateurService;
import ci.objis.service.impl.CompteUserService;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.R;
import ci.objis.ui.main.MainViewModel;

public class ProfilUserFragment extends Fragment {

    //les proprietes
    private MainViewModel mainViewModel;

    private Intent intent;

    private LinearLayout lnchangpassword;
    private LinearLayout lnCopyright;

    private TextView txtNomPrenom;
    private TextView txtDateAdhesion;
    private TextView txttelephone;
    private TextView txtFermerSession;
    private String nomPrenom;

    private FitButton btnEditCompte;

    private IUtilisateurService utilisateurService;
    private Utilisateur utilisateur;

    private ICompteUserService compteUserService;

    public static ProfilUserFragment newInstance() {
        return new ProfilUserFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_profil_user, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        //faire le matching
        lnchangpassword = getActivity().findViewById(R.id.profiluser_lnchangpassword);
        txtNomPrenom = getActivity().findViewById(R.id.profiluser_nomprenom);
        txtDateAdhesion = getActivity().findViewById(R.id.profiluser_dateadhesion);
        txttelephone = getActivity().findViewById(R.id.profiluser_telephone);
        txtFermerSession = getActivity().findViewById(R.id.profiluser_fermersession);
        btnEditCompte = getActivity().findViewById(R.id.profiluser_editprofil);
        lnCopyright = getActivity().findViewById(R.id.profiluser_copyright);

        //instancier les objets
        utilisateurService = new UtilisateurService();
        compteUserService = new CompteUserService(getContext(), getActivity());

        //appeler les methodes
        clickerSurLinearChangMdp();
        chargInfosUser();
        clickerSurFermerSession();
        clickerSurBtnEditProfil();
        clickerSurLinearCopyright();
    }

    /**
     * Methode permettant d'ouvrir l'intent de copyright
     */
    void clickerSurLinearCopyright() {
        lnCopyright.setOnClickListener(v -> {
            intent = new Intent(getActivity(), CopyrightActivity.class);
            startActivity(intent);
        });
    }

    /**
     * Methode permettant d'ouvrir l'intent de changement du mot de passe
     */
    void clickerSurLinearChangMdp() {
        lnchangpassword.setOnClickListener(v -> {
            intent = new Intent(getActivity(), ChangerMdpActivity.class);
            startActivity(intent);
        });
    }

    /**
     * Methode permettant de fermer la session
     */
    void clickerSurFermerSession() {
        txtFermerSession.setOnClickListener(v -> {
            compteUserService.logoutUser();
        });
    }

    /**
     * Methode permettant d'editer le profil de l'utilisateur
     */
    void clickerSurBtnEditProfil() {
        btnEditCompte.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), EditerProfitActivity.class);
            startActivity(intent);
        });
    }

    /**
     * Methode permettant de charger les informations de l'utilisateur
     */
    void chargInfosUser() {
        Utilisateur utilisateur = utilisateurService.getAll().get(0);
        nomPrenom = null;

        //ajout du nom et prenom
        if (!TextUtils.isEmpty(utilisateur.getNom())) {
            nomPrenom = utilisateur.getNom();
            if (!TextUtils.isEmpty(utilisateur.getPrenom())) {
                nomPrenom += " " + utilisateur.getPrenom();
            }
        }
        if (!TextUtils.isEmpty(nomPrenom)) {
            txtNomPrenom.setText(nomPrenom);
        } else {
            txtNomPrenom.setText(getString(R.string.Utilisateur));
        }

        //ajout de la date d'adhesion
        txtDateAdhesion.setText(String.format("%s %s %s",getString(R.string.A_rejoint_Elevage_plus_en), new SimpleDateFormat("MMMM").format(utilisateur.getDateCreation()), new SimpleDateFormat("yyyy").format(utilisateur.getDateCreation())));

        //ajout du numero de telephone
        txttelephone.setText(String.format("+225 %s", utilisateur.getTel()));
    }


}
