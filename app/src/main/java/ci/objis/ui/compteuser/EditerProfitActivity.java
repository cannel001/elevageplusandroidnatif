package ci.objis.ui.compteuser;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.libizo.CustomEditText;

import java.io.File;
import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.api.UtilisateurRestDao;
import ci.objis.model.Utilisateur;
import ci.objis.service.IUtilisateurService;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.ui.base.BaseActivity;

import com.yalantis.ucrop.ImagePickerActivity;

public class EditerProfitActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = EditerProfitActivity.class.getSimpleName();

    private IUtilisateurService utilisateurService;
    private UtilisateurRestDao utilisateurRestDao;
    private Utilisateur utilisateur;

    public static final int REQUEST_IMAGE = 100;

    @BindView(R.id.img_profile)
    ImageView imgProfile;

    @BindView(R.id.editprofil_edtnom)
    CustomEditText edtNom;

    @BindView(R.id.editprofil_edtprenom)
    CustomEditText edtPrenom;

    @BindView(R.id.editprofil_lnformulaire)
    LinearLayout lnFormulaire;

    @BindView(R.id.editprofil_lnprogessbar)
    LinearLayout lnProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadProfileDefault();

        // Clearing older images from cache directory
        // don't call this line if you want to choose multiple images in the same activity
        // call this once the bitmap(s) usage is over
        ImagePickerActivity.clearCache(this);

        //cacher linear progress par defaut
        lnProgressBar.setVisibility(View.GONE);

        utilisateurService = new UtilisateurService();
        utilisateurRestDao = new UtilisateurRestDao(this, lnFormulaire, lnProgressBar, EditerProfitActivity.this);

        chargInfosUser();
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        //traitement lié à la toolbar
        toolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(toolbar);
        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Editer)));
        txtSdeTitleToolbar.setText(String.format(" %s",getString(R.string.compte)));
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_profit_editer;
    }

    //methode permettant d'annuler la mise à jour et revenir à l'intent precedent
    @OnClick(R.id.editprofil_btnannuler)
    void clickerSurbtnAnnuler() {
        finish();
    }

    //methode permettant de faire la mise a jour des informations de l'utilisateur
    @OnClick(R.id.editprofil_btnvalider)
    void clickerSurBtnValider() {
        //mise a jour du compte de l'utilisateur
        if (contrainte()) {
            utilisateur.setNom(edtNom.getText().toString());
            utilisateur.setPrenom(edtPrenom.getText().toString());
            utilisateurRestDao.updateCompte(utilisateur);
        }
    }

    //methode permettant de gerer les contraintes d'enregistrement
    Boolean contrainte() {
        if (TextUtils.isEmpty(edtNom.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_votre_nom_sil_vous_plait)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });
            builder.show();
            return false;
        }
        return true;
    }

    /**
     * Methode permettant de charger les informations de l'utilisateur
     */
    void chargInfosUser() {
        utilisateur = utilisateurService.getAll().get(0);

        //ajout du nom et prenom
        if (!TextUtils.isEmpty(utilisateur.getNom())) {
            edtNom.setText(utilisateur.getNom());
        }

        if (!TextUtils.isEmpty(utilisateur.getPrenom())) {
            edtPrenom.setText(utilisateur.getPrenom());
        }

    }

    private void loadProfile(String url) {
        Log.d(TAG, "Image cache path: " + url);

        Glide.with(this).load(url)
                .into(imgProfile);
        imgProfile.setColorFilter(ContextCompat.getColor(this, android.R.color.transparent));
    }

    private void loadProfileDefault() {
        Glide.with(this).load(R.mipmap.baseline_account_circle_black_48)
                .into(imgProfile);
        imgProfile.setColorFilter(ContextCompat.getColor(this, R.color.profile_default_tint));
    }

    @OnClick({R.id.img_plus, R.id.img_profile})
    void onProfileImageClick() {
        Dexter.withActivity(this)
                .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        if (report.areAllPermissionsGranted()) {
                            showImagePickerOptions();
                        }

                        if (report.isAnyPermissionPermanentlyDenied()) {
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private void showImagePickerOptions() {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent();
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent();
            }
        });
    }

    private void launchCameraIntent() {
        Intent intent = new Intent(EditerProfitActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDHT_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDHT, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent intent = new Intent(EditerProfitActivity.this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = data.getParcelableExtra("path");
                try {
                    // You can update this bitmap to your server
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);

                    File file=new File(bitmap.toString());

                    // loading profile image from local cache
                    loadProfile(uri.toString());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(EditerProfitActivity.this);
        builder.setTitle("test");
        builder.setMessage("ljfkjdf");
        builder.setPositiveButton("hfhdg", (dialog, which) -> {
            dialog.cancel();
            openSettings();
        });
        builder.setNegativeButton(getString(android.R.string.cancel), (dialog, which) -> dialog.cancel());
        builder.show();

    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }
}
