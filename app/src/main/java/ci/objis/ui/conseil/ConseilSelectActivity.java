package ci.objis.ui.conseil;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.adapter.recyclerview.all.AllConseilAdapter;
import ci.objis.model.Publication;
import ci.objis.service.ICompteUserService;
import ci.objis.service.impl.CompteUserService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseActivity;

public class ConseilSelectActivity extends BaseActivity {

    //les proprietes
    private static String TAG = ConseilSelectActivity.class.getSimpleName();

    @BindView(R.id.articpubli_titre)
    TextView txtTitre;

    @BindView(R.id.articpubli_intro)
    TextView txtIntro;

    @BindView(R.id.articpubli_contenu)
    TextView txtContenu;

    @BindView(R.id.articpubli_image)
    ImageView imagePublie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(StartUp.PREF, MODE_PRIVATE);

        //recuperation de la publication selectionné
        Publication publication = (Publication) getIntent().getSerializableExtra(AllConseilAdapter.ARTICLESELECT);

        //affectation des valeurs aux variables
        txtTitre.setText(publication.getTitre());
        txtIntro.setText(publication.getIntro());
        txtContenu.setText(publication.getContenu());

        //recuperation et affichage de l'image avec picasso
        Picasso.get()
                .load(publication.getLienPhoto())
                .placeholder(R.drawable.ic_camera_front)
                .error(R.drawable.ic_camera_front)
                .fit()
                .into(imagePublie);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_article_publie;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Mon)));
        txtSdeTitleToolbar.setText(getString(R.string.conseil));

        toolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }
}
