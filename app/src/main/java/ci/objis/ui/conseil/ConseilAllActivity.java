package ci.objis.ui.conseil;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.iitr.kaishu.nsidedprogressbar.NSidedProgressBar;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import java.util.LinkedList;
import java.util.List;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.api.PublicationRestDao;
import ci.objis.model.Publication;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseActivity;

public class ConseilAllActivity extends BaseActivity {

    //les proprietes
    @BindView(R.id.monconseiller_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.attentconseiller_text)
    TextView textAttente;

    @BindView(R.id.attentconseiller_progre)
    NSidedProgressBar progressBar;

    @BindView(R.id.conseilall_attenteconseiller_linea)
    LinearLayout linearAttente;

    @BindView(R.id.conseilall_listevide)
    LinearLayout linearConseilVide;

    @BindView(R.id.attentconseiller_reload)
    ImageView imgReload;

    private PublicationRestDao publicationRestDao = null;
    public static List<Publication> listAllPublications = null;

    CarouselView carouselView;

    int[] sampleImages = {R.mipmap.backgroundboeuf, R.mipmap.backgroundboeuf, R.mipmap.imagehaut_newactivity};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = getSharedPreferences(StartUp.PREF, MODE_PRIVATE);

        //chargement des données à partir de l'api
        publicationRestDao = new PublicationRestDao();
        listAllPublications = new LinkedList<>();
        listAllPublications = publicationRestDao.getAll(progressBar, recyclerView, this, linearAttente, textAttente, imgReload,linearConseilVide);

        imgReload.setOnClickListener(v -> {
            listAllPublications = publicationRestDao.getAll(progressBar, recyclerView, ConseilAllActivity.this, linearAttente, textAttente, imgReload,linearConseilVide);
        });

        carouselView = findViewById(R.id.monconseiller_carouselView);
        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);

    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
        txtSdeTitleToolbar.setText(getString(R.string.conseils));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });

        toolbar.setNavigationIcon(R.drawable.ic_back);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MenuActionService.recherche(menu, this);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_conseil_all;
    }

    ImageListener imageListener = ((position, imageView) -> {
        imageView.setImageResource(sampleImages[position]);
    });

}
