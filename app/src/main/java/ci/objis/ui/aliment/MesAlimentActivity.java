package ci.objis.ui.aliment;

import android.content.res.ColorStateList;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.adapter.tabs.TabsAlimentAdapter;
import ci.objis.service.IAlimentService;
import ci.objis.service.impl.AlimentService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.base.BaseListAllActivity;

public class MesAlimentActivity extends BaseListAllActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        //affectation des valeurs aux textview de l'entete
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.LISTE));
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.STATISTIQUE));
                        tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.grisfonce)));

                        viewPager.setAdapter(new TabsAlimentAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null)));
                        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                switch (tab.getPosition()) {
                                    case 0:
                                        navHeader.setVisibility(View.VISIBLE);
                                        break;
                                    case 1:
                                        navHeader.setVisibility(View.GONE);
                                        break;
                                    default:
                                        break;
                                }
                                viewPager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });

                        IAlimentService alimentService = new AlimentService();

                        txtNbQteThisMonth.setText(String.format("%s %s",String.valueOf(alimentService.getAllNbRegisterByThisMonth(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))),getString(R.string.ce_mois)));
                        txtNbQteThisYear.setText(String.format("%s %s",String.valueOf(alimentService.getAllNbRegisterByThisYear(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))),getString(R.string.cette_annee)));
                    }
                });
            }
        });

        thread.start();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_aliment_all;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
        txtSdeTitleToolbar.setText(getString(R.string.aliment));
        kenBurnsView.setImageResource(R.drawable.chickenfoodimage);
    }

}
