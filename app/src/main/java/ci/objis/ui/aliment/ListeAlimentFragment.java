package ci.objis.ui.aliment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ci.objis.R;
import ci.objis.adapter.recyclerview.all.AllAlimentAdapter;
import ci.objis.service.IAlimentService;
import ci.objis.service.impl.AlimentService;
import ci.objis.service.impl.MenuActionService;


public class ListeAlimentFragment extends Fragment implements SearchView.OnQueryTextListener {

    //les proprietes
    private RecyclerView recyclerView;
    private IAlimentService alimentService;
    private AllAlimentAdapter allAlimentAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_liste_aliment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SearchView searchView = MenuActionService.mySearchView;

        searchView.setOnQueryTextListener(this);

        setUprecycler();
    }

    private void setUprecycler() {
        alimentService = new AlimentService();
        recyclerView = getActivity().findViewById(R.id.allaliment_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        allAlimentAdapter = new AllAlimentAdapter(alimentService.readAll());

        recyclerView.setAdapter(allAlimentAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        allAlimentAdapter.getFilter().filter(s);

        return false;
    }
}
