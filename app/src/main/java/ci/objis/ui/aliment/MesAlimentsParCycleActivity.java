package ci.objis.ui.aliment;

import android.os.Bundle;
import android.support.annotation.Nullable;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.AlimentParCycleAdapter;
import ci.objis.service.IAlimentService;
import ci.objis.service.impl.AlimentService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesAlimentsParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mesalimentparcyle_recyclerview)
    RecyclerView recyclerView;
    private IAlimentService alimentService;
    private AlimentParCycleAdapter alimentParCycleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.chickenfoodimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_aliments_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        alimentService = new AlimentService();
        alimentParCycleAdapter = new AlimentParCycleAdapter(alimentService.readAll());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(alimentParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        alimentParCycleAdapter.getFilter().filter(newText);
        return false;
    }
}
