package ci.objis.ui.aliment;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.libizo.CustomEditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.Aliment;
import ci.objis.model.Cycle;
import ci.objis.service.IAlimentService;
import ci.objis.service.ICycleService;
import ci.objis.service.impl.AlimentService;
import ci.objis.service.impl.CycleService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.cycle.SuiviCycleActivity;

public class AddAlimentCycleActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = AddAlimentCycleActivity.class.getSimpleName();

    //les proprietes de l'entete
    @BindView(R.id.addalimentcycle_edt_libellealiment)
    CustomEditText edtAlimentLibelle;

    @BindView(R.id.addalimentcycle_edt_qtealiment)
    CustomEditText edtAlimentQuantite;

    @BindView(R.id.formulaire_dateoublie)
    DatePicker dateOubliePicker;

    @BindView(R.id.lnformulaire_dateoublie)
    LinearLayout lnDateOublie;

    private IAlimentService alimentService;

    private int jourSelect;
    private Cycle cycleSelect;
    private Aliment aliment;

    private ICycleService cycleService;

    private int requestCode;

    private Date dateOublie;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);

        alimentService = new AlimentService();
        cycleService = new CycleService();

        cycleSelect = (Cycle) getIntent().getSerializableExtra(StartUp.CYCLESELECT);

        requestCode=getIntent().getIntExtra(StartUp.OPTIONSELECT,0);

        switch (requestCode) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);
                updateUIForFormul(SuiviCycleActivity.OPTION_NOUVEAU);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                updateUIForFormul(SuiviCycleActivity.OPTION_OUBLIE);
                break;
            default:
                break;
        }
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        //traitement lié à la toolbar
        txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvel)));
        txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.aliment)));

        //bouton retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_aliment_add_cycle;
    }

    @OnClick(R.id.btngroup_btnannuler)
    void clickerSurBtnAnnuler() {
        finish();
    }

    //methode permettant d'enregistrer un aliment
    @OnClick(R.id.btngroup_btnvalider)
    void clickerSurBtnValider() {
        if (contrainteEnregis()) {

            aliment = new Aliment();

            aliment.setLibelle(edtAlimentLibelle.getText().toString());
            aliment.setQuantite(Float.valueOf(edtAlimentQuantite.getText().toString()));
            aliment.setCycle(cycleService.readOneByIdentif(cycleSelect.getIdentif()));

            Aliment alimentACreer=null;

            if(SuiviCycleActivity.OPTION_NOUVEAU==requestCode){
                alimentACreer=alimentService.create(aliment);
            }else{
                int day=dateOubliePicker.getDayOfMonth();
                int month=dateOubliePicker.getMonth();
                int year=dateOubliePicker.getYear();
                try {
                    dateOublie=new SimpleDateFormat("dd-MM-yyyy").parse(String.format("%s-%s-%s",day,month,year));
                    aliment.setLastModifiedDate(dateOublie);
                    alimentACreer=alimentService.createOublie(aliment);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (alimentACreer != null) {
                // Create Alert using Builder
                builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                        .setTitle(R.string.Felicitations)
                        .setMessage(R.string.Nouvel_aliment_enregistre)
                        .addButton(getString(R.string.CONTINUER), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });

                // Show the alert
                CFAlertDialog alertDialog = builder.show();
                alertDialog.setOnDismissListener(dialog -> {
                    finish();
                });
            } else {
                //appel le dialog pour notifier une erreur pendant le traitement
                dialogErrorProgress();
            }
        }

    }

    //methode permettant de gerer les contraintes de selection
    private Boolean contrainteEnregis() {
        if (TextUtils.isEmpty(edtAlimentLibelle.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_nom_de_laliment)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });
            builder.show();
            return false;
        }

        if (TextUtils.isEmpty(edtAlimentQuantite.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(R.string.Oups)
                    .setMessage(R.string.Veuillez_entrer_la_quantite_de_laliment)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });
            builder.show();
            return false;
        }

        return true;
    }

    //----------------------------------
    // UI
    //-------------------------------------

    private void updateUIForFormul(int option) {
        switch (option) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvel)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.aliment)));
                lnDateOublie.setVisibility(View.GONE);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvel)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.aliment_oublie)));
                lnDateOublie.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

}
