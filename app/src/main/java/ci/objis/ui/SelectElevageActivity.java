package ci.objis.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.api.Helper.UtilisateurHelper;
import ci.objis.model.Utilisateur;
import ci.objis.service.impl.UtilisateurService;
import ci.objis.ui.base.BaseActivity;

import static maes.tech.intentanim.CustomIntent.customType;

public class SelectElevageActivity extends BaseActivity {

    //les proprietes
    private static final String TAG = SelectElevageActivity.class.getSimpleName();
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_select_elevage;
    }

    //methode permettant d'ouvrir le menu des poulets
    @OnClick(R.id.selectelevage_poulet)
    void clickerSurBtnPoulet() {

        //modifier la valeur
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(StartUp.ELEVAGESELECTIONNE, "POULET");
        editor.apply();

        Intent intent = new Intent(SelectElevageActivity.this, MainActivity.class);
        startActivity(intent);
        customType(this, "fadein-to-fadeout");
    }

    //methode permettant d'ouvrir le menu des poulets
    @OnClick(R.id.selectelevage_pintade)
    void clickerSurBtnPintade(View view) {
        Snackbar.make(view, R.string.Bientot_disponible, Snackbar.LENGTH_LONG).show();
    }

    @Override
    protected void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();

        //verifier si l'utilisateur est connecté ou pas
        FirebaseUser user = mAuth.getCurrentUser();

        Boolean isSignedIn = (user != null);

        if(isSignedIn){
            Log.i(TAG,"merci Seigneur");
            Toast.makeText(SelectElevageActivity.this, "Authentication validé", Toast.LENGTH_SHORT).show();
        }else{

            mAuth.signInAnonymously().addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInAnonymously:success");
                        Toast.makeText(SelectElevageActivity.this, "Authentication validé", Toast.LENGTH_SHORT).show();
                    }else{
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInAnonymously:failure", task.getException());
                        Toast.makeText(SelectElevageActivity.this, "Authentication failed", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }


        Utilisateur utilisateur = new UtilisateurService().getAll().get(0);

        UtilisateurHelper.createUser(utilisateur);

        UtilisateurHelper.getUtilisateur(utilisateur.getTel()).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                UtilisateurHelper.createUser(utilisateur);
            }
        });
    }
}
