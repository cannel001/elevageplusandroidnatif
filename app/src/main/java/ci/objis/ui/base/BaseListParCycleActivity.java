package ci.objis.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.flaviofaria.kenburnsview.KenBurnsView;
import com.flaviofaria.kenburnsview.RandomTransitionGenerator;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.service.impl.MenuActionService;

public abstract class BaseListParCycleActivity extends BaseActivity implements SearchView.OnQueryTextListener{

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.burnview_cycle)
    protected KenBurnsView kenBurnsView;

    private Boolean moving = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                 runOnUiThread(new Runnable() {
                     @Override
                     public void run() {
                         fab.setOnClickListener(view -> {
                             onBackPressed();
                         });

                         SearchView searchView = MenuActionService.mySearchView;
                         searchView.setOnQueryTextListener(BaseListParCycleActivity.this);

                         setUpRecycler();

                         AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
                         RandomTransitionGenerator generator = new RandomTransitionGenerator(3000, interpolator);
                         kenBurnsView.setTransitionGenerator(generator);

                         kenBurnsView.setOnClickListener(new View.OnClickListener() {
                             @Override
                             public void onClick(View v) {
                                 if (moving) {
                                     moving = false;
                                     kenBurnsView.pause();
                                 } else {
                                     moving = true;
                                     kenBurnsView.resume();
                                 }
                             }
                         });
                     }
                 });
            }
        });

        thread.start();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MenuActionService.recherche(menu, this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    protected abstract void setUpRecycler();

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        toolbar=findViewById(R.id.toolbar_cycle);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }
}
