package ci.objis.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flaviofaria.kenburnsview.KenBurnsView;
import com.flaviofaria.kenburnsview.RandomTransitionGenerator;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.service.impl.MenuActionService;

public abstract class BaseListAllActivity extends BaseActivity {

    //les proprietes
    @BindView(R.id.listall_tablayout)
    protected TabLayout tabLayout;

    @BindView(R.id.listall_viewpager)
    protected ViewPager viewPager;

    @BindView(R.id.nbqtethismonth)
    protected TextView txtNbQteThisMonth;

    @BindView(R.id.nbqtethisyear)
    protected TextView txtNbQteThisYear;

    @BindView(R.id.nav_headeractivity)
    protected CardView navHeader;

    @BindView(R.id.linear_headeractivity)
    protected RelativeLayout linHeader;

    @BindView(R.id.burnview)
    protected KenBurnsView kenBurnsView;

    private Boolean moving = true;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AccelerateDecelerateInterpolator interpolator = new AccelerateDecelerateInterpolator();
        RandomTransitionGenerator generator = new RandomTransitionGenerator(3000, interpolator);
        kenBurnsView.setTransitionGenerator(generator);

        kenBurnsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (moving) {
                    moving = false;
                    kenBurnsView.pause();
                } else {
                    moving = true;
                    kenBurnsView.resume();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuActionService.recherche(menu, this);

        return true;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }
}
