package ci.objis.ui.base;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.crowdfire.cfalertdialog.CFAlertDialog;

import butterknife.ButterKnife;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.service.ICompteUserService;
import ci.objis.service.impl.CompteUserService;
import ci.objis.service.impl.MenuActionService;

public abstract class BaseActivity extends AppCompatActivity {

    //les proprietes
    protected Toolbar toolbar;
    protected TextView txtFirstTitleToolbar;
    protected TextView txtSdeTitleToolbar;
    protected ICompteUserService compteUserService;
    protected Dialog dialog;
    protected SharedPreferences sharedPreferences;
    protected CFAlertDialog.Builder builder;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getFragmentLayout());

        compteUserService = new CompteUserService(this, this);

        ButterKnife.bind(this);

        configuerToolbar();

        sharedPreferences = getSharedPreferences(StartUp.PREF, MODE_PRIVATE);

    }

    protected abstract int getFragmentLayout();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MenuActionService.standart(menu, this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_closeapp:
                compteUserService.logoutUser();
        }
        return super.onOptionsItemSelected(item);
    }

    protected void configuerToolbar() {
        toolbar = findViewById(R.id.toolBar);
        txtFirstTitleToolbar = findViewById(R.id.txt_titlefirst_toolbar);
        txtSdeTitleToolbar = findViewById(R.id.txt_titlesecond_toolbar);
        setSupportActionBar(toolbar);
    }

    //dialog pour notifier une erreur pendant l'operation
    protected void dialogErrorProgress(){
        // Create Alert using Builder
        CFAlertDialog.Builder builder = new CFAlertDialog.Builder(this)
                .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                .setTitle(R.string.Oups)
                .setMessage(getString(R.string.Erreur_inattendue_survenue_durant_loperation))
                .addButton(getString(R.string.CONTINUER), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                    //
                    dialog.dismiss();
                });

        builder.show();
    }
}
