package ci.objis.ui.perte;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.crowdfire.cfalertdialog.CFAlertDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.Cycle;
import ci.objis.model.Perte;
import ci.objis.service.ICycleService;
import ci.objis.service.IPerteService;
import ci.objis.service.impl.CycleService;
import ci.objis.service.impl.PerteService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.cycle.SuiviCycleActivity;

public class AddPerteCycleActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = AddPerteCycleActivity.class.getSimpleName();

    //les proprietes de l'entete
    @BindView(R.id.addperte_edt_nombreperte)
    EditText edtNombrePerte;

    @BindView(R.id.formulaire_dateoublie)
    DatePicker dateOubliePicker;

    @BindView(R.id.lnformulaire_dateoublie)
    LinearLayout lnDateOublie;

    private IPerteService perteService;

    private int jourSelect;
    private Cycle cycleSelect;
    private Perte perte;

    private ICycleService cycleService;

    private int requestCode;

    private Date dateOublie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);

        perteService = new PerteService();
        cycleService = new CycleService();

        cycleSelect = (Cycle) getIntent().getSerializableExtra(StartUp.CYCLESELECT);
        requestCode = getIntent().getIntExtra(StartUp.OPTIONSELECT, 0);

        switch (requestCode) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                Log.i(TAG, "passage dans option nouveau ");
                jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);
                updateUIForFormul(SuiviCycleActivity.OPTION_NOUVEAU);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                Log.i(TAG, "passage dans option oublie");
                updateUIForFormul(SuiviCycleActivity.OPTION_OUBLIE);
                break;
            default:
                break;
        }

        //recuperation du jour en cours
        // afficher l'etat de la progress bar
        //recuperer le nombre de jours restants
        //pourcentage actuel
        int jrEnCours = cycleService.getJourEnCours(cycleSelect);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_perte_add_cycle;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();
        //traitement lié à la toolbar
        setSupportActionBar(toolbar);
        txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
        txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.Perte)));

        //bouton retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    @OnClick(R.id.btngroup_btnannuler)
    void clickerSurBtnAnnuler() {
        finish();
    }

    //methode permettant d'enregistrer un aliment
    @OnClick(R.id.btngroup_btnvalider)
    void clickerSurBtnValider(View view) {
        if (contrainteEnregis()) {

            perte = new Perte();

            perte.setQuantite(Integer.valueOf(edtNombrePerte.getText().toString()));
            perte.setCycle(cycleService.readOneByIdentif(cycleSelect.getIdentif()));

            Perte perteACreer = null;

            if (SuiviCycleActivity.OPTION_NOUVEAU == requestCode) {
                perteACreer = perteService.create(perte);
            } else {
                int day = dateOubliePicker.getDayOfMonth();
                int month = dateOubliePicker.getMonth();
                int year = dateOubliePicker.getYear();
                try {
                    dateOublie = new SimpleDateFormat("dd-MM-yyyy").parse(String.format("%s-%s-%s", day, month, year));
                    perte.setLastModifiedDate(dateOublie);
                    perteACreer = perteService.createOublie(perte);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (perteACreer != null) {
                // Create Alert using Builder
                builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                        .setTitle(getString(R.string.Felicitations))
                        .setMessage(R.string.Nouvelle_perte_enregistree)
                        .addButton(getString(R.string.CONTINUER), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });
                // Show the alert
                CFAlertDialog alertDialog = builder.show();
                alertDialog.setOnDismissListener(dialog -> {
                    finish();
                });
            } else {
                //appel le dialog pour notifier une erreur pendant le traitement
                dialogErrorProgress();
            }
        }

    }

    //methode permettant de gerer les contraintes de selection
    private Boolean contrainteEnregis() {
        if (TextUtils.isEmpty(edtNombrePerte.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_la_quantite_des_pertes)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });
            builder.show();
            return false;
        } else {

            Integer nbPerte = Integer.valueOf(edtNombrePerte.getText().toString());

            if (cycleService.getNbBeterestant(cycleSelect) - nbPerte < 0) {
                // Create Alert using Builder
                builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                        .setTitle(getString(R.string.Oups))
                        .setMessage(R.string.Nombre_incorrect)
                        .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });
                builder.show();
                return false;
            }
        }
        return true;
    }

    //----------------------------------
    // UI
    //-------------------------------------

    private void updateUIForFormul(int option) {
        switch (option) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.perte)));
                lnDateOublie.setVisibility(View.GONE);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.perte_oublie)));
                lnDateOublie.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }
}
