package ci.objis.ui.perte;

import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.view.View;

import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.adapter.tabs.TabsPerteAdapter;
import ci.objis.service.IPerteService;
import ci.objis.service.impl.PerteService;
import ci.objis.ui.base.BaseListAllActivity;

public class MesPertesActivity extends BaseListAllActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        tabLayout.addTab(tabLayout.newTab().setText(R.string.Liste));
                        tabLayout.addTab(tabLayout.newTab().setText(R.string.Statistiques));
                        tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.grisfonce)));

                        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                        viewPager.setAdapter(new TabsPerteAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null)));
                        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                switch (tab.getPosition()) {
                                    case 0:
                                        navHeader.setVisibility(View.VISIBLE);
                                        break;
                                    case 1:
                                        navHeader.setVisibility(View.GONE);
                                        break;
                                    default:
                                        break;
                                }
                                viewPager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });

                        IPerteService perteService = new PerteService();

                        //affectation des valeurs aux textview de l'entete
                        txtNbQteThisMonth.setText(String.format("%s %s", String.valueOf(perteService.getAllNbRegisterByThisMonth(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))),getString(R.string.ce_mois)));
                        txtNbQteThisYear.setText(String.format("%s %s", String.valueOf(perteService.getAllNbRegisterByThisYear(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))),getString(R.string.cette_annee)));

                    }
                });
            }
        });

        thread.start();
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
        txtSdeTitleToolbar.setText(String.format("%s",getString(R.string.pertes)));
        kenBurnsView.setImageResource(R.drawable.grapheperteimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_perte_all;
    }
}
