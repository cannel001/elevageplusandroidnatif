package ci.objis.ui.perte;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ci.objis.R;
import ci.objis.adapter.recyclerview.all.AllPerteAdapter;
import ci.objis.service.IPerteService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.service.impl.PerteService;

public class ListePerteFragment extends Fragment implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private IPerteService perteService;
    private AllPerteAdapter allPerteAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_perte_liste,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SearchView searchView= MenuActionService.mySearchView;
        searchView.setOnQueryTextListener(this);

        setUpRecycler();
    }

    private void setUpRecycler(){
        perteService=new PerteService();
        recyclerView=getActivity().findViewById(R.id.allperte_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getContext());
        allPerteAdapter=new AllPerteAdapter(perteService.readAll());

        recyclerView.setAdapter(allPerteAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        allPerteAdapter.getFilter().filter(s);

        return false;
    }
}
