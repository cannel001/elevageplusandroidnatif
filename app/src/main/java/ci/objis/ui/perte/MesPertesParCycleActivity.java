package ci.objis.ui.perte;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.PerteParCycleAdapter;
import ci.objis.service.IPerteService;
import ci.objis.service.impl.PerteService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesPertesParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mespertes_recyclerview)
    RecyclerView recyclerView;
    private IPerteService perteService;
    private PerteParCycleAdapter perteParCycleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.drugsimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_pertes_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        perteService=new PerteService();
        RecyclerView.LayoutManager layoutManager=new GridLayoutManager(this,3);
        perteParCycleAdapter=new PerteParCycleAdapter(perteService.readAll());
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(perteParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String s) {
        perteParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
