package ci.objis.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.VideoView;

import com.crashlytics.android.Crashlytics;
import com.example.circulardialog.CDialog;
import com.example.circulardialog.extras.CDConstants;

import ci.objis.R;
import ci.objis.service.IAlimentService;
import ci.objis.service.ICycleService;
import ci.objis.service.IMedicamentService;
import ci.objis.service.IPerteService;
import ci.objis.service.IVenteOeufService;
import ci.objis.service.IVenteOiseauService;
import ci.objis.service.impl.AlimentService;
import ci.objis.service.impl.CycleService;
import ci.objis.service.impl.MedicamentService;
import ci.objis.service.impl.PerteService;
import ci.objis.service.impl.VenteOeufService;
import ci.objis.service.impl.VenteOiseauService;
import ci.objis.ui.aliment.MesAlimentActivity;
import ci.objis.ui.client.AllClientActivity;
import ci.objis.ui.cycle.MesCyclesActivity;
import ci.objis.ui.medicament.MesMedicamentActivity;
import ci.objis.ui.perte.MesPertesActivity;
import ci.objis.ui.conseil.ConseilAllActivity;
import ci.objis.ui.venteoeuf.MesVentesOeufActivity;
import ci.objis.ui.venteoiseau.MesVentesOiseauActivity;
import io.fabric.sdk.android.Fabric;

import static maes.tech.intentanim.CustomIntent.customType;

public class AccueilFragment extends Fragment {

    private MainViewModel mViewModel;

    private LinearLayout linl_mesacti_accuei,
            linl_mesclient_accuei,
            linl_monconseiller_accuei,
            linl_venteoeuf_accuei,
            linl_mesmedoc_accuei,
            linl_mesventeoiseau_accuei,
            linl_mesperte_accuei,
            linl_mesaliment_accuei;


    //les proprietes
    private static final String TAG = AccueilFragment.class.getSimpleName();

    VideoView videoView;

    private ICycleService cycleService;

    private View view;

    public static AccueilFragment newInstance() {
        return new AccueilFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.accueil_fragment, container, false);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        // TODO: Use the ViewModel

        cycleService = new CycleService();

        //
        linl_mesacti_accuei = getActivity().findViewById(R.id.linl_mesacti_accuei);
        linl_mesclient_accuei = getActivity().findViewById(R.id.linl_mesclient_accuei);
        linl_monconseiller_accuei = getActivity().findViewById(R.id.linl_monconseiller_accuei);
        linl_venteoeuf_accuei = getActivity().findViewById(R.id.linl_venteoeuf_accuei);
        linl_mesmedoc_accuei = getActivity().findViewById(R.id.linl_mesmedoc_accuei);
        linl_mesventeoiseau_accuei = getActivity().findViewById(R.id.linl_mesventeoiseau_accuei);
        linl_mesperte_accuei = getActivity().findViewById(R.id.linl_mesperte_accuei);
        linl_mesaliment_accuei = getActivity().findViewById(R.id.linl_mesaliment_accuei);

        //lancement de la video
        Uri uri = Uri.parse("android.resource://" + getContext().getPackageName() + "/" + R.raw.chickenmovie);
        videoView = getActivity().findViewById(R.id.videoViewaccueil);
        videoView.setVideoURI(uri);
        videoView.start();

        //
        clickerSurMesActivites();
        clickerSurMesClients();
        clickerSurMonConseiller();
        clickerSurMesVenteOeuf();
        clickerSurMesMedoc();
        clickerSurMesVentesOiseau();
        clickerSurMesPertes();
        clickerSurMesAliments();

    }


    //methode permettant d'afficher mes activites
    void clickerSurMesActivites() {
        linl_mesacti_accuei.setOnClickListener(v -> {
            if (cycleService.readAll().isEmpty()) {
                new CDialog(getContext()).createAlert(getString(R.string.Vous_navez_pas_dactivites_en_cours),
                        CDConstants.WARNING,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                        .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                        .setDuration(3000)   // in milliseconds
                        .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                        .show();
            } else {
                Intent intent = new Intent(getActivity(), MesCyclesActivity.class);
                startActivity(intent);
            }
        });
    }

    //methode permettant d'afficher mes clients
    void clickerSurMesClients() {
        linl_mesclient_accuei.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), AllClientActivity.class);
            startActivity(intent);
        });
    }

    //methode permettant d'afficher mon conseiller
    void clickerSurMonConseiller() {
        linl_monconseiller_accuei.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ConseilAllActivity.class);
            startActivity(intent);
        });
    }

    /**
     * methode permettant d'afficher la liste des  ventes oeufs
     */
    void clickerSurMesVenteOeuf() {
        linl_venteoeuf_accuei.setOnClickListener(v -> {
            IVenteOeufService venteOeufService = new VenteOeufService();
            if (venteOeufService.readAll().isEmpty()) {
                new CDialog(getContext()).createAlert(getString(R.string.La_liste_des_ventes_doeufs_est_vide),
                        CDConstants.WARNING,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                        .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                        .setDuration(3000)   // in milliseconds
                        .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                        .show();
            } else {
                Intent intent = new Intent(getActivity(), MesVentesOeufActivity.class);
                startActivity(intent);
                customType(getContext(),"bottom-to-up");
            }
        });
    }

    /**
     * methode permettant d'afficher la liste des medocs
     */
    void clickerSurMesMedoc() {
        linl_mesmedoc_accuei.setOnClickListener(v -> {
            IMedicamentService medicamentService = new MedicamentService();
            if (medicamentService.readAll().isEmpty()) {
                new CDialog(getContext()).createAlert(getString(R.string.La_liste_des_medicaments_est_vide),
                        CDConstants.WARNING,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                        .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                        .setDuration(3000)   // in milliseconds
                        .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                        .show();
            } else {
                Intent intent = new Intent(getActivity(), MesMedicamentActivity.class);
                startActivity(intent);
                customType(getContext(),"bottom-to-up");
            }
        });
    }

    /**
     * methode permettant d'afficher la liste des ventes
     */
    void clickerSurMesVentesOiseau() {
        linl_mesventeoiseau_accuei.setOnClickListener(v -> {
            IVenteOiseauService venteOiseauService = new VenteOiseauService();
            if (venteOiseauService.readAll().isEmpty()) {
                new CDialog(getContext()).createAlert(getString(R.string.La_liste_des_ventes_doiseaux_est_vide),
                        CDConstants.WARNING,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                        .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                        .setDuration(3000)   // in milliseconds
                        .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                        .show();
            } else {
                Intent intent = new Intent(getActivity(), MesVentesOiseauActivity.class);
                startActivity(intent);
                customType(getContext(),"bottom-to-up");
            }
        });
    }

    /**
     * methode permettant d'afficher la liste des pertes
     */
    void clickerSurMesPertes() {
        linl_mesperte_accuei.setOnClickListener(v -> {
            IPerteService perteService = new PerteService();
            if (perteService.readAll().isEmpty()) {
                new CDialog(getContext()).createAlert(getString(R.string.La_liste_des_pertes_est_vide),
                        CDConstants.WARNING,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                        .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                        .setDuration(3000)   // in milliseconds
                        .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                        .show();
            } else {
                Intent intent = new Intent(getActivity(), MesPertesActivity.class);
                startActivity(intent);
                customType(getContext(),"bottom-to-up");
            }
        });
    }

    /**
     * methode permettant d'afficher la liste des aliments
     */
    void clickerSurMesAliments() {
        linl_mesaliment_accuei.setOnClickListener(v -> {
            IAlimentService alimentService = new AlimentService();
            if (alimentService.readAll().isEmpty()) {
                new CDialog(getContext()).createAlert(getString(R.string.La_liste_des_aliments_est_vide),
                        CDConstants.WARNING,   // Type of dialog
                        CDConstants.LARGE)    //  size of dialog
                        .setAnimation(CDConstants.SCALE_FROM_BOTTOM_TO_TOP)     //  Animation for enter/exit
                        .setDuration(3000)   // in milliseconds
                        .setTextSize(CDConstants.NORMAL_TEXT_SIZE)  // CDConstants.LARGE_TEXT_SIZE, CDConstants.NORMAL_TEXT_SIZE
                        .show();
            } else {
                Intent intent = new Intent(getActivity(), MesAlimentActivity.class);
                startActivity(intent);
                customType(getContext(),"bottom-to-up");
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        videoView.start();
    }
}
