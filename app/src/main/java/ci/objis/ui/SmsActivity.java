package ci.objis.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import ci.objis.R;
import ci.objis.ui.base.BaseActivity;

public class SmsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_sms;
    }
}
