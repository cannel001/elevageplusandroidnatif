package ci.objis.ui.eau;

import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.Cycle;
import ci.objis.model.Eau;
import ci.objis.service.ICycleService;
import ci.objis.service.IEauService;
import ci.objis.service.impl.CycleService;
import ci.objis.service.impl.EauService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.cycle.SuiviCycleActivity;

public class AddEauCycleActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = AddEauCycleActivity.class.getSimpleName();

    @BindView(R.id.entetecycleencours_nbjourestant)
    TextView txtNbJourRestant;

    @BindView(R.id.entetecycleencours_percentnbjr)
    TextView txtPourcentag;

    @BindView(R.id.entetecycleencours_progressBar)
    ProgressBar progressBarNbJour;

    @BindView(R.id.addeaucycle_edt_quantite)
    EditText edtEauQuantite;

    @BindView(R.id.formulaire_dateoublie)
    DatePicker dateOubliePicker;

    @BindView(R.id.lnformulaire_dateoublie)
    LinearLayout lnDateOublie;

    private IEauService eauService;

    private int jourSelect;
    private Cycle cycleSelect;
    private Eau eau;

    private int requestCode;
    private Date dateOublie;

    private ICycleService cycleService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);

        eauService = new EauService();
        cycleService = new CycleService();

        cycleSelect = (Cycle) getIntent().getSerializableExtra(StartUp.CYCLESELECT);

        requestCode=getIntent().getIntExtra(StartUp.OPTIONSELECT,0);

        switch (requestCode) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);
                updateUIForFormul(SuiviCycleActivity.OPTION_NOUVEAU);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                updateUIForFormul(SuiviCycleActivity.OPTION_OUBLIE);
                break;
            default:
                break;
        }

        //recuperation du jour en cours
        // afficher l'etat de la progress bar
        //recuperer le nombre de jours restants
        //pourcentage actuel
        int jrEnCours = cycleService.getJourEnCours(cycleSelect);

        progressBarNbJour.setMax(cycleSelect.getDureeCycle());

        if (jrEnCours <= cycleSelect.getDureeCycle()) {
            txtNbJourRestant.setText(String.format("%s %s",cycleService.getNbJrRestant(jrEnCours, cycleSelect),getString(R.string.jours_restants)));
            progressBarNbJour.setProgress(jrEnCours);
            txtPourcentag.setText(String.valueOf(cycleService.getPourcentagActuel(jrEnCours, cycleSelect) + "%"));
        } else {
            progressBarNbJour.setProgress(cycleSelect.getDureeCycle());
            txtNbJourRestant.setText(String.valueOf(0));
            txtPourcentag.setText("100%");
        }
    }

    //----------------------------------
    // UI
    //-------------------------------------

    private void updateUIForFormul(int option) {
        switch (option) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.eau)));
                lnDateOublie.setVisibility(View.GONE);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.eau_oublie)));
                lnDateOublie.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_eau_add_cycle;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        //traitement lié à la toolbar
        setSupportActionBar(toolbar);
        txtFirstTitleToolbar.setText(String.format("%S ",getString(R.string.Cycle)));
        txtSdeTitleToolbar.setText(String.format(" %s",getString(R.string.En_cours)));

        //bouton retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    //methode permettant d'enregistrer un aliment
    @OnClick(R.id.btngroup_btnvalider)
    void clickerSurBtnValider(View view) {
        if (contrainteEnregis()) {

            eau = new Eau();

            eau.setQuantite(Float.valueOf(edtEauQuantite.getText().toString()));
            eau.setCycle(cycleService.readOneByIdentif(cycleSelect.getIdentif()));

            if (eauService.create(eau) != null) {
                Snackbar.make(view, R.string.Enregistrement_effectue_avec_succes, Snackbar.LENGTH_LONG).show();
                finish();
            }
        }

    }

    //methode permettant d'enregistrer un aliment
    @OnClick(R.id.btngroup_btnannuler)
    void clickerSurBtnAnnuler(View view) {
        finish();
    }

    //methode permettant de gerer les contraintes de selection
    private Boolean contrainteEnregis() {
        if (TextUtils.isEmpty(edtEauQuantite.getText().toString())) {
            edtEauQuantite.setError(getString(R.string.Veuillez_entrer_la_quantite));
            edtEauQuantite.requestFocus();
            return false;
        }

        return true;
    }
}
