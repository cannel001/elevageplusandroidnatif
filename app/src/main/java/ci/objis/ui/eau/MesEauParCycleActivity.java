package ci.objis.ui.eau;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.EauParCycleAdapter;
import ci.objis.service.IEauService;
import ci.objis.service.impl.EauService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesEauParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.meseau_recyclerview)
    RecyclerView recyclerView;
    private IEauService eauService;
    private EauParCycleAdapter eauParCycleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.mipmap.backgrdfdpouletafri);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_eau_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        eauService=new EauService();
        eauParCycleAdapter=new EauParCycleAdapter(eauService.readAll());
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager=new GridLayoutManager(this,3);

        recyclerView.setAdapter(eauParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String s) {
        eauParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
