package ci.objis.ui.temperature;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.TemperatureParCycleAdapter;
import ci.objis.service.ITemperatureService;
import ci.objis.service.impl.TemperatureService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesTemperatureParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mestemperatures_recyclerview)
    RecyclerView recyclerView;
    private ITemperatureService temperatureService;
    private TemperatureParCycleAdapter temperatureParCycleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.temperatureimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_temperature_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        temperatureService=new TemperatureService();
        temperatureParCycleAdapter=new TemperatureParCycleAdapter(temperatureService.readAll());
        RecyclerView.LayoutManager layoutManager= new GridLayoutManager(this,3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(temperatureParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String s) {
        temperatureParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
