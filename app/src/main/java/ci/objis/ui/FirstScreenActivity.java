package ci.objis.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.compteuser.LoginActivity;
import ci.objis.ui.compteuser.RegisterActivity;

public class FirstScreenActivity extends BaseActivity {

    private int[] sampleImages = {R.mipmap.backgroundpoulet1,R.mipmap.backgroundvolailler,R.mipmap.backgroundboeuf};

    private Intent intent;

    @BindView(R.id.firstscreen_carouselView)
    CarouselView carouselView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);

        ImageListener imageListener = (int position, ImageView imageView)-> {
                imageView.setImageResource(sampleImages[position]);
        };
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_first_screen;
    }

    ImageListener imageListener = (int position, ImageView imageView)-> {
            imageView.setImageResource(sampleImages[position]);
    };

    @OnClick(R.id.firstscreen_connect)
    void clickerSurBtnConnect(){
        intent=new Intent(FirstScreenActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.firstscreen_inscript)
    void clicerSurBtnIncript(){
        intent=new Intent(FirstScreenActivity.this, RegisterActivity.class);
        startActivity(intent);
    }
}
