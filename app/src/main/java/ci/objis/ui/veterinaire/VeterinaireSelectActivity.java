package ci.objis.ui.veterinaire;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jackandphantom.circularimageview.RoundedImage;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import com.mapbox.mapboxsdk.maps.Style;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.Veterinaire;
import ci.objis.service.ICompteUserService;
import ci.objis.service.impl.CompteUserService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.aliment.MesAlimentActivity;
import ci.objis.ui.base.BaseActivity;

public class VeterinaireSelectActivity extends BaseActivity {

    //les proprietes
    private final static String TAG = VeterinaireSelectActivity.class.getSimpleName();

    @BindView(R.id.veterinaireselect_mapView)
    MapView mapView;

    @BindView(R.id.veterinaireselect_nomprenom)
    TextView txtNomPrenom;

    @BindView(R.id.veterinaireselect_imageveterinaire)
    RoundedImage imgVeterinaire;

    @BindView(R.id.veterinaireselect_mapnondispo)
    LinearLayout lnMapNoDispo;

    private Veterinaire veterinaire;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Mapbox.getInstance(this, getString(R.string.acces_token));

        compteUserService = new CompteUserService(this, VeterinaireSelectActivity.this);

        mapView.onCreate(savedInstanceState);

        //recuperation du veterinaire
        veterinaire = (Veterinaire) getIntent().getSerializableExtra(StartUp.VETERINAIRESELECT);

        if (veterinaire.getLatitude() != null && veterinaire.getLongitude() != null) {

            lnMapNoDispo.setVisibility(View.GONE);

            final Double longitude = Double.valueOf(veterinaire.getLongitude());
            final Double latitude = Double.valueOf(veterinaire.getLatitude());
            final LatLng latLng = new LatLng(latitude, longitude);
            final LatLngBounds latLngBounds = new LatLngBounds.Builder()
                    .include(latLng)
                    .include(latLng)
                    .build();

            mapView.getMapAsync(mapboxMap -> {

                mapboxMap.setLatLngBoundsForCameraTarget(latLngBounds);

                mapboxMap.setStyle(Style.MAPBOX_STREETS, style -> {

                    style.addImage("marker-icon-id",
                            BitmapFactory.decodeResource(VeterinaireSelectActivity.this.getResources(), R.drawable.mapbox_marker_icon_default));

                    GeoJsonSource geoJsonSource = new GeoJsonSource("source-id",
                            Feature.fromGeometry(
                                    Point.fromLngLat(longitude, latitude)
                            ));

                    style.addSource(geoJsonSource);

                    SymbolLayer symbolLayer = new SymbolLayer("layer-id", "source-id");
                    symbolLayer.withProperties(
                            PropertyFactory.iconImage("marker-icon-id"));

                    style.addLayer(symbolLayer);

                });
            });
        } else {
            lnMapNoDispo.setVisibility(View.VISIBLE);
            mapView.setVisibility(View.GONE);
        }

        //appeler la methode permettant de charger les infos du veterinaire
        chargerInfoVeterinaire();

    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mon)));
        txtSdeTitleToolbar.setText(getString(R.string.veterinaire));

        toolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_veterinaire_select;
    }


    //ajouter mapBox aux differentes lethodes du cycle de vie
    @Override
    protected void onStart() {
        super.onStart();
        mapView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mapView.onStop();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
        mapView.onSaveInstanceState(outState);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MenuActionService.message(menu, VeterinaireSelectActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_closeapp:
                compteUserService.logoutUser();
        }
        return super.onOptionsItemSelected(item);
    }


    //methode permettant d'appeler le veterinaire
    @OnClick(R.id.veterinaireselect_lnCall)
    void clickerSurLnCall() {
        Uri telVeterinaire = Uri.parse("tel:" + veterinaire.getTel());
        Intent intent = new Intent(Intent.ACTION_DIAL, telVeterinaire);
        startActivity(intent);
    }

    //charger les infos du veterinaire
    void chargerInfoVeterinaire() {
        txtNomPrenom.setText(veterinaire.getNom());
        if (!"".equals(veterinaire.getPrenom())) {
            txtNomPrenom.setText(String.format("%s %s", txtNomPrenom.getText(), veterinaire.getPrenom()));
        }

        if (!TextUtils.isEmpty(veterinaire.getLienPhoto())) {
            //ajout de l'image avec picasso
            Picasso.get()
                    .load(veterinaire.getLienPhoto())
                    .placeholder(R.drawable.avatardefault)
                    .error(R.drawable.avatardefault)
                    .fit()
                    .into(imgVeterinaire);
        }
    }


}
