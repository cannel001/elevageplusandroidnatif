package ci.objis.ui.veterinaire;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import ci.objis.R;
import ci.objis.api.VeterinaireRestDao;

public class AllVeterinaireFragment extends Fragment {

    //les proprietes
    private VeterinaireRestDao veterinaireRestDao;

    private RecyclerView recyclerView;
    private LinearLayout lnLayoutProgres;
    private ProgressBar progressBar;
    private TextView txtMessage;
    private ImageView imageViewReload;

    private LinearLayout lnLayoutListeVide;
    private LinearLayout lnmessage;

    public static AllVeterinaireFragment newInstance() {
        return new AllVeterinaireFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_veterinaire_all, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //matching
        recyclerView = getActivity().findViewById(R.id.veterinaireall_recyclerview);
        lnLayoutProgres = getActivity().findViewById(R.id.veterinaireall_lnprogress);
        progressBar = getActivity().findViewById(R.id.veterinaireall_progress);
        txtMessage = getActivity().findViewById(R.id.veterinaireall_text);
        imageViewReload = getActivity().findViewById(R.id.veterinaireall_reload);
        lnLayoutListeVide=getActivity().findViewById(R.id.veterinaireall_listevide);
        lnmessage=getActivity().findViewById(R.id.veterinaireall_lnmessage);

        recyclerView.setVisibility(View.GONE);

        veterinaireRestDao = new VeterinaireRestDao(recyclerView, getContext(), lnLayoutProgres, imageViewReload, txtMessage, progressBar,lnLayoutListeVide,lnmessage);
        veterinaireRestDao.getAllBySpecialiste("VOLAILLE");

        //appel des methodes
        clickerSurImageReload();
    }

    //methode permettant de relancer le chargement
    void clickerSurImageReload() {
        imageViewReload.setOnClickListener(v -> {
            veterinaireRestDao.getAllBySpecialiste("VOLAILLE");
        });
    }

}
