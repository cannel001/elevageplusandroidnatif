package ci.objis.ui.venteoiseau;

import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.os.Bundle;
import android.view.View;

import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.adapter.tabs.TabsVenteOiseauAdapter;
import ci.objis.service.IVenteOiseauService;
import ci.objis.service.impl.VenteOiseauService;
import ci.objis.ui.base.BaseListAllActivity;

public class MesVentesOiseauActivity extends BaseListAllActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.LISTE)));
                        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.STATISTIQUE)));
                        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
                        tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.grisfonce)));

                        TabsVenteOiseauAdapter tabsVenteOiseauAdapter = new TabsVenteOiseauAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null));

                        viewPager.setAdapter(tabsVenteOiseauAdapter);
                        viewPager.setOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                switch (tab.getPosition()) {
                                    case 0:
                                        navHeader.setVisibility(View.VISIBLE);
                                        break;
                                    case 1:
                                        navHeader.setVisibility(View.GONE);
                                        break;
                                    default:
                                        break;
                                }
                                viewPager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });

                        IVenteOiseauService venteOiseauService = new VenteOiseauService();
                        //affectation des valeurs aux textview de l'entete
                        txtNbQteThisMonth.setText(String.format("%s %s", String.valueOf(venteOiseauService.getAllNbRegisterByThisMonth(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))),getString(R.string.ce_mois)));
                        txtNbQteThisYear.setText(String.format("%s %s", String.valueOf(venteOiseauService.getAllNbRegisterByThisYear(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))),getString(R.string.cette_annee)));
                    }
                });
            }
        });

        thread.start();
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
        txtSdeTitleToolbar.setText(getString(R.string.ventes));
        kenBurnsView.setImageResource(R.drawable.ventepouletimage);

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_venteoiseau;
    }

}
