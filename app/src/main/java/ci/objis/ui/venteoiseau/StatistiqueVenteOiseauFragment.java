package ci.objis.ui.venteoiseau;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ci.objis.R;
import ci.objis.config.VariableAnneeConfig;
import ci.objis.service.IVenteOiseauService;
import ci.objis.service.impl.VenteOiseauService;

public class StatistiqueVenteOiseauFragment extends Fragment {

    private IVenteOiseauService venteOiseauService;
    private String typElevage;

    private AnyChartView anyChartView;
    private Pie pie;

    public void setTypElevage(String typElev){
        this.typElevage=typElev;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_venteoiseau_statistique,container,false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //
        anyChartView=getActivity().findViewById(R.id.mesventesoiseau_anychart);
        pie = AnyChart.pie();

        new ChargmentAsynscData().execute("go");

    }

    private class ChargmentAsynscData extends AsyncTask<String,String,List<DataEntry>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            anyChartView.setVisibility(View.GONE);
        }

        @Override
        protected List<DataEntry> doInBackground(String... strings) {
            venteOiseauService = new VenteOiseauService();

            //recuperer le type d'elevage
            Map<String, Object> maMap = venteOiseauService.getStatForYearByElevage(typElevage);

            List<DataEntry> dataEntries = new LinkedList<>();

            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.JANVIER, (Number) maMap.get(VariableAnneeConfig.JANVIER)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.FEVRIER, (Number) maMap.get(VariableAnneeConfig.FEVRIER)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.MARS, (Number) maMap.get(VariableAnneeConfig.MARS)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.AVRIL, (Number) maMap.get(VariableAnneeConfig.AVRIL)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.MAI, (Number) maMap.get(VariableAnneeConfig.MAI)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.JUIN, (Number) maMap.get(VariableAnneeConfig.JUIN)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.JUILLET, (Number) maMap.get(VariableAnneeConfig.JUILLET)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.AOUT, (Number) maMap.get(VariableAnneeConfig.AOUT)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.SEPTEMBRE, (Number) maMap.get(VariableAnneeConfig.SEPTEMBRE)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.OCTOBRE, (Number) maMap.get(VariableAnneeConfig.OCTOBRE)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.NOVEMBRE, (Number) maMap.get(VariableAnneeConfig.NOVEMBRE)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.DECEMBRE, (Number) maMap.get(VariableAnneeConfig.DECEMBRE)));

            return dataEntries;
        }

        @Override
        protected void onPostExecute(List<DataEntry> dataEntries) {
            super.onPostExecute(dataEntries);
            pie.data(dataEntries);
            pie.title(getString(R.string.Ventes_realisees_durant_cette_annee_par_mois));
            pie.animation(true);
            anyChartView.setChart(pie);
            anyChartView.setVisibility(View.VISIBLE);
        }
    }
}
