package ci.objis.ui.venteoiseau;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.VenteOIseauParCycleAdapter;
import ci.objis.service.IVenteOiseauService;
import ci.objis.service.impl.VenteOiseauService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesVenteOiseauParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mesventesoiseau_recyclerview)
    RecyclerView recyclerView;
    private IVenteOiseauService venteOiseauService;
    private VenteOIseauParCycleAdapter venteOIseauParCycleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.ventepouletimage);
    }

    @Override
    protected void setUpRecycler() {
        venteOiseauService = new VenteOiseauService();
        venteOIseauParCycleAdapter = new VenteOIseauParCycleAdapter(venteOiseauService.readAll());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(venteOIseauParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_vente_oiseau_par_cycle;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        venteOIseauParCycleAdapter.getFilter().filter(s);
        return false;
    }

}
