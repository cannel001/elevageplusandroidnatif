package ci.objis.ui.venteoiseau;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ci.objis.R;
import ci.objis.adapter.recyclerview.all.AllVenteOiseauAdapter;
import ci.objis.service.IVenteOiseauService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.service.impl.VenteOiseauService;

public class ListeVenteOiseauFragment extends Fragment implements SearchView.OnQueryTextListener{

    //les proprietes
    private RecyclerView recyclerView;
    private IVenteOiseauService venteOiseauService;
    private AllVenteOiseauAdapter allVenteOiseauAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_venteoiseau_liste, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SearchView searchView = MenuActionService.mySearchView;

        searchView.setOnQueryTextListener(this);

        setUpRecycler();
    }

    private void setUpRecycler() {
        venteOiseauService = new VenteOiseauService();
        recyclerView = getActivity().findViewById(R.id.allventeoiseau_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        allVenteOiseauAdapter = new AllVenteOiseauAdapter(venteOiseauService.readAll());

        recyclerView.setAdapter(allVenteOiseauAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        allVenteOiseauAdapter.getFilter().filter(s);

        return false;
    }
}
