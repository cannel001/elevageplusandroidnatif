package ci.objis.ui.recolteoeuf;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.RecolteOeufParCycleAdapter;
import ci.objis.service.IRecolteOeufService;
import ci.objis.service.impl.RecolteOeufService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesRecolteOeufParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mesrecoltesoeuf_recyclerview)
    RecyclerView recyclerView;
    private IRecolteOeufService recolteOeufService;
    private RecolteOeufParCycleAdapter recolteOeufParCycleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.venteoeufimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_recolte_oeuf_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        recolteOeufService=new RecolteOeufService();
        recolteOeufParCycleAdapter=new RecolteOeufParCycleAdapter(recolteOeufService.readAll());
        RecyclerView.LayoutManager layoutManager=new GridLayoutManager(this,3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(recolteOeufParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String s) {
        recolteOeufParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
