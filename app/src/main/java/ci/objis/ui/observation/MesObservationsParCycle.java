package ci.objis.ui.observation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.ObservationParCycleAdapter;
import ci.objis.service.IObservationService;
import ci.objis.service.impl.ObservationService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesObservationsParCycle extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mesobservations_recyclerview)
    RecyclerView recyclerView;
    private IObservationService observationService;
    private ObservationParCycleAdapter observationParCycleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.observationimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_observations_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        observationService = new ObservationService();
        observationParCycleAdapter = new ObservationParCycleAdapter(observationService.readAll());
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(this, 3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(observationParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String s) {
        observationParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
