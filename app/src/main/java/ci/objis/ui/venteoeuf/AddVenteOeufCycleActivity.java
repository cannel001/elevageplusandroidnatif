package ci.objis.ui.venteoeuf;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.libizo.CustomEditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.Cycle;
import ci.objis.model.VenteOeuf;
import ci.objis.service.ICycleService;
import ci.objis.service.IVenteOeufService;
import ci.objis.service.impl.CompteUserService;
import ci.objis.service.impl.CycleService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.service.impl.VenteOeufService;
import ci.objis.ui.aliment.MesAlimentActivity;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.cycle.SuiviCycleActivity;
import ci.objis.ui.venteoiseau.AddVenteOiseauCycleActivity;

public class AddVenteOeufCycleActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = AddVenteOeufCycleActivity.class.getSimpleName();

    //les proprietes de l'entete
    @BindView(R.id.addventoeuf_edt_nombr)
    CustomEditText edtVenteOeufNombr;

    @BindView(R.id.addventoeuf_edt_moyenpaiment)
    CustomEditText edtVenteOeufMoyenpaiement;

    @BindView(R.id.addventoeuf_edt_montanttt)
    CustomEditText edtVenteOeufMontantTt;

    @BindView(R.id.formulaire_dateoublie)
    DatePicker dateOubliePicker;

    @BindView(R.id.lnformulaire_dateoublie)
    LinearLayout lnDateOublie;

    private int requestCode;
    private Date dateOublie;

    private IVenteOeufService venteOeufService;

    private int jourSelect;
    private Cycle cycleSelect;
    private VenteOeuf venteOeuf;

    private ICycleService cycleService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);

        venteOeufService = new VenteOeufService();
        cycleService = new CycleService();

        cycleSelect = (Cycle) getIntent().getSerializableExtra(StartUp.CYCLESELECT);
        requestCode=getIntent().getIntExtra(StartUp.OPTIONSELECT,0);

        switch (requestCode) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                Log.i(TAG,"passage dans option nouveau ");
                jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);
                updateUIForFormul(SuiviCycleActivity.OPTION_NOUVEAU);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                Log.i(TAG,"passage dans option oublie");
                updateUIForFormul(SuiviCycleActivity.OPTION_OUBLIE);
                break;
            default:
                break;
        }
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_add_vente_oeuf;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        //traitement lié à la toolbar
        setSupportActionBar(toolbar);
        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Nouvelle)));
        txtSdeTitleToolbar.setText(String.format(" %s",getString(R.string.Vente_doeufs)));

        //bouton retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v-> {
                onBackPressed();
        });
    }

    @OnClick(R.id.btngroup_btnannuler)
    void clickerSurBtnAnnuler(){
        finish();
    }

    //methode permettant d'enregistrer un aliment
    @OnClick(R.id.btngroup_btnvalider)
    void clickerSurBtnValider() {
        if (contrainteEnregis()) {

            venteOeuf = new VenteOeuf();

            venteOeuf.setQuantite(Integer.valueOf(edtVenteOeufNombr.getText().toString()));
            venteOeuf.setMoyenPaiement(edtVenteOeufMoyenpaiement.getText().toString());
            venteOeuf.setMontTtVente(Double.valueOf(edtVenteOeufMontantTt.getText().toString()));
            venteOeuf.setCycle(cycleService.readOneByIdentif(cycleSelect.getIdentif()));

            VenteOeuf venteOeufACreer = null;

            if (SuiviCycleActivity.OPTION_NOUVEAU == requestCode) {
                venteOeufACreer = venteOeufService.create(venteOeuf);
            } else {
                int day = dateOubliePicker.getDayOfMonth();
                int month = dateOubliePicker.getMonth();
                int year = dateOubliePicker.getYear();
                try {
                    dateOublie = new SimpleDateFormat("dd-MM-yyyy").parse(String.format("%s-%s-%s", day, month, year));
                    venteOeuf.setLastModifiedDate(dateOublie);
                    venteOeufACreer = venteOeufService.createOublie(venteOeuf);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (venteOeufACreer != null) {
                // Create Alert using Builder
                builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                        .setTitle(getString(R.string.Felicitations))
                        .setMessage(R.string.Nouvelle_vente_doeufs)
                        .addButton(getString(R.string.CONTINUER), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });
                // Show the alert
                CFAlertDialog alertDialog = builder.show();
                alertDialog.setOnDismissListener(dialog -> {
                    finish();
                });
            } else {
                //appel le dialog pour notifier une erreur pendant le traitement
                dialogErrorProgress();
            }
        }
    }

    //methode permettant de gerer les contraintes de selection
    private Boolean contrainteEnregis() {
        if (TextUtils.isEmpty(edtVenteOeufNombr.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_nombre_doeufs_vendus)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });
            builder.show();
            return false;
        }

        if (TextUtils.isEmpty(edtVenteOeufMoyenpaiement.toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_moyen_de_paiement)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });
            builder.show();
            return false;
        }

        if (TextUtils.isEmpty(edtVenteOeufMontantTt.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_montant_total_des_ventes)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });
            builder.show();
            return false;
        }

        return true;
    }

    //----------------------------------
    // UI
    //-------------------------------------

    private void updateUIForFormul(int option) {
        switch (option) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.vente_doeufs)));
                lnDateOublie.setVisibility(View.GONE);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouvelle)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.vente_doeufs_oubliee)));
                lnDateOublie.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }
}
