package ci.objis.ui.venteoeuf;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ci.objis.R;
import ci.objis.adapter.recyclerview.all.AllVenteOeufAdapter;
import ci.objis.service.IVenteOeufService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.service.impl.VenteOeufService;

public class ListeVenteOeufFragment extends Fragment implements SearchView.OnQueryTextListener {

    private RecyclerView recyclerView;
    private IVenteOeufService venteOeufService;
    private AllVenteOeufAdapter allVenteOeufAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_liste_vente_oeuf, container, false);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SearchView searchView= MenuActionService.mySearchView;
        searchView.setOnQueryTextListener(this);

        setUpRecycler();
    }

    private void setUpRecycler(){
        venteOeufService=new VenteOeufService();

        recyclerView=getActivity().findViewById(R.id.allventeoeuf_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager= new LinearLayoutManager(getContext());
        allVenteOeufAdapter=new AllVenteOeufAdapter(venteOeufService.readAll());

        recyclerView.setAdapter(allVenteOeufAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        allVenteOeufAdapter.getFilter().filter(s);

        return false;
    }
}
