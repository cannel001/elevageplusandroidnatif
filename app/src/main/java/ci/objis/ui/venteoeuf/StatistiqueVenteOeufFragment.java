package ci.objis.ui.venteoeuf;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Pie;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import ci.objis.R;
import ci.objis.config.VariableAnneeConfig;
import ci.objis.service.IVenteOeufService;
import ci.objis.service.impl.VenteOeufService;

public class StatistiqueVenteOeufFragment extends Fragment {

    //les proprietes
    private IVenteOeufService venteOeufService;
    private String typElevage;

    private AnyChartView anyChartView;
    private Pie pie;

    public void setTypElevage(String typElev){
        this.typElevage=typElev;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_statistique_vente_oeuf, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        anyChartView=getActivity().findViewById(R.id.mesventeoeuf_stat_chart);
        pie = AnyChart.pie();

        new ChargmentAsynscData().execute("go");

    }

    private class ChargmentAsynscData extends AsyncTask<String,String,List<DataEntry>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            anyChartView.setVisibility(View.GONE);
        }

        @Override
        protected List<DataEntry> doInBackground(String... strings) {
            //
            venteOeufService = new VenteOeufService();

            //recuperer le type d'elevage
            Map<String, Object> maMap = venteOeufService.getStatForYearByElevage(typElevage);

            List<DataEntry> dataEntries = new LinkedList<>();

            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.JANVIER, (Number) maMap.get(VariableAnneeConfig.JANVIER)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.FEVRIER, (Number) maMap.get(VariableAnneeConfig.FEVRIER)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.MARS, (Number) maMap.get(VariableAnneeConfig.MARS)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.AVRIL, (Number) maMap.get(VariableAnneeConfig.AVRIL)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.MAI, (Number) maMap.get(VariableAnneeConfig.MAI)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.JUIN, (Number) maMap.get(VariableAnneeConfig.JUIN)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.JUILLET, (Number) maMap.get(VariableAnneeConfig.JUILLET)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.AOUT, (Number) maMap.get(VariableAnneeConfig.AOUT)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.SEPTEMBRE, (Number) maMap.get(VariableAnneeConfig.SEPTEMBRE)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.OCTOBRE, (Number) maMap.get(VariableAnneeConfig.OCTOBRE)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.NOVEMBRE, (Number) maMap.get(VariableAnneeConfig.NOVEMBRE)));
            dataEntries.add(new ValueDataEntry(VariableAnneeConfig.DECEMBRE, (Number) maMap.get(VariableAnneeConfig.DECEMBRE)));

            return dataEntries;
        }

        @Override
        protected void onPostExecute(List<DataEntry> dataEntries) {
            super.onPostExecute(dataEntries);

            pie.data(dataEntries);

            pie.data(dataEntries);
            pie.title(getString(R.string.Ventes_realisees_durant_cette_annee_par_mois));
            pie.animation(true);
            anyChartView.setChart(pie);

            anyChartView.setVisibility(View.VISIBLE);
        }
    }
}
