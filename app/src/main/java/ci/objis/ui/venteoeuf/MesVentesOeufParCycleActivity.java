package ci.objis.ui.venteoeuf;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.VenteOeufParCycleAdapter;
import ci.objis.service.IVenteOeufService;
import ci.objis.service.impl.VenteOeufService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesVentesOeufParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mesventesoeuf_recyclerview)
    RecyclerView recyclerView;
    private IVenteOeufService venteOeufService;
    private VenteOeufParCycleAdapter venteOeufParCycleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.venteoeufimage);
    }

    @Override
    protected void setUpRecycler() {
        venteOeufService=new VenteOeufService();
        venteOeufParCycleAdapter=new VenteOeufParCycleAdapter(venteOeufService.readAll());
        RecyclerView.LayoutManager layoutManager=new GridLayoutManager(this,3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(venteOeufParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_ventes_oeuf_par_cycle;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        venteOeufParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
