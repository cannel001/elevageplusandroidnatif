package ci.objis.ui.medicament;

import android.content.res.ColorStateList;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.adapter.tabs.TabsMedicamentAdapter;
import ci.objis.service.IMedicamentService;
import ci.objis.service.impl.MedicamentService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.base.BaseListAllActivity;

public class MesMedicamentActivity extends BaseListAllActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //affectation des valeurs aux textview de l'entete
                        IMedicamentService medicamentService = new MedicamentService();
                        txtNbQteThisMonth.setText(String.format("%s %s", String.valueOf(medicamentService.getAllNbRegisterByThisMonth(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))), getString(R.string.ce_mois)));
                        txtNbQteThisYear.setText(String.format("%s %s", String.valueOf(medicamentService.getAllNbRegisterByThisYear(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null))), getString(R.string.cette_annee)));

                        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.LISTE)));
                        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.STATISTIQUE)));
                        tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.grisfonce)));
                        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

                        viewPager.setAdapter(new TabsMedicamentAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null)));
                        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                switch (tab.getPosition()) {
                                    case 0:
                                        navHeader.setVisibility(View.VISIBLE);
                                        break;
                                    case 1:
                                        navHeader.setVisibility(View.GONE);
                                        break;
                                    default:
                                        break;
                                }
                                viewPager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });
                    }
                });
            }
        });

        thread.start();
    }


    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();
        txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Mes)));
        txtSdeTitleToolbar.setText(getString(R.string.medicaments));

        kenBurnsView.setImageResource(R.drawable.drugsimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_medicament_all;
    }
}
