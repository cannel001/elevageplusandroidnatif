package ci.objis.ui.medicament;

import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.libizo.CustomEditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.model.Cycle;
import ci.objis.model.Medicament;
import ci.objis.service.ICycleService;
import ci.objis.service.IMedicamentService;
import ci.objis.service.impl.CycleService;
import ci.objis.service.impl.MedicamentService;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.cycle.SuiviCycleActivity;

public class AddMedicamentCycleActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = AddMedicamentCycleActivity.class.getSimpleName();

    //les proprietes de l'entete
    @BindView(R.id.addmedicamentcycle_edt_nom)
    CustomEditText edtNomMedoc;

    @BindView(R.id.addmedicamentcycle_edt_qte)
    CustomEditText edtQuantiteMedoc;

    @BindView(R.id.formulaire_dateoublie)
    DatePicker dateOubliePicker;

    @BindView(R.id.lnformulaire_dateoublie)
    LinearLayout lnDateOublie;

    private Date dateOublie;

    private IMedicamentService medicamentService;

    private int jourSelect;
    private Cycle cycleSelect;
    private Medicament medicament;

    private ICycleService cycleService;

    private int requestCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        medicamentService = new MedicamentService();
        cycleService = new CycleService();

        requestCode=getIntent().getIntExtra(StartUp.OPTIONSELECT,0);
        cycleSelect = (Cycle) getIntent().getSerializableExtra(StartUp.CYCLESELECT);

        switch (requestCode) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                Log.i(TAG,"passage dans option nouveau ");
                jourSelect = getIntent().getIntExtra(StartUp.JOURCYCLESELECT, 0);
                updateUIForFormul(SuiviCycleActivity.OPTION_NOUVEAU);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                Log.i(TAG,"passage dans option oublie");
                updateUIForFormul(SuiviCycleActivity.OPTION_OUBLIE);
                break;
            default:
                break;
        }

    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();
        //traitement lié à la toolbar
        setSupportActionBar(toolbar);
        txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouveau)));
        txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.medicament)));

        //bouton retour
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_medicament_add_cycle;
    }

    /**
     * methode permettant d'annuler l'enregistrement et revenir à l'activité precedente
     */
    @OnClick(R.id.btngroup_btnannuler)
    void clickerSurBtnAnnuler() {
        finish();
    }

    /**
     * methode permettant d'enregistrer un aliment
     */
    @OnClick(R.id.btngroup_btnvalider)
    void clickerSurBtnValider() {
        if (contrainteEnregis()) {

            medicament = new Medicament();

            medicament.setNomMedoc(edtNomMedoc.getText().toString());
            medicament.setQuantite(Integer.valueOf(edtQuantiteMedoc.getText().toString()));
            medicament.setCycle(cycleService.readOneByIdentif(cycleSelect.getIdentif()));

            Medicament medocACreer=null;

            if(SuiviCycleActivity.OPTION_NOUVEAU==requestCode){
                medocACreer=medicamentService.create(medicament);
            }else{
                int day=dateOubliePicker.getDayOfMonth();
                int month=dateOubliePicker.getMonth();
                int year=dateOubliePicker.getYear();
                try {
                    dateOublie=new SimpleDateFormat("dd-MM-yyyy").parse(String.format("%s-%s-%s",day,month,year));
                    medicament.setLastModifiedDate(dateOublie);
                    medocACreer=medicamentService.createOublie(medicament);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }

            if (medocACreer != null) {
                // Create Alert using Builder
                builder = new CFAlertDialog.Builder(this)
                        .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                        .setTitle(getString(R.string.Felicitations))
                        .setMessage(R.string.Nouveau_medicament_enregistre)
                        .addButton(getString(R.string.CONTINUER), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                            //
                            dialog.dismiss();
                        });

                // Show the alert
                CFAlertDialog alertDialog = builder.show();
                alertDialog.setOnDismissListener(dialog -> {
                    finish();
                });
            } else {
                //appel le dialog pour notifier une erreur pendant le traitement
                dialogErrorProgress();
            }
        }
    }

    /**
     * methode permettant de gerer les contraintes d'enregistrements
     *
     * @return
     */
    private Boolean contrainteEnregis() {
        if (TextUtils.isEmpty(edtNomMedoc.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_nom_sil_vous_plait)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });

            builder.show();

            return false;
        }

        if (TextUtils.isEmpty(edtQuantiteMedoc.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(this)
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(getString(R.string.Veuillez_entrer_la_quantitee))
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });

            builder.show();

            return false;
        }

        return true;
    }

    //----------------------------------
    // UI
    //-------------------------------------

    private void updateUIForFormul(int option) {
        switch (option) {
            case SuiviCycleActivity.OPTION_NOUVEAU:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouveau)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.medicament)));
                lnDateOublie.setVisibility(View.GONE);
                break;
            case SuiviCycleActivity.OPTION_OUBLIE:
                txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Nouveau)));
                txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.medicament_oublie)));
                lnDateOublie.setVisibility(View.VISIBLE);
                break;
            default:
                break;
        }
    }
}
