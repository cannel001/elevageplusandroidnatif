package ci.objis.ui.medicament;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.flaviofaria.kenburnsview.KenBurnsView;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.adapter.recyclerview.parcycle.MedicamentParCycleAdapter;
import ci.objis.service.IMedicamentService;
import ci.objis.service.impl.MedicamentService;
import ci.objis.ui.base.BaseListParCycleActivity;

public class MesmedicamentsParCycleActivity extends BaseListParCycleActivity {
    //les proprietes
    @BindView(R.id.mesmedicaments_recyclerview)
    RecyclerView recyclerView;
    private MedicamentParCycleAdapter medicamentParCycleAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        kenBurnsView.setImageResource(R.drawable.drugsimage);
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mesmedicaments_par_cycle;
    }

    @Override
    protected void setUpRecycler() {
        IMedicamentService medicamentService=new MedicamentService();
        medicamentParCycleAdapter= new MedicamentParCycleAdapter(medicamentService.readAll());
        RecyclerView.LayoutManager layoutManager= new GridLayoutManager(this,3);
        recyclerView.setHasFixedSize(true);

        recyclerView.setAdapter(medicamentParCycleAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onQueryTextChange(String s) {
        medicamentParCycleAdapter.getFilter().filter(s);
        return false;
    }
}
