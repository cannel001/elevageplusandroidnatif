package ci.objis.ui.medicament;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.SearchView;

import ci.objis.R;
import ci.objis.adapter.recyclerview.all.AllMedicamentAdapter;
import ci.objis.service.IMedicamentService;
import ci.objis.service.impl.MedicamentService;
import ci.objis.service.impl.MenuActionService;


public class ListeMedicamentFragment extends Fragment implements SearchView.OnQueryTextListener{
    //les proprietes
    private RecyclerView recyclerView;
    private IMedicamentService medicamentService;
    private AllMedicamentAdapter allMedicamentAdapter;


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SearchView searchView = MenuActionService.mySearchView;
        searchView.setOnQueryTextListener(this);

        medicamentService = new MedicamentService();
        setUpRecyclerView();
    }

    private void setUpRecyclerView() {
        recyclerView = getActivity().findViewById(R.id.allmedicament_recyclerview);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        allMedicamentAdapter = new AllMedicamentAdapter(medicamentService.readAll());

        recyclerView.setAdapter(allMedicamentAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_liste_medicament, container, false);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        allMedicamentAdapter.getFilter().filter(s);
        return false;
    }
}
