package ci.objis.ui.cycle;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;
import java.util.Map;

import butterknife.BindView;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.adapter.recyclerview.CycleEnCoursAdapter;
import ci.objis.dao.impl.PoussinDao;
import ci.objis.service.ICycleService;
import ci.objis.service.impl.CycleService;
import ci.objis.service.impl.MenuActionService;
import ci.objis.ui.base.BaseActivity;

public class MesCyclesActivity extends BaseActivity {

    //les proprietes
    private PoussinDao poussinDao;

    private ICycleService cycleService;

    private List<Map<String, Object>> listMapAllCycl;
    private int nbJourRestant = 0;

    @BindView(R.id.mescycles_recyclerview)
    RecyclerView recyclerView;

    @BindView(R.id.mescycles_nbactivites)
    TextView txtNbActivite;

    @BindView(R.id.mescycles_nbjourestant)
    TextView txtNbJourRestant;

    private CycleEnCoursAdapter cycleEnCoursAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Thread thread=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        cycleService = new CycleService();

                        listMapAllCycl = cycleService.allCyclAndPercentbyLibelleElevage(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null));
                        nbJourRestant = cycleService.getTtNbJrCyclRestantParAnnee(sharedPreferences.getString(StartUp.ELEVAGESELECTIONNE, null));

                        //methode permettant d'appeler le recyclerview
                        setUpRecyclerView();

                        //traitement lié au tableau de bord
                        txtNbActivite.setText(String.format("%s %s", String.valueOf(listMapAllCycl.size()),getString(R.string.activites)));
                        txtNbJourRestant.setText(String.format("%s %s", String.valueOf(nbJourRestant),getString(R.string.jours_restants)));

                    }
                });
            }
        });

        thread.start();

    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_mes_cycles;
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();
        //traitement lié à la toolbar
        txtFirstTitleToolbar.setText(String.format("%s ",getString(R.string.Mes)));
        txtSdeTitleToolbar.setText(String.format(" %s",getString(R.string.activités)));

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }

    //methode permettant de configuer recycler view
    void setUpRecyclerView() {
        cycleEnCoursAdapter = new CycleEnCoursAdapter(this, listMapAllCycl);
        recyclerView.setAdapter(cycleEnCoursAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return MenuActionService.recherche(menu, this);
    }


}
