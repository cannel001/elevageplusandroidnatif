package ci.objis.ui.cycle;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nightonke.boommenu.BoomButtons.BoomButton;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.TextOutsideCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.OnBoomListener;

import java.util.Calendar;

import az.plainpie.PieView;
import butterknife.BindView;
import ci.objis.R;
import ci.objis.StartUp;
import ci.objis.config.TacheConfig;
import ci.objis.model.Cycle;
import ci.objis.service.ICycleService;
import ci.objis.service.impl.CompteUserService;
import ci.objis.service.impl.CycleService;
import ci.objis.ui.aliment.AddAlimentCycleActivity;
import ci.objis.ui.aliment.MesAlimentsParCycleActivity;
import ci.objis.ui.attaquemaladie.AddAttaqMaladieCyclActivity;
import ci.objis.ui.attaquemaladie.MesAttaquesMaladieParCycleActivity;
import ci.objis.ui.base.BaseActivity;
import ci.objis.ui.medicament.AddMedicamentCycleActivity;
import ci.objis.ui.medicament.MesmedicamentsParCycleActivity;
import ci.objis.ui.observation.AddObservationCycleActivity;
import ci.objis.ui.observation.MesObservationsParCycle;
import ci.objis.ui.perte.AddPerteCycleActivity;
import ci.objis.ui.perte.MesPertesParCycleActivity;
import ci.objis.ui.recolteoeuf.AddRecoltOeufCycleActivity;
import ci.objis.ui.recolteoeuf.MesRecolteOeufParCycleActivity;
import ci.objis.ui.temperature.AddTemperatureCycleActivity;
import ci.objis.ui.temperature.MesTemperatureParCycleActivity;
import ci.objis.ui.venteoeuf.AddVenteOeufCycleActivity;
import ci.objis.ui.venteoeuf.MesVentesOeufParCycleActivity;
import ci.objis.ui.venteoiseau.AddVenteOiseauCycleActivity;
import ci.objis.ui.venteoiseau.MesVenteOiseauParCycleActivity;
import devs.mulham.horizontalcalendar.HorizontalCalendar;
import devs.mulham.horizontalcalendar.utils.HorizontalCalendarListener;

public class SuiviCycleActivity extends BaseActivity {

    //les proprietes
    public static final String TAG = SuiviCycleActivity.class.getSimpleName();

    private String typTache;
    private int typOption;

    public final static int OPTION_LISTE = 40;
    public final static int OPTION_NOUVEAU = 41;
    public final static int OPTION_OUBLIE = 42;
    public final static int OPTION_LISTE_OUBLIE = 43;

    //les proprietes de l'entete
    @BindView(R.id.entetecycleencours_nbjourestant)
    TextView txtNbJourRestant;

    @BindView(R.id.entetecycleencours_percentnbjr)
    TextView txtPourcentag;

    @BindView(R.id.entetecycleencours_progressBar)
    ProgressBar progressBarNbJour;

    @BindView(R.id.cycleencours_nbbeterestant)
    TextView txtNbBeteRestant;

    private Cycle cycleSeelect;
    private ICycleService cycleService;

    private int jourSelect;

    private Dialog dialog;

    @BindView(R.id.pieView)
    PieView pieView;

    @BindView(R.id.suivicycle_bmbnew)
    BoomMenuButton bmbMenu;

    @BindView(R.id.suivicycle_bmbHam)
    BoomMenuButton bmbHam;

    private Intent intent;

    private int jrEnCours;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        compteUserService = new CompteUserService(this, this);

        //initialiser la variable du jour selectionné
        jourSelect = 0;

        //instanciation des objets
        cycleService = new CycleService();

        //traitement de l'entete
        cycleSeelect = (Cycle) getIntent().getSerializableExtra(StartUp.CYCLESELECT);

        configuerGraphe();
        configuerBomb();

        updateUI();

        /* starts before 1 month from now */
        Calendar startDate = Calendar.getInstance();
        startDate.add(Calendar.MONTH, -1);

        /* ends after 1 month from now */
        Calendar endDate = Calendar.getInstance();
        endDate.add(Calendar.MONTH, 1);

        HorizontalCalendar horizontalCalendar = new HorizontalCalendar.Builder(SuiviCycleActivity.this, R.id.calendarView)
                .range(startDate, endDate)
                .datesNumberOnScreen(5)
                .build();

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Calendar date, int position) {
                //do something
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    //---------------------------------------------
    // UI
    //----------------------------------------------


    //Mise a jour du nombre de betes
    private void updateNbBete() {
        txtNbBeteRestant.setText((String.format("%s %s %s", cycleService.getNbBeterestant(cycleSeelect), cycleSeelect.getPoussin().getLibelle(), getString(R.string.restants))).toLowerCase());
    }

    //configuer Bomb
    private void configuerBomb() {
        TextOutsideCircleButton.Builder builder;
        HamButton.Builder builderHam;
        //traitement lié au bomb des nouvels enregistrements
        for (int i = 0; i < bmbMenu.getPiecePlaceEnum().pieceNumber(); i++) {
            switch (i) {
                case 0:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.injection02white)
                            .normalText(getString(R.string.medicament))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 1:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.chicken01wfwhite)
                            .normalText(getString(R.string.vente_doiseaux))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 2:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.alimentwhite)
                            .normalText(getString(R.string.aliment))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 3:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.cemeterywfwhite)
                            .normalText(getString(R.string.perte))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 4:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.temperaturewhite)
                            .normalText(getString(R.string.temperature))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 5:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.notewhite)
                            .normalText(getString(R.string.observation))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 6:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.diagramwhite)
                            .normalText(getString(R.string.attaque_maladie))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 7:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.chickeneggwhite)
                            .normalText(getString(R.string.vente_doeufs))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
                case 8:
                    builder = new TextOutsideCircleButton.Builder()
                            .normalImageRes(R.mipmap.shoppingbagwhite)
                            .normalText(getString(R.string.collecte_doeufs))
                            .isRound(false);
                    bmbMenu.addBuilder(builder);
                    break;
            }
        }

        bmbMenu.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {

                jourSelect = cycleService.getJourEnCours(cycleSeelect);
                if (jourSelect <= cycleSeelect.getDureeCycle()) {
                    switch (index) {
                        case 0:
                            typTache = TacheConfig.MEDICAMENT;
                            bmbHam.boom();
                            break;
                        case 1:
                            typTache = TacheConfig.VENTEOISEAU;
                            bmbHam.boom();
                            break;
                        case 2:
                            typTache = TacheConfig.ALIMENT;
                            bmbHam.boom();
                            break;
                        case 3:
                            typTache = TacheConfig.PERTE;
                            bmbHam.boom();
                            break;
                        case 4:
                            typTache = TacheConfig.TEMPERATURE;
                            bmbHam.boom();
                            break;
                        case 5:
                            typTache = TacheConfig.OBSERVATION;
                            bmbHam.boom();
                            break;
                        case 6:
                            typTache = TacheConfig.ATTAQUEMALADIE;
                            bmbHam.boom();
                            break;
                        case 7:
                            typTache = TacheConfig.VENTEOEUF;
                            bmbHam.boom();
                            break;
                        case 8:
                            typTache = TacheConfig.RECOLTEOEUF;
                            bmbHam.boom();
                            break;
                    }
                } else {
                    //Snackbar.make(view, "Desolé, ce cycle est terminé", Snackbar.LENGTH_LONG).show();
                }
            }

            @Override
            public void onBackgroundClick() {}
            @Override
            public void onBoomWillHide() {}
            @Override
            public void onBoomDidHide() {}
            @Override
            public void onBoomWillShow() {}
            @Override
            public void onBoomDidShow() {}
        });

        //traitement lié au bomb ham
        for (int i = 0; i < bmbHam.getPiecePlaceEnum().pieceNumber(); i++) {
            switch (i) {
                case 0:
                    builderHam = new HamButton.Builder()
                            .normalImageRes(R.mipmap.newregister)
                            .imagePadding(new Rect(15,15,15,15))
                            .normalText(getString(R.string.Nouvel_enregistrement));
                    bmbHam.addBuilder(builderHam);
                    break;
                case 1:
                    builderHam = new HamButton.Builder()
                            .normalImageRes(R.mipmap.newforget)
                            .imagePadding(new Rect(15,15,15,15))
                            .normalText(getString(R.string.Enregistrement_oublie));
                    bmbHam.addBuilder(builderHam);
                    break;
                case 2:
                    builderHam = new HamButton.Builder()
                            .normalImageRes(R.mipmap.listregister)
                            .imagePadding(new Rect(15,15,15,15))
                            .normalText(getString(R.string.Liste_des_enregistrements));
                    bmbHam.addBuilder(builderHam);
                    break;
                case 3:
                    builderHam = new HamButton.Builder()
                            .normalImageRes(R.mipmap.listregisterforget)
                            .imagePadding(new Rect(15,15,15,15))
                            .normalText(getString(R.string.Liste_des_oublies));
                    bmbHam.addBuilder(builderHam);
                    break;
            }
        }

        bmbHam.setOnBoomListener(new OnBoomListener() {
            @Override
            public void onClicked(int index, BoomButton boomButton) {
                switch (index) {
                    case 0:
                        typOption = OPTION_NOUVEAU;
                        getTache(typTache, typOption);
                        break;
                    case 1:
                        typOption = OPTION_OUBLIE;
                        getTache(typTache, typOption);
                        break;
                    case 2:
                        typOption = OPTION_LISTE;
                        getTache(typTache, typOption);
                        break;
                    case 3:
                        typOption = OPTION_LISTE_OUBLIE;
                        getTache(typTache, typOption);
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onBackgroundClick() {}
            @Override
            public void onBoomWillHide() {}
            @Override
            public void onBoomDidHide() {}
            @Override
            public void onBoomWillShow() {}
            @Override
            public void onBoomDidShow() {}
        });
    }

    //Update UI
    private void updateUI() {
        updateNbBete();
    }

    private void configuerGraphe() {
        //recuperation du jour en cours
        // afficher l'etat de la progress bar
        //recuperer le nombre de jours restants
        //pourcentage actuel
        jrEnCours = cycleService.getJourEnCours(cycleSeelect);

        progressBarNbJour.setMax(cycleSeelect.getDureeCycle());

        if (jrEnCours <= cycleSeelect.getDureeCycle()) {
            txtNbJourRestant.setText(String.format("%s %s", cycleService.getNbJrRestant(jrEnCours, cycleSeelect), getString(R.string.jours_restants)).toLowerCase());
            progressBarNbJour.setProgress(jrEnCours);
            txtPourcentag.setText(String.format("%s%%", cycleService.getPourcentagActuel(jrEnCours, cycleSeelect)));
        } else {
            progressBarNbJour.setProgress(cycleSeelect.getDureeCycle());
            txtNbJourRestant.setText(String.valueOf(0));
            txtPourcentag.setText("100%");
        }

        pieView.setPercentageBackgroundColor(getResources().getColor(R.color.forestgreen));
        pieView.setInnerText(String.valueOf(jrEnCours));
        pieView.setMaxPercentage((float) cycleSeelect.getDureeCycle());
        pieView.setPercentage((float) jrEnCours);
        // Change the color fill of the background of the widget, by default is transparent
        pieView.setMainBackgroundColor(getResources().getColor(R.color.grisfonce));
        // Change the color of the text of the widget
        pieView.setTextColor(getResources().getColor(R.color.forestgreen));
        // Change the thickness of the percentage bar
        pieView.setPieInnerPadding(50);
        // Update the visibility of the widget text
        pieView.setInnerTextVisibility(View.VISIBLE);
        // Change the text size of the widget
        pieView.setPercentageTextSize(22);
        pieView.setInnerText(String.format("%s n°%s", getString(R.string.Jour), jrEnCours));
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.activity_suivi_cycle;
    }

    //methode permettant d'ouvrir l'intent du nouveau formulaire medicament
    void afficherMedoc(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesmedicamentsParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesmedicamentsParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddMedicamentCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddMedicamentCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire aliment
    void afficherAliment(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesAlimentsParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesAlimentsParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddAlimentCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddAlimentCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire perte
    void afficherPerte(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesPertesParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesPertesParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddPerteCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddPerteCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire vente oiseau
    void afficherVenteOiseau(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesVenteOiseauParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesVenteOiseauParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddVenteOiseauCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddVenteOiseauCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire observation
    void afficherObservation(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesObservationsParCycle.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesObservationsParCycle.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddObservationCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddObservationCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire temperature
    void afficherTemperature(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesTemperatureParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesTemperatureParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddTemperatureCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddTemperatureCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire recolte oeuf
    void afficherRecolteOeuf(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesRecolteOeufParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesRecolteOeufParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddRecoltOeufCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddRecoltOeufCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire vente oeuf
    void afficherVenteOeuf(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesVentesOeufParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesVentesOeufParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddVenteOeufCycleActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddVenteOeufCycleActivity.class);
                break;
            default:
                break;
        }
    }

    //methode permettant d'ouvrir l'intent du formulaire attaque maladie
    void afficherAttaquMaladie(int option) {
        switch (option) {
            case OPTION_LISTE:
                startNewIntent(MesAttaquesMaladieParCycleActivity.class);
                break;
            case OPTION_LISTE_OUBLIE:
                startNewIntent(MesAttaquesMaladieParCycleActivity.class);
                break;
            case OPTION_NOUVEAU:
                startNewIntent(AddAttaqMaladieCyclActivity.class);
                break;
            case OPTION_OUBLIE:
                startNewIntent(AddAttaqMaladieCyclActivity.class);
                break;
            default:
                break;
        }

    }

    //methode permettant d'ouvrir un formulaire
    private void startNewIntent(Class classIntent) {
        intent = new Intent(SuiviCycleActivity.this, classIntent);
        intent.putExtra(StartUp.CYCLESELECT, cycleSeelect);
        intent.putExtra(StartUp.JOURCYCLESELECT, jourSelect);
        intent.putExtra(StartUp.OPTIONSELECT,typOption);
        startActivityForResult(intent, typOption);
    }

    //methode permettant d'appeler une tache
    private void getTache(String tache, int option) {
        switch (tache) {
            case TacheConfig.ALIMENT:
                afficherAliment(option);
                break;
            case TacheConfig.ATTAQUEMALADIE:
                afficherAttaquMaladie(option);
                break;
            case TacheConfig.EAU:
                break;
            case TacheConfig.MEDICAMENT:
                afficherMedoc(option);
                break;
            case TacheConfig.OBSERVATION:
                afficherObservation(option);
                break;
            case TacheConfig.PERTE:
                afficherPerte(option);
                break;
            case TacheConfig.RECOLTEOEUF:
                afficherRecolteOeuf(option);
                break;
            case TacheConfig.TEMPERATURE:
                afficherTemperature(option);
                break;
            case TacheConfig.VENTEOEUF:
                afficherVenteOeuf(option);
                break;
            case TacheConfig.VENTEOISEAU:
                afficherVenteOiseau(option);
                break;
            default:
                break;
        }
    }

    @Override
    protected void configuerToolbar() {
        super.configuerToolbar();

        txtFirstTitleToolbar.setText(String.format("%s ", getString(R.string.Cycle)));
        txtSdeTitleToolbar.setText(String.format(" %s", getString(R.string.en_cours)));

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        toolbar.setNavigationOnClickListener(v -> {
            onBackPressed();
        });
    }
}
