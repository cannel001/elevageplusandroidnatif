package ci.objis.ui.cycle;

import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.github.nikartm.button.FitButton;

import java.io.IOException;
import java.util.List;

import ci.objis.R;
import ci.objis.dao.impl.PoussinDao;
import ci.objis.model.Cycle;
import ci.objis.model.Poussin;
import ci.objis.service.ICycleService;
import ci.objis.service.impl.CycleService;
import ci.objis.ui.main.AccueilFragment;
import ci.objis.ui.main.MainViewModel;
import es.dmoral.toasty.Toasty;
import pl.droidsonroids.gif.GifDrawable;

public class NewCycleActivity extends Fragment {

    //les proprietes
    private MainViewModel mainViewModel;

    private ImageView newcycl_chair;
    private ImageView newcycl_ponte;

    private EditText edtNomCycle;
    private EditText edtNbTroupeau;

    private FitButton btnValider;
    private FitButton btnAnnuler;

    private Dialog dialog;
    private String typPoussin;

    private Poussin poussin;
    private PoussinDao poussinDao;
    private ICycleService cycleService;

    private List<Poussin> poussinList;

    private CFAlertDialog.Builder builder;


    public static NewCycleActivity newInstance() {
        return new NewCycleActivity();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_new_cycle, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        newcycl_chair = getActivity().findViewById(R.id.newcycl_chair);
        newcycl_ponte = getActivity().findViewById(R.id.newcycl_ponte);

        dialog = new Dialog(getContext());
        poussinDao=new PoussinDao();
        cycleService=new CycleService();

        poussinList=poussinDao.readAll();

        //matching des composants du dialog
        dialog.setContentView(R.layout.addnbtroupeau_newcycle);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(0));

        edtNomCycle = dialog.findViewById(R.id.addtroupeau_nomcycle);
        edtNbTroupeau = dialog.findViewById(R.id.addtroupeau_nbbete);
        btnValider = dialog.findViewById(R.id.addtroupeau_btnvalider);
        btnAnnuler = dialog.findViewById(R.id.addtroupeau_btnannuler);

        //initialisation des champs du dialog
        edtNbTroupeau.getText().clear();
        edtNomCycle.getText().clear();

        newcycl_chair.setOnClickListener(v -> {
            typPoussin="POULET CHAIR";
            dialog.show();
        });

        newcycl_ponte.setOnClickListener(v -> {
            typPoussin="PONTE";
            dialog.show();
        });

        //animation du gif
        try {
            GifDrawable gifFromResource = new GifDrawable(getResources(), R.drawable.congrets);
            gifFromResource.start();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //ajout des methodes
        validNewCycle();
        clickerSurBtnAnnuler();
    }

    /**
     * methode permettant de valider un nouveau cycle
     */
    private void validNewCycle() {

        btnValider.setOnClickListener(v -> {
            if (contraintNewCycle()) {
                //recuperer le type de poussin selectionné
                poussin = poussinDao.readOneByLibelle(typPoussin);

                //creer un instance de cycle pour l'enregistrer
                Cycle cycle = new Cycle();
                cycle.setPoussin(poussin);
                cycle.setNom(edtNomCycle.getText().toString());
                cycle.setNbOisseau(Integer.valueOf(edtNbTroupeau.getText().toString()));

                if (cycleService.create(cycle) != null) {
                    dialog.dismiss();

                    // Create Alert using Builder
                    builder = new CFAlertDialog.Builder(getContext())
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                            .setTitle(R.string.Felicitations)
                            .setMessage(R.string.Le_nouveau_cycle_ajoute)
                            .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                                //
                                dialog.dismiss();
                            });

                    // Show the alert
                    CFAlertDialog alertDialog = builder.show();
                    alertDialog.setOnDismissListener(dialog -> {
                        getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, AccueilFragment.newInstance()).commitNow();
                    });

                }else{
                    // Create Alert using Builder
                    builder = new CFAlertDialog.Builder(getContext())
                            .setDialogStyle(CFAlertDialog.CFAlertStyle.NOTIFICATION)
                            .setTitle(getString(R.string.Oups))
                            .setMessage(getString(R.string.Erreur_inattendue_survenue_durant_loperation))
                            .addButton(getString(R.string.CONTINUER), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.END, (dialog, which) -> {
                                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, AccueilFragment.newInstance()).commitNow();
                                //
                                dialog.dismiss();
                            });

                    builder.show();
                }
            }
        });
    }

    /**
     * Methode permettant d'annuler la creation d'un nouveau cycle
     */
    void clickerSurBtnAnnuler(){
        btnAnnuler.setOnClickListener(v->{
            dialog.dismiss();
            Toasty.warning(getContext(), R.string.Operation_annulee,Toasty.LENGTH_LONG).show();
        });
    }

    /**
     * methode permettant de gerer des contraintes de selection pour l'ajout du nouveau cycle
     * @return
     */
    private Boolean contraintNewCycle() {
        if (TextUtils.isEmpty(edtNomCycle.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(getContext())
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_un_nom_sil_vous_plait)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });

            builder.show();

            return false;
        }

        if (TextUtils.isEmpty(edtNbTroupeau.getText().toString())) {
            // Create Alert using Builder
            builder = new CFAlertDialog.Builder(getContext())
                    .setDialogStyle(CFAlertDialog.CFAlertStyle.BOTTOM_SHEET)
                    .setTitle(getString(R.string.Oups))
                    .setMessage(R.string.Veuillez_entrer_le_nombre_de_poussins_sil_vous_plait)
                    .addButton(getString(R.string.OK), Color.parseColor("#FDFDFC"), Color.parseColor("#FF9800"), CFAlertDialog.CFAlertActionStyle.POSITIVE, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                        //
                        dialog.dismiss();
                    });

            builder.show();

            return false;
        }

        return true;
    }

}
