package ci.objis.dao;

import java.util.List;

import ci.objis.model.Cycle;
import ci.objis.model.VenteOiseau;

public interface IVenteOiseauDao {

    public VenteOiseau create(VenteOiseau venteOiseau);

    public VenteOiseau readOne(Long id);

    public List<VenteOiseau> readAll();

    public Boolean delete(Long id);

}
