package ci.objis.dao;

import java.util.List;

import ci.objis.model.Utilisateur;

public interface IUtilisateurDao {

    public Utilisateur create(Utilisateur utilisateur);

    public List<Utilisateur> readAll();

    public Utilisateur readOne(Long id);

    public Utilisateur readOneByTel(String tel);

    public Utilisateur update(Utilisateur utilisateur);

    public Boolean delete(Long id);

}
