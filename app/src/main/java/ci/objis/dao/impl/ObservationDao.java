package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Observation;

public class ObservationDao {

    public Observation create(Observation observation) {
        if (observation.save() > 0) {
            return observation;
        }
        return null;
    }

    public Observation readOne(Long id) {
        return new Select()
                .from(Observation.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Observation> readAll() {
        return new Select()
                .from(Observation.class)
                .execute();
    }

    public Boolean delete(Long id) {
        Observation cycle = Observation.load(Observation.class, id);
        cycle.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }
}
