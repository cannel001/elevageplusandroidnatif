package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Elevage;

public class ElevageDao {


    public Elevage create(Elevage elevage) {

        if (elevage.save() > 0) {
            return elevage;
        }
        return null;
    }

    public Elevage readOne(Long id) {
        return new Select()
                .from(Elevage.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Elevage> readAll() {

        List<Elevage> listElevage = new Select().from(Elevage.class).execute();

        if (listElevage.isEmpty()) {
            //enregistrer des donnees par defaut
            create(new Elevage(null, "POULET"));
            create(new Elevage(null, "PINTADE"));
            //renvoyer les données enregistrées
            return new Select()
                    .from(Elevage.class)
                    .execute();

        }
        return listElevage;
    }

    public Boolean delete(Long id) {
        Elevage elevage = Elevage.load(Elevage.class, id);
        elevage.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

    public Elevage readOneByLibelle(String libelle) {
        return new Select()
                .from(Elevage.class)
                .where("libelle = ?", libelle)
                .executeSingle();
    }



}
