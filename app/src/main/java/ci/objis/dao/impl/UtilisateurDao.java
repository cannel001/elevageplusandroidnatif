package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

import ci.objis.dao.IUtilisateurDao;
import ci.objis.model.Utilisateur;
import okhttp3.internal.Util;

public class UtilisateurDao implements IUtilisateurDao {

    @Override
    public Utilisateur create(Utilisateur utilisateur) {

        if (utilisateur.save() > 0) {
            return utilisateur;
        }

        return null;
    }

    @Override
    public List<Utilisateur> readAll() {
        return new Select().from(Utilisateur.class).execute();
    }

    @Override
    public Utilisateur readOne(Long id) {
        return new Select().from(Utilisateur.class).where("id = ?", id).executeSingle();
    }

    @Override
    public Utilisateur readOneByTel(String tel) {
        return new Select().from(Utilisateur.class).where("tel = ?",tel).executeSingle();
    }

    @Override
    public Utilisateur update(Utilisateur utilisateur) {
        utilisateur.setDateUpdate(new Date());
        if (utilisateur.save() > 0) {
            return utilisateur;
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) {
        Utilisateur utilisateur = Utilisateur.load(Utilisateur.class, id);
        utilisateur.delete();
        if (Utilisateur.load(Utilisateur.class, id) == null) {
            return true;
        }
        return false;
    }
}
