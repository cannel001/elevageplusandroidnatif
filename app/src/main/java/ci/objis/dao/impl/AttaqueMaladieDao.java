package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.Date;
import java.util.List;

import ci.objis.dao.IAttaqueMaladieDao;
import ci.objis.model.AttaqueMaladie;
import ci.objis.model.Utilisateur;

public class AttaqueMaladieDao implements IAttaqueMaladieDao {

    @Override
    public AttaqueMaladie create(AttaqueMaladie attaqueMaladie) {

        if (attaqueMaladie.save() > 0) {
            return attaqueMaladie;
        }

        return null;
    }

    @Override
    public List<AttaqueMaladie> readAll() {
        return new Select().from(AttaqueMaladie.class).execute();
    }

    @Override
    public AttaqueMaladie readOne(Long id) {
        return new Select().from(AttaqueMaladie.class).where("id = ?", id).executeSingle();
    }

    @Override
    public AttaqueMaladie update(AttaqueMaladie attaqueMaladie) {
            if (attaqueMaladie.save() > 0) {
            return attaqueMaladie;
        }
        return null;
    }

    @Override
    public Boolean delete(Long id) {
    AttaqueMaladie attaqueMaladie = AttaqueMaladie.load(AttaqueMaladie.class, id);
        attaqueMaladie.delete();
        if (Utilisateur.load(Utilisateur.class, id) == null) {
            return true;
        }
        return false;
    }
}
