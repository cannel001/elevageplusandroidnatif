package ci.objis.dao.impl;

import android.util.Log;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.dao.IPerteDao;
import ci.objis.model.Cycle;
import ci.objis.model.Perte;

public class PerteDao implements IPerteDao {

    private static final String TAG="PerteDao";

    @Override
    public Perte create(Perte perte) {
        if (perte.save() > 0) {
            return perte;
        }
        return null;
    }

    @Override
    public Perte readOne(Long id) {
        return new Select()
                .from(Perte.class)
                .where("id = ?", id)
                .executeSingle();
    }

    @Override
    public List<Perte> readAll() {
        return new Select()
                .from(Perte.class)
                .execute();
    }

    @Override
    public Boolean delete(Long id) {
        Perte perte = Perte.load(Perte.class, id);
        perte.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

}
