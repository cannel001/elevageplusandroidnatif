package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Medicament;

public class MedicamentDao {

    public Medicament create(Medicament medicament) {
        if (medicament.save() > 0) {
            return medicament;
        }
        return null;
    }

    public Medicament readOne(Long id) {
        return new Select()
                .from(Medicament.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Medicament> readAll() {
        return new Select()
                .from(Medicament.class)
                .execute();
    }

    public Boolean delete(Long id) {
        Medicament medicament = Medicament.load(Medicament.class, id);
        medicament.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }
}
