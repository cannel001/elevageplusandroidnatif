package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Eau;

public class EauDao {

    public Eau create(Eau eau) {
        if (eau.save() > 0) {
            return eau;
        }
        return null;
    }

    public Eau readOne(Long id) {
        return new Select()
                .from(Eau.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Eau> readAll() {
        return new Select()
                .from(Eau.class)
                .execute();
    }

    public Boolean delete(Long id) {
        Eau eau = Eau.load(Eau.class, id);
        eau.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }
}
