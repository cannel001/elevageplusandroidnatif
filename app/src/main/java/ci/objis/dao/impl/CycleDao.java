package ci.objis.dao.impl;

import android.text.TextUtils;

import com.activeandroid.query.Select;

import java.util.LinkedList;
import java.util.List;

import ci.objis.model.Cycle;
import ci.objis.model.Elevage;
import ci.objis.model.Poussin;

public class CycleDao {

    //les proprietes
    private ElevageDao elevageDao;

    //constructeur
    public CycleDao() {
        this.elevageDao = new ElevageDao();
    }

    public Cycle create(Cycle cycle) {

        if (cycle.save() > 0) {
            return cycle;
        }
        return null;
    }

    public Cycle readOne(Long id) {
        return new Select()
                .from(Cycle.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Cycle> readAll() {
        return new Select()
                .from(Cycle.class)
                .execute();
    }

    public Boolean delete(Long id) {
        Cycle cycle = Cycle.load(Cycle.class, id);
        cycle.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

    public Cycle readOneByIdentif(Long identif){
        return new Select()
                .from(Cycle.class)
                .where("identif = ? ",identif)
                .executeSingle();
    }

    public List<Cycle> readAllByLibelleElevage(String libelleElevage) {

        if (!TextUtils.isEmpty(libelleElevage)) {
            //recuperation de l'elevage
            Elevage elevage = elevageDao.readOneByLibelle(libelleElevage);

            if (elevage != null) {

                List<Poussin> listPoussin = elevage.poussins();

                if (listPoussin != null) {
                    List<Cycle> cycleList = new LinkedList<>();
                    for (Poussin poussin : listPoussin) {
                        if (poussin.cycles() != null) {
                            cycleList.addAll(poussin.cycles());
                        }
                    }
                    return cycleList;
                }
            }
        }
        return null;
    }


}
