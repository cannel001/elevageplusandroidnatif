package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Aliment;
import ci.objis.model.Cycle;

public class AlimentDao {

    public Aliment create(Aliment aliment) {
        if (aliment.save() > 0) {
            return aliment;
        }
        return null;
    }

    public Aliment readOne(Long id) {
        return new Select()
                .from(Aliment.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Aliment> readAll() {
        return new Select()
                .from(Aliment.class)
                .execute();
    }

    public Boolean delete(Long id) {
        Aliment cycle = Aliment.load(Aliment.class, id);
        cycle.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

    public List<Aliment> readAllByCycle(Cycle cycle) {
        return new Select()
                .from(Aliment.class)
                .where("cycle=?", cycle.getId())
                .execute();
    }

}
