package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Temperature;

public class TemperatureDao {

    public Temperature create(Temperature temperature) {
        if (temperature.save() > 0) {
            return temperature;
        }
        return null;
    }

    public Temperature readOne(Long id) {
        return new Select()
                .from(Temperature.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Temperature> readAll() {
        return new Select()
                .from(Temperature.class)
                .execute();
    }

    public Boolean delete(Long id) {
        Temperature cycle = Temperature.load(Temperature.class, id);
        cycle.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }
}
