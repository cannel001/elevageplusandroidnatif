package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.dao.IVenteOiseauDao;
import ci.objis.model.Cycle;
import ci.objis.model.VenteOiseau;

public class VenteOiseauDao implements IVenteOiseauDao {

    @Override
    public VenteOiseau create(VenteOiseau venteOiseau) {
        if (venteOiseau.save() > 0) {
            return venteOiseau;
        }
        return null;
    }

    @Override
    public VenteOiseau readOne(Long id) {
        return new Select()
                .from(VenteOiseau.class)
                .where("id = ?", id)
                .executeSingle();
    }

    @Override
    public List<VenteOiseau> readAll() {
        return new Select()
                .from(VenteOiseau.class)
                .execute();
    }

    @Override
    public Boolean delete(Long id) {
        VenteOiseau cycle = VenteOiseau.load(VenteOiseau.class, id);
        cycle.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

}
