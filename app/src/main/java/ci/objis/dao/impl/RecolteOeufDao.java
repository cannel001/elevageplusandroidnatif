package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.RecolteOeuf;

public class RecolteOeufDao {

    public RecolteOeuf create(RecolteOeuf recolteOeuf) {
        if (recolteOeuf.save() > 0) {
            return recolteOeuf;
        }
        return null;
    }

    public RecolteOeuf readOne(Long id) {
        return new Select()
                .from(RecolteOeuf.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<RecolteOeuf> readAll() {
        return new Select()
                .from(RecolteOeuf.class)
                .execute();
    }

    public Boolean delete(Long id) {
        RecolteOeuf recolteOeuf = RecolteOeuf.load(RecolteOeuf.class, id);
        recolteOeuf.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

}
