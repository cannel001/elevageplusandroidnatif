package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.VenteOeuf;

public class VenteOeufDao {

    public VenteOeuf create(VenteOeuf recolteOeuf) {
        if (recolteOeuf.save() > 0) {
            return recolteOeuf;
        }
        return null;
    }

    public VenteOeuf readOne(Long id) {
        return new Select()
                .from(VenteOeuf.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<VenteOeuf> readAll() {
        return new Select()
                .from(VenteOeuf.class)
                .execute();
    }

    public Boolean delete(Long id) {
        VenteOeuf venteOeuf = VenteOeuf.load(VenteOeuf.class, id);
        venteOeuf.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

}
