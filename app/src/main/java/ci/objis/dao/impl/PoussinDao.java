package ci.objis.dao.impl;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Elevage;
import ci.objis.model.Poussin;

public class PoussinDao {

    //les proprietes
    private ElevageDao elevageDao;

    //constructeur
    public PoussinDao() {
        this.elevageDao = new ElevageDao();
    }

    public Poussin create(Poussin poussin) {

        if (poussin.save() > 0) {
            return poussin;
        }
        return null;
    }

    public Poussin readOne(Long id) {
        return new Select()
                .from(Poussin.class)
                .where("id = ?", id)
                .executeSingle();
    }

    public List<Poussin> readAll() {

        List<Poussin> listPoussin = new Select().from(Poussin.class).execute();

        if (listPoussin.isEmpty()) {

            //enregistrer des donnees par defaut
            Elevage el1,el2;
            el1 = elevageDao.readOneByLibelle("POULET");
            el2 = elevageDao.readOneByLibelle("PINTADE");

            if (el1 == null) {
                elevageDao.create(new Elevage(null, "POULET"));
                el1 = elevageDao.readOneByLibelle("POULET");
            }

            if (el2 == null) {
                elevageDao.create(new Elevage(null, "PINTADE"));
                el2 = elevageDao.readOneByLibelle("PINTADE");
            }


            create(new Poussin(null, "POULET CHAIR", el1));
            create(new Poussin(null, "PONTE", el1));
            //renvoyer les données enregistrées
            return new Select()
                    .from(Poussin.class)
                    .execute();

        }
        return listPoussin;
    }

    public Boolean delete(Long id) {
        Poussin elevage = Poussin.load(Poussin.class, id);
        elevage.delete();
        if (readOne(id) != null) {
            return false;
        }
        return true;
    }

    public Poussin readOneByLibelle(String libelle) {
        return new Select()
                .from(Poussin.class)
                .where("libelle = ?", libelle)
                .executeSingle();
    }

}
