package ci.objis.dao;

import com.activeandroid.query.Select;

import java.util.List;

import ci.objis.model.Cycle;
import ci.objis.model.Perte;

public interface IPerteDao {

    public Perte create(Perte perte);

    public Perte readOne(Long id);

    public List<Perte> readAll();

    public Boolean delete(Long id);

}
