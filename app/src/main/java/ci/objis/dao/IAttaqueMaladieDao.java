package ci.objis.dao;

import java.util.List;

import ci.objis.model.AttaqueMaladie;

public interface IAttaqueMaladieDao {

    public AttaqueMaladie create(AttaqueMaladie attaqueMaladie);

    public List<AttaqueMaladie> readAll();

    public AttaqueMaladie readOne(Long id);

    public AttaqueMaladie update(AttaqueMaladie attaqueMaladie);

    public Boolean delete(Long id);
}
