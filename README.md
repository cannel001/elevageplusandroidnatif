# Elevage Plus - Android

C'est une solution mobile de suivi journalier des taches d’une ferme et de la mise en relations des éleveurs avec les clients potentiels.


* Quelques ecrans

![Screenshot_20190603-091512](/uploads/1306567c44dec4a41de4d07b1c632789/Screenshot_20190603-091512.png)

![Screenshot_20190603-091503](/uploads/f9e99ce41b75717d9b630445a0298db9/Screenshot_20190603-091503.png)

![Screenshot_20190603-091521](/uploads/230719b9fab2013b8f8b0c4cd0be574e/Screenshot_20190603-091521.png)

![Screenshot_20190603-091533](/uploads/d2392f7232109037dcb38a65c55bda78/Screenshot_20190603-091533.png)

![Screenshot_20190603-091545](/uploads/b8fbd888d4c11e6ed05307e6d6e129d9/Screenshot_20190603-091545.png)

![Screenshot_20190603-091556](/uploads/92b381e27a638440c2c81200e32abcb1/Screenshot_20190603-091556.png)